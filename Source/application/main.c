//******************************************************************************
//

//      Copyright (c) 2012, Canrig Drilling
//      ALL RIGHTS RESERVED
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-     		DD          Initial Implementation
//	....
//
//	2013-09-18 		DD			Update RFC packet structure
//	2013-10-22		DD			Turn On/Off the Strain gauges in Idle mode. Strain gauges ADCs initialized in InitIdleMode() only.
//	2014-07-17		DD			use ADC internal IDAC instead of external Vref.
//  2019-06-04      Brian Dewald Added Wake on Turns and did a LOT of refactoring.
//  2019-09-07      Brian Dewald Restructured IdleTask and RFC timer so that the a RFC other than 1S will work. Now the variable
//                               time is how long the IdleTask "sleeps" rather than how long it samples the strain gauges.
//                               Removed a lot of dead code and simplified other code.
//*******************************************************************************

#include "..\drivers\drivers.h"

#define ZB_TX_PERIOD		        5		                // +1ms to read the compass 			//in ms

#define SENSORS_READ_TIME           220                     // read the sensors in IdleMode
#define ADC_STABLIZATION_TIME       30                      // ADC needs to be read for 30mS before readings are stable

WTTTS_STATES currentState;

const UVOLTS SG_DIVIDER_VALUE = 2050000l;

static BYTE adcNumber;
static ADS1220_SOURCE analogChannel;
static BYTE bDataArray[30] = {0};
static UVOLTS uvResultTemp[ADS1220_TOTAL] = {0};
UVOLTS uvResultAverage[ADS1220_TOTAL];
UVOLTS uvResultAverageTemp[ADS1220_TOTAL];

static TIMERHANDLE thGenTimer, thGenTimer1, thGenTimer2;    // represents a 1mS timer. Used for various timing tasks
static TIMERHANDLE thTimerSec1,thTimerSec2;

WORD	wCounter;
static BYTE bStGyroId;                                      // This reads the ID value from the ST gyro but not used

static BYTE pUartRxData[BUF_LENGTH];
signed long gyroSumAverage;                                 // Used to accumulate Epson gyro readings
signed long slVar;
static WORD nbrTransmission;
static WORD nbrResponses;
WORD maxPktLength;

WORD SamplesCounter[LAST_SG_ADC+2] = {0};                   // This keeps track of number of ADC samples for each channel. Use for averaging
WORD missedRFC = 0;                                         // used for debugging will be removed
BYTE lostConnection = 0;                                    // used for debugging will be removed

static BYTE skipSample;                                     // used to count number of RFC before reading non critical sensors (battery, gas gauge, etc...)
static BOOL RfcSent;                                        // Flag used to let IdleMode know when the RFC command has been sent
static BOOL readCompass;
static BOOL radioRxOn;
BOOL digGyroOn;
BOOL reedSwitchEvent = FALSE;                               // Used to tell system to leave deep sleep

 //	Care must be taken not to exceed the compliance voltage of the IDACs.
 // In other words, limit the voltage on the pin where the IDAC is routed to < (AVDD � 0.9 V), otherwise the specified
 // accuracy of the IDAC current is not met
ADS1220_IDAC_CURRENT idacCurrent250ua = AD1220_IDAC_CURRENT_250uA;
ADS1220_IDAC_CURRENT idacCurrent1000ua = AD1220_IDAC_CURRENT_1000uA;
ADS1220_IDAC_CURRENT idacCurrent1500ua = AD1220_IDAC_CURRENT_1500uA;		// only for use with 1 KOhm Strain Gauge
ADS1220_IDAC_CURRENT idacCurrent500ua = AD1220_IDAC_CURRENT_500uA;		// only for use with 1 KOhm Strain Gauge
ADS1220_IDAC_CURRENT idacCurrent[ADS1220_TOTAL];		// only for use with 1 KOhm Strain Gauge

// ***************************************Local function declaration

void EpsonGyroRead(void);                                   // Move to ads1220.c
void StrainGaugeRead(void);                                 // move to ads1220.c
void deepSleep(void);                                       // This is much like IdleTask but only checks the gyro and reed switch
void SystemPowerDown(void);                                 // Sets the system into a very low power mode
void SystemInit(void);                                      // Used after wake from "Deep Sleep"
void InitStreamMode(void);                                  // when switching from any other state to streaming
void InitIdleMode(void);                                    // when switching from any other state to idle
void RfCommTask(void);                                      // check to see if valid communications have come in
void WtttsMainTask(void);                                   // system state machine
void StreamingTask(void);                                   // sends data at streaming rate (usually 10mS)
void IdleTask(void);                                        // Will sleep between comms then sends system data and requests a command. Otherwise sleeps
void setIdleSleep(void);                                    // Used by IdleTask() in two locations
void InitHuntingMode(void);                                 // Used to establish comms with PC on the various channels
void ConnectingTask(void);                                  // Sets system up after PC found on a certain channel
void resetStrainGaugesReading(void);                        // Used to clear readings before starting new sequence
void InitAllStrainGaugeADC(void);                           // Power up all strain gauge ADC's
void StrainGaugeReadInIdleMode(void);                       //
void AverageADCResults(void);                               // Used by IdleTask() and StreamTask()
void MeasureStrainGaugeValueAndSetCurrent(void);            // Should be in ads1220.c
void initGaugesAfterIdleWake(void);                         // Sets ADC read time and will start ADC and wait for it to stabilize

void main(void) {
	initMPS430Pins();
	currentState = ST_INIT;
	ReadLastResetCause();

	clearMagnetData();
	memset( &pUartRxData, 0, sizeof( pUartRxData ) );
	memset( &uvResultAverageTemp, 0, sizeof( uvResultAverageTemp ) );
	
	StopWatchdog();
	SetWatchdog();

	ResetWatchdog();

	Set_System_Clock();
	SpiA0Initialize();
	InitTimers();
	__bis_SR_register(GIE);
	EnableAllPower();

	// Enable and start generic millisecond timers
	thGenTimer = RegisterTimer();                           // gives timer its slot number
	StartTimer( thGenTimer );
	thGenTimer1 = RegisterTimer();
	StartTimer( thGenTimer1 );
	thGenTimer2 = RegisterTimer();
    StartTimer( thGenTimer2 );

    // register and start generic second timers
	thTimerSec1 = RegisterSecTimer();
    StartSecTimer( thTimerSec1 );
	thTimerSec2 = RegisterSecTimer();
	StartSecTimer( thTimerSec2 );

	ResetWatchdog();

	InitRadioInterface();

	ReedSwitchInit();
	SetOutputPin(SW_LED, TRUE );

	ResetTimer( thGenTimer, TEN_THOUSAND_MS );              // Radio can start a firmware update on startup for 10 seconds.

	EvalBatType();
	MeasureBatVoltageAdc();

	initST();                                               // Start the digital gyro
	readSTid();
	bStGyroId = gedStId();
	SelfTestStGyro();                                       // Changed how this works Oct 2019 Brian Dewald
	calibrateStGyro();                                      // This assumes the gyro is not turning on startup. Maybe bad if reset by WDT

	while( !TimerExpired( thGenTimer ) ) {                  // Wait time for over the air firmware update to start
		ResetWatchdog();
	}

	SetOutputPin( SW_LED, FALSE );
	ResetTimer( thGenTimer, FIVE_MS );
	InitGasGauge();

	ReadSerNumber();                                        // Read from the serial number chip on Rev 1 hardware
	StopRtc();

	while(1) {                                              // Main loop
		ResetWatchdog();
		RfCommTask();                                       // if data ready
		WtttsMainTask();
		if( IsSwitchOn() == FALSE ) {                       // if read switch active (button pressed or magnet present)
			SystemPowerDown();
		}
		// check for low battery voltage and power down
		if( GetBatVoltageAdc() < UVLO_LOCKOUT_MV ) {        // if battery lower than minimum level to operate correctly
			SystemPowerDown();                              // minimum level will change for board rev 2
		}
	}
}

//  read the analog gyro output. Do weighted average and accumulate the result.
// TODO: Move to ads1220.c
void EpsonGyroRead(void) {

	if( ADS1220HasResult( ADC_EPSON_GYRO ) ) {
		ADS1220ReadVoltageCont( ADC_EPSON_GYRO, &uvResultTemp[ADC_EPSON_GYRO] );
		if( gyroSumAverage == 0 )
			uvResultAverage[ADC_EPSON_GYRO] = uvResultTemp[ADC_EPSON_GYRO];
		else
			uvResultAverage[ADC_EPSON_GYRO] = ( uvResultAverage[ADC_EPSON_GYRO] * 3 + uvResultTemp[ADC_EPSON_GYRO] ) >> 2;

		gyroSumAverage += uvResultAverage[ADC_EPSON_GYRO];
	}
}

//------------------------------------------------------------------------------
// initialize the strain gauges reading in Idle mode
// clear all storage and counters
void resetStrainGaugesReading(void) {
	memset( &SamplesCounter, 0, sizeof( SamplesCounter ) );     // reset all ADC readings to start new averaging
	memset( &uvResultAverage, 0, sizeof( uvResultAverage ) );

	adcNumber = FIRST_SG_ADC;
}

//------------------------------------------------------------------------------
// Read all strain gauges ADC on a rotational basis. Called repeatedly Streaming task
// This system will average on the fly with a weighted average.
//
// !!!!!!!!!!!!!!!  There is an issue with this. !!!!!!!!!!!!!!!!!!!
//
// If first reading is way out, the average will be heavily skewed !!!!
// todo: move to ads1220.c
void StrainGaugeRead(void) {
    // sample all ADC except the Gyro ADC on a rotational basis
    if( adcNumber > LAST_SG_ADC )
        adcNumber = FIRST_SG_ADC;

    if( ADS1220HasResult( adcNumber ) ) {
        ADS1220ReadVoltageCont( adcNumber, &uvResultTemp[adcNumber] );
        uvResultAverage[adcNumber]=( uvResultAverage[adcNumber] * 15 + uvResultTemp[adcNumber] ) >> 4;
    }
    adcNumber++;
}

//------------------------------------------------------------------------------
// read all strain gauges ADC on a rotational basis and accumulate them over one transmission period.
//	 Count number of samples.
void StrainGaugeReadInIdleMode(void) {

	if( ADS1220HasResult( adcNumber ) ) {                   // if ADC is reporting its sample is complete
		ADS1220ReadVoltageCont( adcNumber, &uvResultTemp[adcNumber] );

		uvResultAverage[adcNumber] += uvResultTemp[adcNumber];

		SamplesCounter[adcNumber]++;                        // keep track of number of samples for averaging when done
	}

	adcNumber++;

	if(adcNumber > LAST_SG_ADC)                             // sample all ADC except the Gyro on a rotational basis
		adcNumber = FIRST_SG_ADC;
}

//------------------------------------------------------------------------------
// Take the value in uvResultAverage and divide it by SamplesCounter
// for each ADC reading and store it back in uvResultAverage
// average the results if data has been read from the ADC
// otherwise send the old values.
void AverageADCResults(void) {
	int i;

	for( i=FIRST_SG_ADC; i<=LAST_SG_ADC; i++ ) {
		if( SamplesCounter[i] ) {		                    // we read at least one data from the ADC. Don't divide by 0
			uvResultAverage[i] /= SamplesCounter[i];
		}
	}
}

//------------------------------------------------------------------------------
// Called when the power button (reed switch) is pressed or if the system has had
// no communications from the PC for a set time
void SystemPowerDown(void) {

    PowerDownRadio();
    DisableAnalogGyro();
    PowerDownADC();
    AccelCompassDisable();                                  // Sets Accel & Compass chip in low power mode
    setDigGyroWakeOnTurns();
    LedSwPowerDownSequence();
    ResetTimer( thGenTimer, ONE_SECOND );

    while( TimerExpired( thGenTimer) == FALSE ) {
        ResetWatchdog();
    }
    SetOutputPin(SW_LED, OFF );

    StopWatchdog();

    SetReedSwitchInterrupt();

    PowerDownStGyro();                                      // don't reset the flag cause it's checked above and set there.
    digGyroOn = FALSE;

    currentState = ST_DEEP_SLEEP;
    //SetSystemDeepSleep();
    //SystemInit();
//  LedSwPowerUpSequence();
//  WDTCTL = WDT_MDLY_0_064;
//  while(1);
}

//------------------------------------------------------------------------------
// This is cycled when the system is in power down mode to check the
// gyro and reed switch when timer A0 interrupts.
// Added Oct 2019 by BDD. This is to reduce power since keeping the gyro
// awake full time draws 7mA. Thus we switched to a 10 second wake and
// then check the gyro for turns.
void deepSleep(void) {
    StopWatchdog();
    SetLPMode( TRUE );                                      // Flag to tell Timer A0 interrupt what to do
    __bis_SR_register(LPM0_bits + GIE);                     // Enter LPM0 w/interrupt
    __no_operation();                                       // For debugger
    SetLPMode( FALSE );                                     // Upon wake
    SetWatchdog();

    if( reedSwitchEvent ) {
        SystemInit();
        return;
    }
    WakeUpStGyro();                                         // after wake from sleep...
    digGyroOn = TRUE;

    ResetTimer( thGenTimer, 300 );                          // give some time for the gyro to stabilize
    while ( TimerExpired( thGenTimer ) == FALSE ) {
        ResetWatchdog();
    }

    if( stGyroHasData() ) {
        GetSTGyroData();
    }
    PowerDownStGyro();                                      // don't reset the flag cause it's checked above and set there.
    digGyroOn = FALSE;

    if( GetRPM() > 5 ) {
       SystemInit();
    }
}

//------------------------------------------------------------------------------
// Was called from SystemPowerDown. Now called from deepSleep. Is used to initialize
//  the board after "Deep Sleep"
void SystemInit(void) {
	currentState = ST_INIT;
	SetOutputPin( SW_LED, TRUE );
	clearMagnetData();
	memset( &pUartRxData, 0, sizeof( pUartRxData ) );       // clear communications data

	ResetWatchdog();

	initMPS430Pins();
	EnableAllPower();
	Set_System_Clock();
	SpiA0Initialize();

	SysTimerConfig();
	__bis_SR_register(GIE);

	InitRadioInterface();

	ReedSwitchInit();
	LedSwPowerUpSequence();

	MeasureBatVoltageAdc();

//	initST();                                               // this is not needed on wake as the gyro was not powered down
//	readSTid();
//	bStGyroId = gedStId();
//	SelfTestStGyro();
//	calibrateStGyro();

    reedSwitchEvent = FALSE;                                // turn off the flag that alerts, it's time to wake

	SetOutputPin( SW_LED, FALSE );
}   // end of SystemInit();

//------------------------------------------------------------------------------
// Called from main, this will check if the radio has received a valid packet
// then call ExecuteCommand. If the command is not a "NoCommand", it will set
// the flags for IdleMode() to send another RFC
void RfCommTask(void) {
	static WORD pktLength;          // Does this need to be 'static'?

	if ( ReceiverHasData() ) {
		pktLength = ReadReceiverData(pUartRxData, sizeof(pUartRxData));

		if( HaveWtttsPacket( pUartRxData, pktLength ) ) {
			ExecuteCommand();
            nbrTransmission = 0;                            // reset counter for lost transmissions

			// after command different from "NO_COMMAND" send RFC after RFC_RESPONSE_TIME
			// does not send right away because the radio relies on 2 ms interval between the packets
			if( GetCommandStatus() == FALSE ) {
				ResetTimer( thGenTimer, RFC_RESPONSE_TIME );
				RfcSent = FALSE;                            // To enter if statement in IdleTask again
			}
		}
	}
}

//------------------------------------------------------------------------------
// Called from main, this is the central director that will call the needed
// functions based on the "mode" the system is in. The system will default to
// "Hunting" (ST_INIT) if there's no connection to the PC.
void WtttsMainTask(void) {
	switch( currentState ) {
	case ST_INIT:
		InitHuntingMode();
		break;
	case ST_HUNT:
		ConnectingTask();
		break;
	case ST_IDLE:
		IdleTask();
		break;
	case ST_STREAMING:
		StreamingTask();
		break;
	case ST_DEEP_SLEEP:
	    deepSleep();
	    break;
	default:
		currentState = ST_INIT;
		break;
	}
}

//------------------------------------------------------------------------------
//  Initialize the board for hunting mode to search for PC
// after a set number of seconds (default 900) put the system to sleep
void InitHuntingMode(void) {

	EnableAllPower();
	InitRadioInterface();
	ResetWatchdog();
	UpdateTimeParamsFromFlash();

	AccelCompassDisable();

	initST();

	TurnOffAllAdcRef();                                     // turn off the reference voltage. We use the internal current source now.
    PowerDownStGyro();                                      // don't reset the flag cause it's checked above and set there.
    digGyroOn = FALSE;

	ResetWatchdog();

	MeasureStrainGaugeValueAndSetCurrent();                 // Initial read of strain gauges to know which type is used. Set current as needed
	// configure ADC6 as temperature sensor
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray, sizeof(bDataArray));
	ResetTimer( thGenTimer, 5 );

	while( !TimerExpired( thGenTimer ) ) {
		ResetWatchdog();
	}

	ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
	ResetTimer( thGenTimer, 5 );

	while( !TimerExpired( thGenTimer ) ) {
			ResetWatchdog();
	}

	ADS1220ReadAllRegs( ADC_USED_AS_TEMP, bDataArray, sizeof( bDataArray ) );
	ADS1220StartConversion( ADC_USED_AS_TEMP );

	// initialize tension and torque to 0x7FFFFF
	for( adcNumber = FIRST_SG_ADC; adcNumber<ADS1220_TOTAL; adcNumber++ ) {
		uvResultAverage[adcNumber] = INT24_MAX_POSITIVE;
	}
	// configure the timers
	ResetTimer( thGenTimer, RFC_HUNTING_PERIOD_MS );
	ResetTimer( thGenTimer1, CHANNEL_CYCLE_MS );
	ResetSecTimer( thTimerSec1, GetTimeParameter( SRT_PAIR_TIMEOUT ) );

	nbrTransmission = 0;
	nbrResponses = 0;
    currentState = ST_HUNT;
}

//------------------------------------------------------------------------------
//  read temperature, RPM and gas gauge. sends RFC every 100 ms.
// 	If it doesn't get response cycle the channels every UPDATE_CHANNEL_NBR_TR transmission
//	If it doesn't get response after HUNTING_MODE_TIMEOUT_SEC go to deep sleep
//	If it gets response go to IDLE mode
void ConnectingTask(void) {

	// if a valid radio packet has been received increment nbrResponses
	if( GetRadioPacketStatus() == TRUE ) {
		ClearRadioPacketStatus();
		nbrResponses++;
	}

	// if response (NoCommand) is received more than NBR_RESP_CONNECTION on one channel
	// the correct channel is found - go to IDLE mode
	if( nbrResponses >= NBR_RESP_CONNECTION ) {
		currentState = ST_IDLE;
		InitIdleMode();
		return;
	}

	// RFC timeout - send the message
	if( TimerExpired( thGenTimer ) ) {
		ResetTimer( thGenTimer, RFC_HUNTING_PERIOD_MS );
		ReadTemperature();
		SendRfc();
		nbrTransmission++;
	}

	// channel cycle timeout - change the channel
	if( nbrTransmission >= UPDATE_CHANNEL_NBR_TR ) {
		// wait until the UART finishes sending the previous packet
		while( IsZbTiSending() ) {
			ResetWatchdog();
		}
		// wait for the radio to send the data over the air and get ready for the packet
		ResetTimer( thGenTimer1, RFC_RESPONSE_TIME );
		while(TimerExpired( thGenTimer1 ) == FALSE ) {
			ResetWatchdog();
		}
		IncreaseZbChannel();							    // increment the channel
		SetRadioChannel();								    // set the new channel in the radio
		nbrTransmission = 0;
		nbrResponses = 0;
	}

	// check for power down timeout.
	if( SecTimerExpired( thTimerSec1 ) ) {
		SystemPowerDown();                                  // This is set if there has been nothing from the PC for X seconds (default is 15 min)
	}
} // End of ConnectingTaskI()

//------------------------------------------------------------------------------
// This will set up all devices and registers for Idle Mode. This will take into
// account that it may be entered after streaming, power up, or wake from sleep
void InitIdleMode(void) {

	DisableAnalogGyro();                                    // If coming from streaming mode
	initST();                                               // Start high speed gyro L3G4200DTR
    digGyroOn = TRUE;
    AccelAndCompassInit();					                // initialize the compass and accelerometer then wait 5ms
	ResetTimer( thGenTimer, FIVE_MS );
	while( !TimerExpired( thGenTimer ) ) {
		ResetWatchdog();
	}

	SetRadioReceiverOn();

	// configure all ADCs connected to strain gauge
	for( adcNumber = FIRST_SG_ADC; adcNumber<=LAST_SG_ADC; adcNumber++ ) {

		ADS1220Reset( adcNumber );

		ADS1220ReadAllRegs( adcNumber, bDataArray, sizeof( bDataArray ) );
		ResetTimer( thGenTimer, FIVE_MS );

		while( !TimerExpired( thGenTimer) ) {
			ResetWatchdog();
		}
		analogChannel = ADS1220_IN_AIN1_AIN2;
		//analogChannel = ADS1220_IN_AVDD_BY_4;	// measures AVDD
		//analogChannel = ADS1220_IN_VREF;	// measures Vref

		ADS1220Configure( analogChannel, adcNumber, idacCurrent[adcNumber] );
		ResetTimer( thGenTimer, FIVE_MS );

		while( !TimerExpired( thGenTimer ) ) {
			ResetWatchdog();
		}
		ADS1220ReadAllRegs( adcNumber, bDataArray,sizeof( bDataArray ) );
		ADS1220StartConversion( adcNumber );
	}

	// configure ADC7 as temperature sensor
	ADS1220ReadAllRegs( ADC_USED_AS_TEMP, bDataArray, sizeof( bDataArray ) );
	ResetTimer( thGenTimer, FIVE_MS );

	while( !TimerExpired( thGenTimer ) ) {
		ResetWatchdog();
	}
	ADS1220ConfigAsTemperatureSensor( ADC_USED_AS_TEMP );
	ResetTimer( thGenTimer, FIVE_MS );

	while( !TimerExpired( thGenTimer) ) {
		ResetWatchdog();
	}
	ADS1220ReadAllRegs( ADC_USED_AS_TEMP, bDataArray, sizeof( bDataArray ) );
	ADS1220StartConversion( ADC_USED_AS_TEMP );

	ResetTimer( thGenTimer, GetTimeParameter( SRT_RFC_RATE ) );
	resetRFCTimer();                                                        // will restart the rolling RFC timer
	ResetTimer( thGenTimer1, GetTimeParameter( SRT_RFC_RATE ) );            // Insure we don't timeout in IdleMode

	ResetSecTimer( thTimerSec1, GetTimeParameter( SRT_PAIR_TIMEOUT ) );     // Reset lost comms timeout
	ResetSecTimer( thTimerSec2, 0 );
	nbrTransmission = 0;                                    // reset missed comms counter
	skipSample = SAMPLES_TO_SKIP;                           // will not read batt. gas gauge, Etc... for set RFC cycles

	resetStrainGaugesReading();
	RfcSent = FALSE;
}   // end of InitIdleMode()

//------------------------------------------------------------------------------
// Used to set how long the ADC's will be read in idle mode
// Starts the ADC and reads it several times to stabilize readings
void initGaugesAfterIdleWake(void) {
    InitAllStrainGaugeADC();

    if( !skipSample ) {                                     // Read temperature on specified RFC's only
        ADS1220ConfigAsTemperatureSensor( ADC_USED_AS_TEMP );
        ADS1220StartConversion( ADC_USED_AS_TEMP );
    }

    ResetTimer( thGenTimer2, ADC_STABLIZATION_TIME );       // set a timer to read dummy ADC values
    while( !TimerExpired( thGenTimer2 ) ) {
        StrainGaugeReadInIdleMode();                        // First few readings are crazy. Just dump them.
        ResetWatchdog();
    }
    resetStrainGaugesReading();                             // Clears all values and counters
    ResetTimer( thGenTimer, SENSORS_READ_TIME );            // determines how long to read sensors. Mostly for strain gauges.

}

//------------------------------------------------------------------------------
//  the IdleTask will be called repeatedly from the main loop until all data is read and RFC sent
//  A "NoCommand" or communications timeout will put the system back to sleep
//  the system is woken every RFC_Time set in Timer A0
//  On wake, read Strain Gauges for a set time
//  Read ST gyro only powered every second RFC. This is because the device takes a long time to wake
//      The reason it's not just left on, is because it draws high current.
//  Read temperature, and gas gauge only on specified RFC cycles.
//	Power down radio between transmissions.
//  In general, the system will send an RFC at 1Hz (unless configured otherwise).
//  The radio will get a "No Command", "Start Streaming", "Read Info Mem", Etc... or will timeout.
//  If the radio times out a set number of times, the system will enter "Hunting" mode.
void IdleTask(void) {

	ResetWatchdog();

	if( IsRadioActive() ) {                                 // If Radio has sent RFC take care of cleanup after command received or timeout
		if( TimerExpired( thGenTimer1 ) ) {                 // If no response from PC
		    missedRFC++;                                    // used for debugging. to be removed.
			setIdleSleep();                                 // when uP wakes, leave this if-else
		} else {
			if( GetCommandStatus() == TRUE ) {              // If PC sent "No Command"
				SetNoCommand( FALSE );                      // reset the flag
				ResetSecTimer( thTimerSec1, GetTimeParameter( SRT_PAIR_TIMEOUT ) ); // reset lost comms timeout
				setIdleSleep();                             // power down and wait for interrupt to wake up
			}
		}
	}

	if( !TimerExpired( thGenTimer ) && !IsRadioActive() ) { // don't read gauges if radio is on cause an RFC has been sent already
	    StrainGaugeReadInIdleMode();                        // This section will read the gauges and sensors until thTimer is done.
	}

    // If sensors have been read for set time (set in initGaugesAfterIdleWake). Should only be done once per RFC!
	if( TimerExpired( thGenTimer ) && !RfcSent ) {

	    if( digGyroOn == TRUE ) {                           // should be on but just in case
	        if( stGyroHasData() ) {
                GetSTGyroData();
	        }
            PowerDownStGyro();                              // don't reset the flag cause it's checked above and set there.
            digGyroOn = FALSE;
		}

		if( !skipSample ) {                                 // read battery, gas gauge, temperature on certain cycles
			BatteryGasMonitorReadAllRegs();                 // Initialize reading gas gauge (takes time)
		}

		// turn on the radio now that readings are done.
		if( IsRadioActive() == FALSE ) {                    // If radio was not active (initial wake from sleep)
			ResetRadio();
			ResetTimer( thGenTimer1, 3 );		            // wait 3ms to wake up the radio
			while( !TimerExpired( thGenTimer1 ) );
			SetRadioChannel();
			ResetTimer( thGenTimer1, 2 );		            // wait 2ms to execute set the channel
			while( !TimerExpired( thGenTimer1 ) );
		} else {                                            // If radio was active
		    if( !skipSample ) {
                ResetTimer( thGenTimer1, 5 );		        // wait to read the gas gauge in case radio was on.
                while( !TimerExpired( thGenTimer1 ) );
		    }
		}

		if( !skipSample ) {                                 // only read on selected RFC
			ReadTemperature();                              // read internal temperature from the ADC (not RTD)

			if( STC3100DataReady() == TRUE ) {              // if gas gauge finished reading
				STC3100GetRawData( bDataArray, sizeof( bDataArray ) );
				ParseGasGaugeData( bDataArray );
			}
			MeasureBatVoltageAdc();
		}

		PowerDownADC();

		AccelCompassReadStart();                            // Start the read process and obtain the values later
		ResetTimer( thGenTimer1, 2 );		                // wait for compass to read and load buffer
		while( !TimerExpired( thGenTimer1 ) );

		ResetTimer( thGenTimer1, 4 );
		// wait to read the compass or timeout occurs
		while( AccelCompassDataReady() == FALSE ) {
			ResetWatchdog();
			if( TimerExpired( thGenTimer1 ) )
				break;
		}

		ReadAccelCompass();                                 // Wont update values if compass is not ready.

		AverageADCResults();
		SendRfc();
//		resetStrainGaugesReading();                         // reset all arrays holding ADC samples
		nbrTransmission++;                                  // Used to determine when connection lost
		SetNoCommand( FALSE );                              // clear the flag
		if( IsChannelChangePending() == TRUE ) {
			ResetTimer( thGenTimer1, 5 );		            // wait 5ms to for the RFC to be sent
			while( !TimerExpired( thGenTimer1 ) );
			SetNewChannel();
		}

		ResetTimer( thGenTimer1, GetTimeParameter( SRT_RFC_TIMEOUT ) );

		if(!skipSample) {
	        skipSample = SAMPLES_TO_SKIP;                   // don't read samples next specified RFC's
		} else {
		    skipSample--;
		}
		RfcSent = TRUE;                                     // Flag to insure RFC only sent when needed
	}   // end of "if" that determines when strain gauge readings have ended

	if( IsRadioActive() ) {
        if( GetStartStreamStatus() ) {                      // check for start streaming command
            currentState = ST_STREAMING;
            SetStartStream( FALSE );
            InitStreamMode();
        }

        if( GetPowerDownStatus() ) {                        // check for power down command
            SystemPowerDown();
            SetPowerDown( FALSE );
        }

        // check for communication lost timeout
        // if it doesn't receive response for 10 transmissions
        // start hunting for base radio again
        if( nbrTransmission > TRANSMISSION_TIMEOUT ) {
            currentState = ST_INIT;
            lostConnection++;
        }
	}

}

//------------------------------------------------------------------------------
//  Description: Initialize the system for streaming mode.
//	update Jan 30 2015	- Keep all ADCs On all the time.
void InitStreamMode(void) {
	int i;
	EnableAllPower();
	EnableAnalogGyro();
	PowerDownStGyro();
	AccelAndCompassInit();

	ResetTimer( thGenTimer, 10 );

	while( !TimerExpired( thGenTimer ) ) {
		ResetWatchdog();
	}

	if( IsRadioActive() == FALSE ) {
		ResetRadio();
		ResetTimer( thGenTimer1, FOUR_MS );		            // wait 4ms to wake up the radio
		while( !TimerExpired( thGenTimer1 ) );
		SetRadioChannel();
		ResetTimer( thGenTimer1, THREE_MS );		        // wait 3ms to execute the command
		while( !TimerExpired( thGenTimer1 ) );
	}

	InitAllStrainGaugeADC();

	analogChannel = ADS1220_IN_AIN1_AIN2;
	ADS1220Configure( analogChannel, ADC_EPSON_GYRO, AD1220_IDAC_CURRENT_OFF ); // Enable gyro for reading.
	ResetTimer( thGenTimer, FIVE_MS );

	while( !TimerExpired( thGenTimer ) ) {
		ResetWatchdog();
	}
	ADS1220ReadAllRegs( ADC_EPSON_GYRO, bDataArray, sizeof( bDataArray ) );
	ADS1220StartConversion( ADC_EPSON_GYRO );

	// TO DO configure accelerometer

	gyroSumAverage = 0;                                     // reset readings from Epson gyro

	//at power up the analog gyro output VOUT_GZ overshoots for 2.5ms
	// wait 4ms to stabilize the gyro
	ResetTimer( thGenTimer1, 60 );
	while( !TimerExpired( thGenTimer1 ) ) {
		if( ADS1220HasResult( ADC_EPSON_GYRO ) )
			ADS1220ReadVoltageCont( ADC_EPSON_GYRO, &uvResultTemp[ADC_EPSON_GYRO] );
	}

	ResetTimer( thGenTimer, GetTimeParameter( SRT_STREAM_RATE ) );
	ResetSecTimer( thTimerSec1, GetTimeParameter( SRT_STREAM_TIMEOUT ) );
	ResetTimer( thGenTimer1, GetTimeParameter( SRT_STREAM_RATE ) /2);
	ResetTimer( thGenTimer2, TENSION_SG_OFF_PERIOD );

	StartTimer( thGenTimer1 );
	StartTimer( thGenTimer2 );
	StartSecTimer( thTimerSec1 );

	SetStopStream(FALSE);                                   // reset the flag

//	SetRadioReceiverOff();
	radioRxOn=FALSE;


	ADS1220Powerdown(ADC_SWITCH_IN);	                    // disable ADC not used in Stream mode

	resetStrainGaugesReading();
	// read the SG a few times before sending the first data
	i=64;
	while(i--) {
		StrainGaugeRead();
	}
}   // end of InitStreamMode();

//------------------------------------------------------------------------------
//  Description: Send the Streaming Data every 10 ms
//	update Jan 30 2015	- Keep all ADCs On all the time. Keep toggling Radio receiver On/Off
//	update Feb 02 2015	- Keep the Radio always On.
//  update Feb 04 2015  - use moving average - StrainGaugeRead();
void StreamingTask(void) {

	if( TimerExpired( thGenTimer ) ) {
		ReadAccelCompass();
		ResetTimer( thGenTimer, GetTimeParameter( SRT_STREAM_RATE ) );
		ResetTimer( thGenTimer1, GetTimeParameter( SRT_STREAM_RATE ) >> 1 );
		readCompass = TRUE;
		SendStreamData();
	}

	if( GetStartStreamStatus() ) {        // PC needs to send Start Streaming command periodically
		SetStartStream(FALSE);
		ResetSecTimer( thTimerSec1, GetTimeParameter( SRT_STREAM_TIMEOUT ) );
	}

	StrainGaugeRead();
	EpsonGyroRead();

//	StrainGaugeReadInIdleMode();	// read the ADCs on a rotational basis

	if( readCompass ) {
		if( TimerExpired( thGenTimer1 ) ) {
			AccelCompassReadStart();
			readCompass = FALSE;
		}
	}

	if( SecTimerExpired( thTimerSec1 ) ) {                  // Need a command from the PC to keep streaming.
		currentState = ST_IDLE;                             // After set time, return to Idle mode
		InitIdleMode();
	}

	if( GetStopStreamStatus() ) {
		currentState = ST_IDLE;
		SetStopStream( OFF );
		InitIdleMode();
	}
}   // End of StreamingTask();

// Configure All ADCs for RFC mode:
//	ADC 0 to 6 (connected to strain gauges)- differential input AIN1 AIN2
//	ADC 7 (connected to Analog Gyro)- as temperature sensor
void InitAllStrainGaugeADC(void) {

	P11SEL |= BIT0;                                         // turn on the ADC CLK

	for( adcNumber = FIRST_SG_ADC; adcNumber <= LAST_SG_ADC; adcNumber++ ) {    // configure all ADCs connected to strain gauge
		analogChannel = ADS1220_IN_AIN1_AIN2;
		ADS1220Configure( analogChannel, adcNumber, idacCurrent[adcNumber] );
		ADS1220StartConversion( adcNumber );
	}
}

//------------------------------------------------------------------------------
// Power down MCU for until Timer A0 wakes it.
// Is called from IdleTask()
void setIdleSleep(void) {
    PowerDownRadio();

	StopWatchdog();
	SetLPMode( TRUE );                                      // Flag to tell Timer A0 interrupt what to do
	__bis_SR_register(LPM0_bits + GIE);                     // Enter LPM0 w/interrupt
	__no_operation();                                       // For debugger
	SetLPMode( FALSE );                                     // Upon wake
	SetWatchdog();

    WakeUpStGyro();                                         // after wake from sleep...
    digGyroOn = TRUE;

	initGaugesAfterIdleWake();
    RfcSent = FALSE;                                        // so we can enter 'if' statement below

}

//------------------------------------------------------------------------------
// measure the Strain gauge value and set the IDAC current based on the value.
//	For now the SG are either 5K or 1K but almost always 5k and will likely never change.
void MeasureStrainGaugeValueAndSetCurrent(void) {

	// configure all ADCs connected to strain gauge

	for(adcNumber = FIRST_SG_ADC; adcNumber<=LAST_SG_ADC; adcNumber++) {
		ADS1220Reset(adcNumber);
		analogChannel = ADS1220_IN_AIN0_AIN3;
		ADS1220ConfigureInSGTestMode(analogChannel,adcNumber, idacCurrent250ua);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );

		while(!TimerExpired( thGenTimer)) {
			ResetWatchdog();
		}
		ADS1220ReadAllRegs(adcNumber, bDataArray,sizeof(bDataArray));
		ADS1220StartConversion(adcNumber);
	}

	resetStrainGaugesReading();

	ResetTimer( thGenTimer,  SENSORS_READ_TIME);        // read the SG for 1000 ms
	while(!TimerExpired( thGenTimer)) {
		ResetWatchdog();
		StrainGaugeReadInIdleMode();
	}

	AverageADCResults();

	for(adcNumber = FIRST_SG_ADC; adcNumber<=LAST_SG_ADC; adcNumber++) {
		if( uvResultAverage[adcNumber] > SG_DIVIDER_VALUE ) {
			idacCurrent[adcNumber] = idacCurrent250ua;	// 5K Strain Gauge
		} else {
			idacCurrent[adcNumber] = idacCurrent1500ua; // 1K Strain Gauge
		}
	}

	idacCurrent[ADC_SWITCH_IN] = idacCurrent500ua;	// temperature sensor will be attached to this channel. Fix the current to 500uA.
}























/************************************
 ******* TEST FUNCTIONS**************
************************************/

#ifdef TEST_FUNCTIONS

void TestPowerDownMode(void)
{
	initMPS430Pins();
	EnableAllPower();
	Set_System_Clock();
	InitTimers();
	SysTimerConfig();
	__bis_SR_register(GIE);
	ReedSwitchInit();
	SetOutputPin(SW_LED, TRUE );
	InitRadioInterface();
	thGenTimer = RegisterTimer();
	ResetTimer( thGenTimer, 3000 );
	StartTimer( thGenTimer );
	while(1)
	{
		if(IsSwitchOn())
		{
	//		if(TimerExpired( thGenTimer))
	//		{
	//			SetSystemDeepSleep();
	//			initMPS430Pins();
	//			SetOutputPin(LED_CONTROL, FALSE );
	//			SetOutputPin(SW_LED, TRUE );
	//			Set_System_Clock();
	//			SysTimerConfig();
	//			ResetTimer( thGenTimer, 3000 );
	//			StartTimer( thGenTimer );
	//		}
		}
		else
		{
			if(TimerExpired( thGenTimer))
			{
				//DisableAllPower();
				DisableAnalogPower();
				DisableDigitalPower();
	//			DisableRadioPower();
				PowerDownRadio();
				DisableAnalogGyro();
				SetOutputPin(SW_LED, FALSE );

				//			EnableDigitalPower();
				SetSystemDeepSleep();
				initMPS430Pins();
				EnableAllPower();
				SetPinAsOutput(ZB_RSTn);
				SetOutputPin(ZB_RSTn, FALSE );

				SetOutputPin(SW_LED, TRUE );

				Set_System_Clock();
				InitTimers();
				SysTimerConfig();
				__bis_SR_register(GIE);
				ReedSwitchInit();
				InitRadioInterface();
				ResetTimer( thGenTimer, 3000 );
				StartTimer( thGenTimer );
				SetOutputPin(ZB_RSTn, TRUE );
			}

		}
	}
}



void TestAccelerometer(void)
{
	EnableAllPower();
	__delay_cycles(90000);
	InitAccelerometer();
	__delay_cycles(90000);
	AccelRegisterDump();
	while(AccelDataReady()==FALSE)
	{
		ResetWatchdog();
	}
	AccelGetRawData(bDataArray,sizeof(bDataArray));
	while(1)
	{
		AccelReadDataStart();
		while(AccelDataReady()==FALSE)
		{
			ResetWatchdog();
		}
		ReadAccelerometer();
		__delay_cycles(90000);
		ResetWatchdog();
	}


}



void TestGasGauge(void)
{
#if GAS_GAUGE_ENABLE
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE);
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	BatteryGasMonitorReadId();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	//InitBatteryGasMonitor();
	ResetBatteryGasMonitor();
	while(STC3100TxDone()==FALSE)
	{
		ResetWatchdog();
	}
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));

	while(1)
	{
		if(TimerExpired( thGenTimer))
		{
			BatteryGasMonitorReadAllRegs();
			while(STC3100DataReady()==FALSE)
			{
				ResetWatchdog();
			}
			STC3100GetRawData(bDataArray,sizeof(bDataArray));
			ParseGasGaugeData(bDataArray);
			ResetTimer( thGenTimer, 1000 );
			StartTimer( thGenTimer );
		}
		ResetWatchdog();
	}

#endif
}







void TestCompass(void)
{
	// test compass
	#if COMPASS_ENABLE
		ResetTimer( thGenTimer, 10 );
		StartTimer( thGenTimer );

		while(1)
		{
			if(TimerExpired( thGenTimer))
			{
				// reading data from the compass takes about 920us
//				SetOutputPin(P8_TP7, TRUE);
				CompassReadDataStart();
				while( CompassAccelDataReady() == FALSE )
				{
					ResetWatchdog();
				}
				CompassGetRawData(bDataArray,sizeof(bDataArray));
				sMagnetData.iXField[0] = bDataArray[0];
				sMagnetData.iXField[0] = (sMagnetData.iXField[0]<<8)|bDataArray[1];
				sMagnetData.iZField[0] = bDataArray[2];
				sMagnetData.iZField[0] = (sMagnetData.iZField[0]<<8)|bDataArray[3];
				sMagnetData.iYField[0] = bDataArray[4];
				sMagnetData.iYField[0] = (sMagnetData.iYField[0]<<8)|bDataArray[5];
//				SetOutputPin(P8_TP7, FALSE);
				ResetTimer( thGenTimer, 10 );
				StartTimer( thGenTimer );
			}
			ResetWatchdog();

		}
	#endif
	// end test compass
}




void TestStGyro(void)
{
	int gyroData[30];
	WORD timeBtwSamples[30];
	WORD timesToStable[30];
	unsigned int i,j,k;
	BOOL gyroStable;

	while(1)
	{
		for(i=0;i<30;i++)
		{

			PowerDownStGyro();
			__delay_cycles(40000);
			__delay_cycles(40000);

			SetSystemUTCTime( (DWORD) 0 );

			setSTreg1();
			setSTreg3();
			gyroStable = FALSE;
			j=0;
			k=0;
			while(k<5)
			{
				while(stGyroHasData()==FALSE);

				gyroStable = !stGyroReadData( &gyroData[i]);
				j++;
				if(gyroStable==TRUE)
					k++;
				else
					k=0;
			}
			timesToStable[i] = j;
			while(stGyroHasData()==FALSE);
			stGyroReadData( &gyroData[i]);

			timeBtwSamples[i] = (WORD)GetSystemUTCTime();

		}

		ResetWatchdog();


	}

}


void TestSerNumber(void)
{
	BYTE crcSnCalc;
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );

	while(1)
	{
		if(TimerExpired( thGenTimer))
		{
			SerNumberReadDataStart();
			while(SerNumberDataReady()==FALSE)
			{
				ResetWatchdog();
			}

			SerNumberGetRawData(bDataArray,sizeof(bDataArray));
			crcSnCalc=CalcSnCRC(bDataArray, 7);
			if(crcSnCalc == bDataArray[7])
			{
				SaveSerNumber(bDataArray);
			}
			ResetTimer( thGenTimer, 10 );
			StartTimer( thGenTimer );
		}
	}
}



//******************************************************************************
//
//  Function: TestSensorReadings
//
//  Arguments: void
//
//  Returns: WORD number of bad sensors.
//
//  Description: read all sensors 10 times, toggle the LDO if there is non responsive sensor,
//				and return the number of non responsive sensors after the last reading cycle
//
//******************************************************************************
void TestSensorReadings(void)
{
	WORD i,j;
	WORD sensorsNotSending;
	WORD wCounterR;
	j=10;
	sensorsNotSending = 0;
	EnableExtPower();
	SetOutputPin(RS485_PWR_EN, FALSE);
	UARTA3Init();
	UARTA3RxEnable();

	while(1)
	{
		// toggle the Sensor LDO - if a board doesn't send data
		j=0;
		if(sensorsNotSending>0)
		{
			sensorsNotSending = 0;
			SetOutputPin(RS485_PWR_EN, TRUE);
			__delay_cycles(16000);
			SetOutputPin(RS485_PWR_EN, FALSE);
		}

		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(EXT_DEV_EN, TRUE);
		SetOutputPin(EXT_DEV_ADD, FALSE);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(EXT_DEV_EN, FALSE);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		for(i=0;i<4;i++)
		{
			SetOutputPin(EXT_DEV_ADD, TRUE);
			__delay_cycles(40000);	//	wait for 12B to transfer - 1.15mS, 12B at 125Kbps will take 0.96mS		- 2ms
			SetOutputPin(EXT_DEV_ADD, FALSE);
			__delay_cycles(1600);	// add 0.2mS guard band

			wCounterR = ReadUartA3Data(pUartRxData);
			if(wCounterR==12)
			{
				j++;
				wCounterR=0;
				sensorsNotSending++;
			}
			else
			{
				wCounterR=1;
				sensorsNotSending++;
			}

			ResetWatchdog();
		}

	}


}

void TestMcuAdc(void)
{


	while(1)
	{
		__delay_cycles(80000);
		MeasureBatVoltageAdc();
	}
}

#endif


