//******************************************************************************
//
//  utils.c: Generic Application Utilities
//
//      Copyright (c) 2001-2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides any utility functions that are required by the system.
//  Most utility functions deal with string manipulation or number conversions.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-04-22     KM          Initial Version
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include "utils.h"



//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define DBG_TIME_PERIOD		50000

//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------
TIMERHANDLE thDebugTimer = 0;
//static BYTE bDataTemp[20];

static BYTE batType = 1;
//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: IsHexChar
//
//  Arguments:
//    IN  byChar - An ASCII character to test
//
//  Returns: TRUE if the char is a valid hex character, FALSE otherwise
//
//  Description: Checks that the passed char is 0-9, a-f, or A-F
//
//******************************************************************************
BOOL IsHexChar( BYTE byChar )
{
    if( ( byChar >= '0' ) && ( byChar <= '9' ) )
        return TRUE;

    if( ( byChar >= 'A' ) && ( byChar <= 'F' ) )
        return TRUE;

    if( ( byChar >= 'a' ) && ( byChar <= 'f' ) )
        return TRUE;

    // Fall through means the char passed is not a hex char
    return FALSE;
}

//******************************************************************************
//
//  Function: HexToByte
//
//  Arguments:
//    IN  aChar - hex char to convert
//
//  Returns: binary value of passed hex char
//
//  Description: Converts the passed hex char to its binary value
//
//******************************************************************************
BYTE HexToByte( char aChar )
{
    if( ( aChar >= '0' ) && ( aChar <= '9' ) )
        return aChar - '0';

    if( ( aChar >= 'a' ) && ( aChar <= 'f' ) )
        return aChar - 'a' + 10;

    if( ( aChar >= 'A' ) && ( aChar <= 'F' ) )
        return aChar - 'A' + 10;

    // Fall through means not a valid hex char.
    return 0;
}

//******************************************************************************
//
//  Function: AsciiToByte
//
//  Arguments:
//    IN  byChar - An ASCII character to convert from.
//
//  Returns: A byte value representation of the ASCII character on success.
//           0 on failure.
//
//  Description: This function converts the passed ASCII character
//               to a byte value representation (supports both hex and
//               decimal values).
//
//******************************************************************************
BYTE AsciiToByte( BYTE byChar )
{
    if( ( byChar >= '0' ) && ( byChar <= '9' ) )
        return ( byChar - '0' );

    if( ( byChar >= 'A' ) && ( byChar <= 'F' ) )
        return ( byChar - 'A' + 10 );

    if( ( byChar >= 'a' ) && ( byChar <= 'f' ) )
        return ( byChar - 'a' + 10 );

    // Fall through means the char passed is not a hex char
    return ( 0 );
}


//******************************************************************************
//
//  Function: CountDelimitersInString
//
//  Arguments:
//    IN  inputLine     - null terminated line to parse
//    IN  delimiter     - character delimiter
//
//  Returns: number of delimited strings in the passed inputLine
//
//  Description: This function counts how many substrings exist in
//               inputLine where the delimiter char defines each
//               substring. Note that back-to-back delimiters count
//               as a line (albeit a zero-length line).
//
//               Note: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//******************************************************************************
BYTE CountDelimitersInString( const char* inputLine, char delimiter ) {
    BYTE lineCount = 0;
    const char* pChar = inputLine;

    while( *pChar != '\0' )  {
        if( *pChar == delimiter )
            lineCount++;

        ++pChar;
    }
    return lineCount;
}

//******************************************************************************
//
//  Function: CountDelimitersInBuff
//
//  Arguments:
//    IN  inputBuffer   - array of bytes to check
//    IN  inputCount    - number of bytes in the array
//    IN  delimiter     - character delimiter
//
//  Returns: number of delimited strings in the passed inputLine
//
//  Description: This function counts how many substrings exist in
//               inputLine where the delimiter char defines each
//               substring. Note that back-to-back delimiters count
//               as a line (albeit a zero-length line).
//
//               Note: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//******************************************************************************
BYTE CountDelimitersInBuff( const BYTE* inputBuffer, WORD inputCount, char delimiter ) {
    BYTE lineCount = 0;
    WORD wIndex;

    for( wIndex = 0; wIndex < inputCount; wIndex++ )  {
        if( inputBuffer[wIndex] == delimiter )
            lineCount++;
    }
    return lineCount;
}

//******************************************************************************
//
//  Function: ParseItemsFromString
//
//  Arguments:
//    OUT parsedLines   - array of pointers to parsed strings in inputLine
//    IN  maxLines      - max nbr of items in parsedlines array
//    IN  inputLine     - null terminated line to parse
//    IN  delimiter     - character delimiter
//
//  Returns: the number of items parsed
//
//  Description: This function parses an input line into individual lines.
//               Each time the delimiter is encountered, a pointer to the
//               start of the characters is saved in the parsedLines array
//               and the delimiter is replaced by a null-term char. If
//               back-to-back delimiters, one of the parseLines pointers
//               will point to a zero-length string. At most maxLines are
//               parsed out of the inputLine.
//
//               Note 1: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//               Note 2: THIS FUNCTION CHANGES THE CONTENTS OF inputLine
//
//               Note 3: unused entries in parsedLines will be set to
//                       a NULL pointer
//
//******************************************************************************
BYTE ParseItemsFromString( PCHAR parsedLines[], BYTE maxLines, char* inputLine, char delimiter )
{
	WORD inputLength = 0;
    WORD inputOffset = 0;
    
    // Validate input first
    if( ( parsedLines == NULL ) || ( inputLine == NULL ) || ( maxLines == 0 ) )
        return 0;

    // Calculate the input line length


    while( inputLine[inputOffset] != '\0' )
    {
        inputLength++;
        inputOffset++;
    }

    // Now let ParseItemsFromBuff() do the actual work
    return ParseItemsFromBuff( parsedLines, maxLines, (BYTE*) inputLine, inputLength, delimiter );
}

//******************************************************************************
//
//  Function: ParseItemsFromBuff
//
//  Arguments:
//    OUT parsedLines   - array of pointers to parsed strings in inputLine
//    IN  maxLines      - max nbr of items in parsedlines array
//    IN  inputLine     - array of bytes to parse
//    IN  buffLen       - number of bytes in the input line
//    IN  delimiter     - character delimiter
//
//  Returns: the number of items parsed
//
//  Description: This function parses an input line into individual lines.
//               Each time the delimiter is encountered, a pointer to the
//               start of the characters is saved in the parsedLines array
//               and the delimiter is replaced by a null-term char. If
//               back-to-back delimiters, one of the parseLines pointers
//               will point to a zero-length string. At most maxLines are
//               parsed out of the inputLine.
//
//               Note 1: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//               Note 2: THIS FUNCTION CHANGES THE CONTENTS OF inputLine
//
//               Note 3: unused entries in parsedLines will be set to
//                       a NULL pointer
//
//******************************************************************************
BYTE ParseItemsFromBuff( PCHAR parsedLines[], BYTE maxLines, BYTE* inputLine, WORD inputLength, char delimiter )
{
	BYTE byIndex;
	BOOL delimIsEOL = FALSE;
	BYTE outputIndex = 0;  // Index into parsedLines[] array
    WORD inputOffset = 0;  // Index into inputLine[] array
    
    // Validate output buffer first. If it is null, there is not we can do
    if( ( parsedLines == NULL ) || ( maxLines == 0 ) )
        return 0;

    // Null terminate each of the parsedLine entries


    for( byIndex = 0; byIndex < maxLines; byIndex++ )
        parsedLines[byIndex] = NULL;

    // Continue validation
    if( ( parsedLines == NULL ) || ( inputLine == NULL ) )
        return 0;

    // Catch the special case of an empty line being passed to us
    if( inputLength == 0 )
        return 0;

    // If the delimiter is CR or LF, set a flag to watch for adjacent
    // LF or CR chars


    if( ( delimiter == '\r' ) || ( delimiter == '\n' ) )
        delimIsEOL = TRUE;

    // Now parsed the input line. By default, the first item
    // in the parsedLines[] array will point to the start of
    // the buffer.
    parsedLines[0] = (char*)inputLine;



    while( inputOffset < inputLength )
    {
        char currChar = inputLine[inputOffset];

        if( currChar == delimiter )
        {
            // We've hit a delimiter - replace it with a NULL.
            inputLine[inputOffset] = '\0';

            // First check for paired EOL delimiters.
            if( delimIsEOL )
            {
                // Step 1: clean up the current output line. Sub any paired delim
                // with a null-terminator.
                if( inputOffset > 0 )
                {
                    char prevChar = inputLine[inputOffset - 1];

                    if( ( ( delimiter == '\r' ) && ( prevChar == '\n' ) )
                            ||
                        ( ( delimiter == '\n' ) && ( prevChar == '\r' ) )
                      )
                    {
                        inputLine[inputOffset - 1] = '\0';
                    }
                }

                // Step 2: skip any following paired delim
                if( inputOffset + 1 < inputLength )
                {
                    char nextChar = inputLine[inputOffset + 1];

                    if( ( ( delimiter == '\r' ) && ( nextChar == '\n' ) )
                            ||
                        ( ( delimiter == '\n' ) && ( nextChar == '\r' ) )
                      )
                    {
                        inputOffset++;
                    }
                }
            }

            // This item has been parsed. Advance to the next one. If our output
            // array is full, quit now.
            outputIndex++;

            if( outputIndex == maxLines )
                break;

            // Init the next pointer (if we're not at the end of the buffer
            if( inputOffset + 1 < inputLength )
                parsedLines[outputIndex] = (PCHAR)&( inputLine[inputOffset+1] );
        }

        // Advance to next char
        inputOffset++;
    }

    // It is possible that the last byte in the buffer is not a delimiter, which
    // will result in the last string will not be null-terminated. If that is the
    // case, replace the last char in the buffer with a null (it means we lose that
    // last char).
    inputLine[inputLength-1] = '\0';

    return outputIndex;
}



//Converts 32 bit int to text. return char* - pointer to text buffer which is a file scope allocated array
void convInt32ToText( signed long inputValue, BYTE* pValueToTextBuffer ) {
    char *pLast;
    char *pFirst;
    char last;
    BYTE negative;
	signed long value;
	
    pLast = (char *)pValueToTextBuffer;

    // Record the sign of the value
    negative = (inputValue < 0);
    value = labs(inputValue);

    // Print the value in the reverse order
    do {
        *pLast = '0' + (BYTE)(value % 10);
        pLast++;
        value /= 10;
    } while (value);

    // Add the '-' when the number is negative, and terminate the string
    if (negative) *(pLast++) = '-';
    *(pLast--) = 0x00;

    // Now reverse the string
    pFirst = (char *)pValueToTextBuffer;
    while (pLast > pFirst) {
        last = *pLast;
        *(pLast--) = *pFirst;
        *(pFirst++) = last;
    }
}

// Converts 16 bit int to text return char* - pointer to text buffer which is a file scope allocated array
void convIntToText( int inputValue, BYTE* pValueToTextBuffer ) {
    char *pLast;
    char *pFirst;
    char last;
    BYTE negative;
	signed long value;
	
    pLast = (char *)pValueToTextBuffer;

    // Record the sign of the value
    negative = (inputValue < 0);
    value = abs(inputValue);

    // Print the value in the reverse order
    do {
        *pLast = '0' + (BYTE)(value % 10);
        pLast++;
        value /= 10;
    } while (value);

    // Add the '-' when the number is negative, and terminate the string
    if (negative) *(pLast++) = '-';
    *(pLast--) = 0x00;

    // Now reverse the string
    pFirst = (char *)pValueToTextBuffer;
    while (pLast > pFirst) {
        last = *pLast;
        *(pLast--) = *pFirst;
        *(pFirst++) = last;
    }
}

void RegisterDebugTimer(void) {
	if( thDebugTimer == TIMER_NOT_REGISTERED ) {
		thDebugTimer = RegisterTimer();
	}
}

void StartDebugTimer(void) {
	ResetTimer( thDebugTimer, DBG_TIME_PERIOD );
	StartTimer( thDebugTimer);
}

void StopDebugTimer(void) {
	StopTimer( thDebugTimer );
}

WORD GetDebugTimer(void) {
	WORD wTimeLeft;
	WORD wTimePassed;
	
	wTimeLeft = GetTimeLeft(thDebugTimer);
	wTimePassed = DBG_TIME_PERIOD - wTimeLeft;
	
	return wTimePassed;		
}

// Used at the end of every packet. A single byte that sets or determines
// a valid payload in the 802.15.4 packet.
BYTE CalculateCRC(BYTE *bPointer, WORD wLength) {
	const WORD c1 = 52845;
    const WORD c2 = 22719;
    WORD r;
    WORD i;
    DWORD sum;
	BYTE cipher;
	
	r = 55665;
	sum = 0;
	
	for( i=0; i<wLength;i++) {
		cipher = ( *bPointer ^ ( r >> 8 ) );
		r = ( cipher + r ) * c1 + c2;
		sum += cipher;
		bPointer++;		
	}
	
	return (BYTE)sum;
}

// Measure the center pin of the battery to determine the type and capacity of
// the inserted battery.
void EvalBatType(void) {
	WORD batVoltage[BAT_INPUTS];
	Adc12TakeSample();
	GetAdc12Results(batVoltage, sizeof(batVoltage)/sizeof(WORD));
	batType = BATTERY_LI;
	if( ( ( batVoltage[1] ) > ( batVoltage[0] / 3 - BAT_NIMH_MARGINE ) ) &&
			( ( batVoltage[1] ) < ( batVoltage[0] / 3 + BAT_NIMH_MARGINE ) ) ) {
		batType = BATTERY_NIMH;
	}
}

BYTE GetBatType(void) {
	return batType;
}




