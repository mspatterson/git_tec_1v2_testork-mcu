//******************************************************************************
//
//  WTTTSProtocolUtils.c: Communication protocol routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-March-26     DD        Initial Version
//	2013-Nov-04		DD 			Update RFC packet structure - RFC now reports voltage and mA/h used.
//******************************************************************************

#include "WTTTSProtocolUtils.h"

//#pragma package(smart_init)

// Private Declarations

// The WTTTS packet parser is table driven. To add a new command
// or response, do the following:
//
//   1. In the .h file, add a new WTTTS_CMD_ or WTTTS_RESP_ define
//      (if required)
//
//   2. Change the NBR_CMD_TYPES or NBR_RESP_TYPES define (if
//      a new cmd or response was defined).
//
//   3. If a new command or response has been defined, and that
//      item has a data payload, define a structure for the
//      payload in the .h file, and add a member of that structure
//      type to the WTTTS_DATA_UNION union definition.
//
//   4. Finally, add a row to (for new cmds or responses) or
//      modify an existing entry in teh m_pktInfo[] table.

#define NBR_CMD_TYPES  			9           // Total number of WTTTS_CMD_... defines
#define NBR_RESP_TYPES 			4           // Total number of WTTTS_RESP_... defines
#define RFC_RATE_DEFAULT		1000		// ms
#define RFC_TIMEOUT_DEFAULT		100			// ms
#define STREAM_RATE_DEFAULT		10			// ms
#define STREAM_TIMEOUT_DEFAULT	300		    // seconds
#define PAIR_TIMEOUT_DEFAULT	900		    // seconds
#define RF_PACKET_NUMBER		BUF_LENGTH

#define RFC_RATE_MIN		    10		    // ms
#define RFC_TIMEOUT_MIN		    3			// ms
#define STREAM_RATE_MIN		    9			// ms
#define STREAM_TIMEOUT_MIN	    5		    // seconds
#define PAIR_TIMEOUT_MIN	    10		    // seconds
#define SER_NUMBER_SIZE	8
typedef struct {
    BYTE pktHdr;                            // Expected header byte
    BYTE cmdRespByte;                       // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
    BYTE expDataLen;                        // Required value for header pktLen param
} WTTTS_PKT_INFO;

static const WTTTS_PKT_INFO m_pktInfo[ NBR_CMD_TYPES + NBR_RESP_TYPES ] = {
    { TEC_HDR_TX, PC_CMD_NO_CMD,           0 },
    { TEC_HDR_TX, PC_CMD_START_STREAM,     0 },
    { TEC_HDR_TX, PC_CMD_STOP_STREAM,      0 },
    { TEC_HDR_TX, PC_CMD_SET_RATE,         sizeof( WTTTS_RATE_PKT ) },
    { TEC_HDR_TX, PC_CMD_QUERY_VER,        0 },
    { TEC_HDR_TX, PC_CMD_ENTER_DEEP_SLEEP, 0 },
    { TEC_HDR_TX, PC_CMD_SET_RF_CHANNEL,   sizeof( WTTTS_SET_CHAN_PKT ) },
    { TEC_HDR_TX, PC_CMD_GET_CFG_DATA,     sizeof( WTTTS_CFG_ITEM )     },
    { TEC_HDR_TX, PC_CMD_SET_CFG_DATA,     sizeof( WTTTS_CFG_DATA )     },
    { PC_HDR_RX, TEC_RESP_STREAM_DATA,     sizeof( WTTTS_STREAM_DATA )  },
    { PC_HDR_RX, TEC_RESP_REQ_FOR_CMD,     sizeof( WTTTS_RFC_PKT )      },
    { PC_HDR_RX, TEC_RESP_CFG_DATA,        sizeof( WTTTS_CFG_DATA )     },
    { PC_HDR_RX, TEC_RESP_VER_PKT,         sizeof( WTTTS_VER_PKT )      },
};

static WORD wtttsTimeParams[NBR_SET_RATE_TYPES];

const WORD wtttsTimeParamsDefault[NBR_SET_RATE_TYPES]= {    // if not set in Info Memory, use these
		RFC_RATE_DEFAULT,
		RFC_TIMEOUT_DEFAULT,
		STREAM_RATE_DEFAULT,
		STREAM_TIMEOUT_DEFAULT,
		PAIR_TIMEOUT_DEFAULT,
};

const WORD wtttsTimeParamsMin[NBR_SET_RATE_TYPES]= {    // used to check incoming settings to insure valid
		RFC_RATE_MIN,
		RFC_TIMEOUT_MIN,
		STREAM_RATE_MIN,
		STREAM_TIMEOUT_MIN,
		PAIR_TIMEOUT_MIN,
};
//static WTTTS_RFC_PKT	wtttsRfcPacket;

static BYTE radioPacket[RF_PACKET_NUMBER];
//static BYTE wtttsPacket[RF_PACKET_NUMBER];
static BYTE *wtttsPacket;
static BYTE sequenceNumber;
static BOOL commNoCommand;
static BOOL commStartStream;
static BOOL commStopStream;
static BOOL commPowerDown;
static BYTE currMode =1;
static BYTE pageNumber;
static BYTE flashWriteResult;
static BOOL  havePkt = FALSE;
static BOOL  radioBusy = FALSE;                             // alerts system that radio is transceiving. (Different from active)
WTTTS_PKT_HDR* pktHdr;
BYTE* pktData;
WTTTS_CFG_DATA* cfgData;
WTTTS_CFG_ITEM* cfgItem;
static BYTE DeviceSerialNum[8];
static WORD tmpWord;

extern UVOLTS uvResultAverage[ADS1220_TOTAL];
extern signed long gyroSumAverage;
extern MAGNETIC_DATA	sMagnetData;
extern ACCELEROMETOR_DATA	sAccelData;

extern WORD missedRFC;                                      // temp var to measure missed packets
extern BYTE lostConnection;                                 // temp var to report lost / reconnects

extern WORD SamplesCounter[LAST_SG_ADC+2];

void word2ByteArray(void *pVoid, WORD wValue);

// This will take values from info memory that are set by the PC
// If the values have not been set (0xFF) the default values are used.
// Timing parameters are stored in a structure
void UpdateTimeParamsFromFlash(void) {
	WORD i;
	WORD wtttsTimes[NBR_SET_RATE_TYPES];
	BOOL varUpdated = FALSE;

	ReadPageFromFlash((BYTE) UPDATE_RATE_PAGE_IN_FLASH, (BYTE *)&wtttsTimes, sizeof(wtttsTimes));
	ResetWatchdog();
	for(i=0;i<NBR_SET_RATE_TYPES;i++) {
		if((wtttsTimes[i] == 0XFFFF)||(wtttsTimes[i] < wtttsTimeParamsMin[i])) { // if failed the test
			wtttsTimeParams[i] = wtttsTimeParamsDefault[i];                     // set to default
			varUpdated = TRUE;
		}
		else {
			wtttsTimeParams[i] = wtttsTimes[i];                                 // otherwise use set value
		}
	}
	ResetWatchdog();
	// if a variable has been updated write back to flash
	if(varUpdated) {
		WritePageInFlash((BYTE) UPDATE_RATE_PAGE_IN_FLASH, (BYTE *)&wtttsTimeParams, BYTES_IN_PAGE);
	}
}

BOOL SetTimeParameter(BYTE paramNum, WORD paramVal) {
	if( paramNum >= NBR_SET_RATE_TYPES )
		return FALSE;

	wtttsTimeParams[paramNum] = paramVal;

	WritePageInFlash( (BYTE)UPDATE_RATE_PAGE_IN_FLASH, (BYTE*)&wtttsTimeParams, BYTES_IN_PAGE);

	return TRUE;
}


WORD GetTimeParameter(const WTTTS_RATE_TYPE paramNum) {
	return wtttsTimeParams[paramNum];

}

// RFC packet:
// HEADER is 6 bytes
// RFC packet is 54 bytes
// CRC is 1 byte
// Total - 61 bytes
void SendRfc(void) {
	WTTTS_PKT_HDR*	pWtttsRfcHeader;
	WTTTS_RFC_PKT*	pWtttsRfcPacket;
	BYTE*	pWtttsRfcCRC;
//	DWORD pressure;
//	WORD wTemp;

	radioPacket[0] = TX_DATA;
	// Initialise the pointers
	wtttsPacket = &radioPacket[1];
	pWtttsRfcHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsRfcPacket = (WTTTS_RFC_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsRfcCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_RFC_PKT)];

	pWtttsRfcHeader->pktHdr = PC_HDR_RX;
	pWtttsRfcHeader->pktType = TEC_RESP_REQ_FOR_CMD;
	pWtttsRfcHeader->seqNbr = sequenceNumber++;
	pWtttsRfcHeader->dataLen = sizeof( WTTTS_RFC_PKT );

	word2ByteArray(&pWtttsRfcHeader->timeStamp,GetTenMsCount());

	pWtttsRfcPacket->temperature = GetTemperature();	// function to do
	pWtttsRfcPacket->battType = GetBatType();
	word2ByteArray(&pWtttsRfcPacket->battVoltage,GetBatVoltageAdc());
	word2ByteArray(&pWtttsRfcPacket->battUsed,GetBatUsedmAh());

//	pressure = 12345;						// function to do
//	pWtttsRfcPacket->pressure[0] = (BYTE)pressure;
//	pWtttsRfcPacket->pressure[1] = (BYTE)(pressure>>8);
//	pWtttsRfcPacket->pressure[2] = (BYTE)(pressure>>16);
	ConvertLongTo3Bytes(uvResultAverage[ADC_SWITCH_IN], pWtttsRfcPacket->pressure);

	pWtttsRfcPacket->rpm = GetRPM();
	pWtttsRfcPacket->lastReset	= GetLastReset();
	pWtttsRfcPacket->rfChannel = GetRfChannel();
	pWtttsRfcPacket->currMode = GetCurrMode();
	pWtttsRfcPacket->rfu = 0;

	word2ByteArray(&pWtttsRfcPacket->rfcRate,GetTimeParameter(SRT_RFC_RATE));
	word2ByteArray(&pWtttsRfcPacket->rfcTimeout,GetTimeParameter(SRT_RFC_TIMEOUT));
	word2ByteArray(&pWtttsRfcPacket->streamRate, GetTimeParameter(SRT_STREAM_RATE));
	word2ByteArray(&pWtttsRfcPacket->streamTimeout, GetTimeParameter(SRT_STREAM_TIMEOUT));
	word2ByteArray(&pWtttsRfcPacket->pairingTimeout, GetTimeParameter(SRT_PAIR_TIMEOUT));

	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_000], pWtttsRfcPacket->torque000);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_180], pWtttsRfcPacket->torque180);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_000], pWtttsRfcPacket->tension000);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_090], pWtttsRfcPacket->tension090);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_180], pWtttsRfcPacket->tension180);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_270], pWtttsRfcPacket->tension270);

	pWtttsRfcPacket->compassX[0] = (BYTE)sMagnetData.iXField[0];
	pWtttsRfcPacket->compassX[1] = (BYTE)(sMagnetData.iXField[0]>>8);
	pWtttsRfcPacket->compassY[0] = (BYTE)sMagnetData.iYField[0];
	pWtttsRfcPacket->compassY[1] = (BYTE)(sMagnetData.iYField[0]>>8);
	pWtttsRfcPacket->compassZ[0] = (BYTE)sMagnetData.iZField[0];
	pWtttsRfcPacket->compassZ[1] = (BYTE)(sMagnetData.iZField[0]>>8);

//	pWtttsRfcPacket->accelX[0] = (BYTE)SamplesCounter[0];
//	pWtttsRfcPacket->accelX[1] = (BYTE)(SamplesCounter[0]>>8);
//	pWtttsRfcPacket->accelY[0] = (BYTE)missedRFC; //(BYTE)SamplesCounter[2];
//	pWtttsRfcPacket->accelY[1] = (BYTE)missedRFC>>8; //(BYTE)(SamplesCounter[2]>>8);
//	pWtttsRfcPacket->accelZ[0] = lostConnection; //(BYTE)SamplesCounter[4];
//	pWtttsRfcPacket->accelZ[1] = 0; //(BYTE)(SamplesCounter[4]>>8);


	pWtttsRfcPacket->accelX[0] = (BYTE)sAccelData.iXField[0];
	pWtttsRfcPacket->accelX[1] = (BYTE)(sAccelData.iXField[0]>>8);
	pWtttsRfcPacket->accelY[0] = (BYTE)sAccelData.iYField[0];
	pWtttsRfcPacket->accelY[1] = (BYTE)(sAccelData.iYField[0]>>8);
	pWtttsRfcPacket->accelZ[0] = (BYTE)sAccelData.iZField[0];
	pWtttsRfcPacket->accelZ[1] = (BYTE)(sAccelData.iZField[0]>>8);

	*pWtttsRfcCRC = CalculateCRC( wtttsPacket, sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_RFC_PKT ) );

	ZbTiSendBinaryData( radioPacket,  sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_RFC_PKT ) + 2 );
}



BOOL HaveWtttsPacket(BYTE *pBuff, WORD buffLen) {
	   // Scans the passed buffer for a WTTTS command or response. Returns true if
	    // a packet is found, in which case pktHdr and pktData pointers are assigned.
	    // Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePkt = FALSE;

    if( buffLen < (WORD)MIN_WTTTS_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_WTTTS_PKT_LEN <= buffLen ) {
    		ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if( pBuff[bytesSkipped] != TEC_HDR_TX ) {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           //WTTTS_PKT_HDR* pHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );
           pktHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );

           for(iInfoItem = 0; iInfoItem < NBR_CMD_TYPES + NBR_RESP_TYPES; iInfoItem++ ) {

               if( m_pktInfo[iInfoItem].cmdRespByte != pktHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_WTTTS_PKT_LEN + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem ) {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )  {
               // Success!
              // memcpy( &pktHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
        	   pktHdr = (WTTTS_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 ) {
                  // memcpy( &pktData, &( pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ), dataLen );
            	   pktData = &pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePkt;
}

// If a command is read from the radio, it's processed here.
// Called from RfCommTask, which is called from Main.
void ExecuteCommand(void) {
	WORD wData;
	radioBusy = TRUE;
	ResetWatchdog();
	switch(pktHdr->pktType)
	{
		case PC_CMD_NO_CMD:
			SetNoCommand( TRUE );
			radioBusy = FALSE;
			break;
		case PC_CMD_START_STREAM:
			SetStartStream( TRUE );
			break;
		case PC_CMD_STOP_STREAM:
			SetStopStream( TRUE );
			break;
		case PC_CMD_SET_RATE:
			wData = pktData[3];
			wData <<= 8;
			wData |=pktData[2];
			SetTimeParameter( pktData[0], wData );
			break;
		case PC_CMD_QUERY_VER:
			SendVersionInfo();
			break;
		case PC_CMD_ENTER_DEEP_SLEEP:
			SetPowerDown( TRUE );
			break;
		case PC_CMD_SET_RF_CHANNEL:
			SetChangeChannelPending( pktData[0] );
			break;
		case PC_CMD_SET_CFG_DATA:
			cfgData = ( WTTTS_CFG_DATA* )pktData;
			pageNumber = cfgData->pageNbr;
			flashWriteResult = WritePageInFlash(pageNumber, cfgData->pageData, (WORD)NBR_BYTES_PER_WTTTS_PAGE);
			SendCfgEvent();
			break;
		case PC_CMD_GET_CFG_DATA:
			cfgItem = (WTTTS_CFG_ITEM*)pktData;
			pageNumber = cfgItem->pageNbr;
			SendCfgEvent();
			break;
		default:
			break;
	}
}


void SendVersionInfo(void) {
	WTTTS_PKT_HDR*	pWtttsHeader;
	WTTTS_VER_PKT*	pWtttsVerPacket;
	BYTE*	pWtttsCRC;

	// Initialise the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsVerPacket = (WTTTS_VER_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT)];

	pWtttsHeader->pktHdr = PC_HDR_RX;
	pWtttsHeader->pktType = TEC_RESP_VER_PKT;
	pWtttsHeader->seqNbr = sequenceNumber++;
	pWtttsHeader->dataLen = sizeof( WTTTS_VER_PKT );
//	pWtttsHeader->timeStamp = GetTenMsCount();
	word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());


	pWtttsVerPacket->hardwareSettings = GetHardwareRev();
	// the fw version is defined in builds.h
	pWtttsVerPacket->fwVer[0] = MAJOR_VERSION;
	pWtttsVerPacket->fwVer[1] = MINOR_VERSION;
	pWtttsVerPacket->fwVer[2] = BUILD_NUMBER;
	pWtttsVerPacket->fwVer[3] = REL_DBG;

	*pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT));

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT)+2);

}

void SendCfgEvent(void) {
	WTTTS_PKT_HDR*	pWtttsHeader;
	WTTTS_CFG_DATA*	pWtttsCfgPacket;
	BYTE*	pWtttsCRC;

	// Initialize the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsCfgPacket =(WTTTS_CFG_DATA*) &wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA)];

	pWtttsHeader->pktHdr = PC_HDR_RX;
	pWtttsHeader->pktType = TEC_RESP_CFG_DATA;
	pWtttsHeader->seqNbr = sequenceNumber++;
	pWtttsHeader->dataLen = sizeof( WTTTS_CFG_DATA );
//	pWtttsHeader->timeStamp = GetTenMsCount();
	word2ByteArray( &pWtttsHeader->timeStamp, GetTenMsCount() );

	pWtttsCfgPacket->pageNbr = pageNumber;
	pWtttsCfgPacket->result = ReadPageFromFlash( pageNumber, pWtttsCfgPacket->pageData, (WORD)NBR_BYTES_PER_WTTTS_PAGE );

	*pWtttsCRC = CalculateCRC( wtttsPacket, sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_CFG_DATA ) );

	ZbTiSendBinaryData( radioPacket,  sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_CFG_DATA ) + 2 );
}

// Stream Packet :
// Header - 6 bytes
// Stream Data - 34 bytes
// CRC - 1 byte
// Total - 41 bytes
void SendStreamData(void) {
	WTTTS_PKT_HDR*	pWtttsRfcHeader;
	WTTTS_STREAM_DATA*	pWtttsStreamPacket;
	BYTE*	pWtttsRfcCRC;

	// Initialize the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsRfcHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsStreamPacket = (WTTTS_STREAM_DATA*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsRfcCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA)];

	pWtttsRfcHeader->pktHdr = PC_HDR_RX;
	pWtttsRfcHeader->pktType = TEC_RESP_STREAM_DATA;
	pWtttsRfcHeader->seqNbr = sequenceNumber++;
	pWtttsRfcHeader->dataLen = sizeof( WTTTS_STREAM_DATA );
	//pWtttsRfcHeader->timeStamp = GetTenMsCount();	// function to do
	word2ByteArray(&pWtttsRfcHeader->timeStamp,GetTenMsCount());
	tmpWord = GetTenMsCount();

	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_000], pWtttsStreamPacket->torque000);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_180], pWtttsStreamPacket->torque180);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_000], pWtttsStreamPacket->tension000);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_090], pWtttsStreamPacket->tension090);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_180], pWtttsStreamPacket->tension180);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_270], pWtttsStreamPacket->tension270);

	ConvertLongTo4Bytes( gyroSumAverage, pWtttsStreamPacket->gyro );	// convert in mdeg for now

	pWtttsStreamPacket->compassX[0] = (BYTE)sMagnetData.iXField[0];
	pWtttsStreamPacket->compassX[1] = (BYTE)(sMagnetData.iXField[0]>>8);
	pWtttsStreamPacket->compassY[0] = (BYTE)sMagnetData.iYField[0];
	pWtttsStreamPacket->compassY[1] = (BYTE)(sMagnetData.iYField[0]>>8);
	pWtttsStreamPacket->compassZ[0] = (BYTE)sMagnetData.iZField[0];
	pWtttsStreamPacket->compassZ[1] = (BYTE)(sMagnetData.iZField[0]>>8);
	pWtttsStreamPacket->accelX[0] = (BYTE)sAccelData.iXField[0];
	pWtttsStreamPacket->accelX[1] = (BYTE)(sAccelData.iXField[0]>>8);
	pWtttsStreamPacket->accelY[0] = (BYTE)sAccelData.iYField[0];
	pWtttsStreamPacket->accelY[1] = (BYTE)(sAccelData.iYField[0]>>8);
	pWtttsStreamPacket->accelZ[0] = (BYTE)sAccelData.iZField[0];
	pWtttsStreamPacket->accelZ[1] = (BYTE)(sAccelData.iZField[0]>>8);

//	ConvertLongTo3Bytes(uvResultAverage[ADC_SWITCH_IN], pWtttsStreamPacket->adcSwIn);

//	tmpWord  %= 100;
//	if(tmpWord < 1)
//	{
//		*pWtttsRfcCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA));
//	}
//	else
//	{
//		*pWtttsRfcCRC = 0;
//		pWtttsRfcHeader->pktType = 0;
//	}

	*pWtttsRfcCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA));
	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA)+2);

}

void SetNoCommand(BOOL bVal) {
	commNoCommand = bVal;
}

BOOL GetCommandStatus(void) {
	return commNoCommand;
}
void SetStartStream(BOOL bVal) {
	commStartStream = bVal;
}

void SetStopStream(BOOL bVal) {
	commStopStream = bVal;
}

BOOL GetStartStreamStatus(void) {
	return commStartStream;
}

BOOL GetStopStreamStatus(void) {
	return commStopStream;
}

// Will set flag to tell system that it's in
void SetPowerDown(BOOL bVal) {
	commPowerDown = bVal;
}

BOOL GetPowerDownStatus(void)
{
	return commPowerDown;
}

// TO DO
BYTE GetCurrMode(void)
{
	return currMode;
}

void ConvertLongTo3Bytes(signed long slValue, BYTE *pData)
{
	if(slValue>INT24_MAX_POSITIVE)
		slValue = INT24_MAX_POSITIVE;

	if(slValue < INT24_MAX_NEGATIVE)
			slValue = INT24_MAX_NEGATIVE;

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);

}


void ConvertLongTo4Bytes(signed long slValue, BYTE *pData)
{

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);
	pData[3] = 	(BYTE) (slValue>>24);

}

void word2ByteArray(void *pVoid, WORD wValue)
{
	BYTE *pByte;
	WORD wTemp;
	wTemp = wValue;	// function to do
	pByte = (BYTE*)	pVoid;
	*pByte++ = (BYTE)wTemp;
	*pByte = (BYTE)(wTemp>>8);
}



BOOL GetRadioPacketStatus(void) {
	return havePkt;
}


void ClearRadioPacketStatus(void) {
	havePkt = FALSE;
}

BOOL IsRadioBusy(void)
{
	return radioBusy;
}

void SetRadioBusy(BOOL bValue) {
	radioBusy = bValue;
}

void SaveSerNumber(BYTE *pData) {
	WORD i;
	for( i=0; i < SER_NUMBER_SIZE; i++ ) {
		DeviceSerialNum[i] = *pData++;
	}
}
