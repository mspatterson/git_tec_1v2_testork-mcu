/*
 * ADC12.c
 *
 *  Created on: 2013-04-08
 *      Author: Owner
 */

#include "adc12.h"

static WORD AdcResult[ADC12_CHANNELS];
static WORD batVoltageAdcMv;

void Adc12Init(void) {

	P6SEL |= BIT0|BIT1|BIT2;	                            // Enable A/D channel A0, A1, A2
	/* Initialize REF module */
	// Enable 2.5V shared reference, disable temperature sensor to save power
	REFCTL0 |= REFMSTR+REFVSEL_2+REFON+REFTCOFF;

	/* Initialize ADC12 */
	ADC12CTL0 = ADC12ON+ADC12SHT02;                         // Turn on ADC12, set sampling time
	ADC12CTL1 = ADC12SHP;                                   // Use sampling timer
	ADC12MCTL0 = ADC12SREF_1;                               // Vr+=Vref+ and Vr-=AVss

	ADC12CTL0 = ADC12ON+ADC12MSC+ADC12SHT0_2;               // Turn on ADC12, set sampling time
//	ADC12CTL1 = ADC12SHP+ADC12CONSEQ_1;                     // Use sampling timer, single sequence
	ADC12CTL1 = ADC12SHP+ADC12DIV0+ADC12DIV1+ADC12CONSEQ_1;
	ADC12MCTL0 = ADC12INCH_0+ADC12SREF_1;                   // ref+=VREF+, channel = A0
	ADC12MCTL1 = ADC12INCH_1+ADC12SREF_1;                   // ref+=VREF+, channel = A1
	ADC12MCTL2 = ADC12INCH_2+ADC12EOS+ADC12SREF_1;          // ref+=VREF+, channel = A2

//	ADC12IE = 0x08;                                         // Enable ADC12IFG.3
//	__delay_cycles(75);                                     // 75 us delay @ ~1MHz
	__delay_cycles(800);                                    // 100 us delay @ ~8MHz
	ADC12CTL0 |= ADC12ENC;                                  // Enable conversions
}

void Adc12Disable(void) {

	P6SEL &= ~(BIT0|BIT1|BIT2);	// Disable A/D channel A0, A1, A2

	// Disable 2.5V shared reference,
	REFCTL0 = 0;

	/* Disable ADC12 */
	ADC12CTL0 = 0;           				                // Turn off ADC12,
	ADC12CTL1 = 0;                                          // Use sampling timer
	ADC12MCTL0 = 0;                                         // Vr+=Vref+ and Vr-=AVss
	ADC12MCTL1 = 0;                                         // ref+=VREF+, channel = A1
	ADC12MCTL2 = 0;                                         // ref+=VREF+, channel = A2
}

void Adc12StartConversion(void) {
	 ADC12CTL0 |= ADC12SC;                                  // Start conversion
}

BOOL IsAdc12ConversionDone(void) {
	//ADC12MEM2 interrupt flag. This bit is set when ADC12MEM2 is loaded with a
	//conversion result. This bit is reset if the ADC12MEM2 is accessed, or it may be reset with software.
	return (BOOL)(ADC12IFG & BIT2);

}

void Adc12TakeSample(void) {

	EnableBatSense();
	__delay_cycles(26000);                                  // This is required for settling time
	Adc12Init();
	__delay_cycles(1000);		                            // wait to stabilize = 5 * 1K0 * 100nF
	Adc12StartConversion();
	while(IsAdc12ConversionDone()==FALSE);
	AdcResult[0] = ADC12MEM0;
	AdcResult[1] = ADC12MEM1;
	AdcResult[2] = ADC12MEM2;
	DisableBatSense();
	Adc12Disable();
}

void GetAdc12Results(WORD * pWord, WORD maxSize) {
	unsigned  int i;
	for (i=0; i<ADC12_CHANNELS ;i++) {
		*pWord++ =  AdcResult[i];
		if(i>maxSize)
			break;
	}
}


void MeasureBatVoltageAdc(void) {

	DWORD batVoltageMv;
	WORD batVoltsAdc[BAT_INPUTS];

	Adc12TakeSample();
	GetAdc12Results(batVoltsAdc, sizeof(batVoltsAdc)/sizeof(WORD));

	batVoltageMv = ((DWORD)batVoltsAdc[0] * ADC12_COEFF_DIV10) / 100;	// convert in mV

	batVoltageAdcMv = (WORD)batVoltageMv*2;			        // resistor divider 2 to 1
}


WORD GetBatVoltageAdc(void) {
	return batVoltageAdcMv;
}

