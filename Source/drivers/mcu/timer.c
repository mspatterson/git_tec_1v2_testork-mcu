//******************************************************************************
//
//  TIMER.C: Module which supports timer services.
//
//  Copyright (c) Canrig Drilling
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//  Modified on:    By:         For:
//-------------------------------------------------------------
//  2011-04-21      JD          Mlynx
//
//******************************************************************************

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

#include "timer.h"
#include "..\WTTTSProtocolUtils.h"

// TIMER_TICKS_PER_SECOND indicates the timer update frequency
// This will normally be 1000 for 1 millisecond resolution
#define TIMER_TICKS_PER_SECOND    1000


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

// defines the parameters of the various system timers
typedef struct {
        BOOL bIsAllocated;
        BOOL bIsRunning;
        WORD wMillisecsLeft;
    } TIMERINFO;

typedef struct {
        BOOL bIsAllocated;
        BOOL bIsRunning;
        WORD wSecsLeft;
    } TIMERINFO_SEC;

extern WTTTS_STATES currentState;

// low power mode structure --- Now using a dedicated timer for RFC
//typedef struct {
//    	BOOL bLPMode;			// the system is in low power mode with timer running
//    	WORD wSecsLeft;	        // when tiLPMTimer expire the MCU will wake up
//    } LPM_INFO;

//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------

volatile TIMERINFO tiTimers[MAX_TIMERS];
volatile TIMERINFO_SEC tiSecTimers[MAX_TIMERS_SEC];
static volatile  BOOL  IsTimerInitialized = FALSE;
//static volatile DWORD TickCount  = 1;
static volatile DWORD UpTime     = 0;
static volatile DWORD UTCTime    = 0;
static volatile WORD  MsecsCount = 0;
static volatile WORD  MsecsCount1 = 0;
static volatile WORD  WaitCount  = 0;
static volatile WORD  TenMsecCount =0;
//static LPM_INFO lpVar;
static BOOL lowPowerFlag = FALSE;
static  WORD RFC_Time = 0;           // counts down the RTC time in Idle mode and will wake processor if sleeping

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

// static void SysTick_Handler(void); ---- This function was only called from the Timer A0 interrupt, so it was moved there.

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Set the uP to interrupt on Timer A0 every 1mS
// called from main and SystemInit
void	SysTimerConfig(void) {
	TA1CCTL0 = CCIE;                        // CCR0 interrupt enabled
	TA1CCR0 = 1000;
  	TA1CTL = TASSEL_2 +ID_3+ MC_1 + TACLR;  // SMCLK /8 , up-mode, clear TAR
  	__bis_SR_register(GIE);   
}

//------------------------------------------------------------------------------
// Disable 1mS timer on A0
// is called from SetSystemDeepSleep
void SysTimerDisable(void)
{
	__bic_SR_register(GIE);
	TA1CCTL0 = 0;                           // CCR0 interrupt enabled
	TA1CCR0 = 0;
  	TA1CTL = 0;                             // SMCLK /8 , up-mode, clear TAR
  	__bis_SR_register(GIE);
}

//------------------------------------------------------------------------------
// Timer A0 interrupt service routine is called every 1mS when enabled
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void) {
	//SysTick_Handler();
    BYTE iIndex;

    WaitCount++;        //increment the counter for WaitMsecs()
    MsecsCount++;       //increment mS counter for 1 second timer
    MsecsCount1++;      // increment mS counter for 10mS timer
    UTCTime++;
    if( MsecsCount >= 1000 ) {
        UpTime++;                       // number of seconds running
        MsecsCount = 0;
        for( iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex ++ ) {
            if( tiSecTimers[iIndex].bIsRunning  == TRUE ) {
               if( tiSecTimers[iIndex].wSecsLeft > 0 ) {
                    tiSecTimers[iIndex].wSecsLeft--;
               }
            }
        }
    }

    // 10mS counter
    if( MsecsCount1 >= 10 ) {
        TenMsecCount++;             // will wrap at 65535
        MsecsCount1 = 0;
    }

    // parse our timer array for running timers
    for( iIndex = 0; iIndex < MAX_TIMERS; iIndex ++ ) {
        if( tiTimers[iIndex].bIsRunning  == TRUE ) {
           if( tiTimers[iIndex].wMillisecsLeft > 0 ) {
                tiTimers[iIndex].wMillisecsLeft--;
           }
        }
    }

    // The "Request for Command" timing gets it's own dedicated timer cause it needs to wake the processor from sleep
    if( RFC_Time ) {
        RFC_Time--;
    } else {
        if( lowPowerFlag ) {                            // if IdleTask has set the uP in low power mode
            LPM0_EXIT;                                  // Macro that alters the register to wake the uP
            lowPowerFlag = FALSE;
        }
        if( currentState == ST_DEEP_SLEEP ) {
            RFC_Time = TEN_THOUSAND_MS;
        } else {
            RFC_Time = GetTimeParameter( SRT_RFC_RATE ) - RFC_OVERHEAD;    // Continually reset RFC timer
        }
    }
}

//------------------------------------------------------------------------------
//  Initializes timers, must be called before any other function in
//   this module. This function assumes that interrupts can be hooked.
//   The SysTick interrupt is configured and used for timer updates.
void InitTimers( void )
{
	unsigned int iIndex;

    UpTime      = 0;
    UTCTime     = 0;
    MsecsCount  = 0;
    WaitCount   = 0;
    TenMsecCount = 0;
    MsecsCount1 = 0;

        // Clear the timer info array.
    for( iIndex = 0; iIndex < MAX_TIMERS; iIndex ++ ) {     // for mS timers
        tiTimers[iIndex].bIsAllocated   = FALSE;
        tiTimers[iIndex].bIsRunning     = FALSE;
        tiTimers[iIndex].wMillisecsLeft = 0;
    }

	for ( iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex++ ) { // for 1 second timers
		tiSecTimers[iIndex].bIsAllocated = FALSE;
		tiSecTimers[iIndex].bIsRunning = FALSE;
		tiSecTimers[iIndex].wSecsLeft = 0;
	}
    // Setup the system tick timer
    // priority is set to lowest (31)
    SysTimerConfig();

    IsTimerInitialized = TRUE;
}

//  Returns number of msec that have passed since the module was initialized
//   resets to zero after 2^32 msec
//DWORD GetTickCount( void ) {
//    return TickCount;
//}

//------------------------------------------------------------------------------
//  Returns number of 10msec that have passed since the module was initialized
//   resets to zero after 655350 msec
WORD GetTenMsCount (void) {
	return TenMsecCount;
}

//------------------------------------------------------------------------------
// Returns the number of seconds since the system booted up
DWORD GetSystemUpTime( void )
{
	return UpTime;
}

//------------------------------------------------------------------------------
// This timer shadows the UTC time received from the RTC
DWORD GetSystemUTCTime( void ) {
	return UTCTime;
}

//------------------------------------------------------------------------------
void SetSystemUTCTime( DWORD newTime ) {
	// Sets the internal value of the UTC time shadow. No need
	// to disable interrupts here as the processor is a 32-bit
	// wide device and so the transfer is atomic.
	UTCTime = newTime;
}


//------------------------------------------------------------------------------
//  Waits for a given number of msec to pass before continuing on.
//   This function is meant for short waits. Otherwise the system can
//   stall here for up to 65 seconds
void WaitMsecs( WORD msecCount ) {
    WaitCount = 0;
    do {
    	ResetWatchdog();
    } while( WaitCount < msecCount );
}

//------------------------------------------------------------------------------
//  Returns: Number of new timer starting at 1. Returns 0 if no timers are available
TIMERHANDLE RegisterTimer( void ) {
    unsigned int iIndex;

    for( iIndex = 0; iIndex < MAX_TIMERS; iIndex++ ) {
        if( tiTimers[iIndex].bIsAllocated == FALSE ) {
            break;
        }
    }

    if( iIndex == MAX_TIMERS ) {
        return NULL;
    }

    tiTimers[iIndex].bIsRunning = FALSE;
    tiTimers[iIndex].bIsAllocated = TRUE;

    return iIndex + 1;
}

//------------------------------------------------------------------------------
// Returns the number of the timer starting at 1. Will return 0 if no timers available.
TIMERHANDLE RegisterSecTimer( void ) {
    unsigned int iIndex;

    for( iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex++ ) {
        if( tiSecTimers[iIndex].bIsAllocated == FALSE ) {
            break;
        }
    }

    if( iIndex == MAX_TIMERS_SEC ) {
        return NULL;
    }

    tiSecTimers[iIndex].bIsRunning = FALSE;
    tiSecTimers[iIndex].bIsAllocated = TRUE;

    return iIndex + 1;
}

//------------------------------------------------------------------------------
//  Unregister timer and return pass / fail
BOOL UnregisterTimer( TIMERHANDLE theHandle) {
	unsigned int iIndex;
	iIndex = (int)theHandle;
	if(IsTimerHandleValid(theHandle)) {
		tiTimers[iIndex-1].bIsRunning = FALSE;
    	tiTimers[iIndex-1].bIsAllocated = FALSE;
	} else {
		return NULL;
	}

    return TRUE;
}

//------------------------------------------------------------------------------
BOOL UnregisterSecTimer( TIMERHANDLE theHandle) {
	unsigned int iIndex;
	iIndex = (int)theHandle;
	if( IsSecTimerHandleValid( theHandle ) ) {
		tiSecTimers[iIndex-1].bIsRunning = FALSE;
    	tiSecTimers[iIndex-1].bIsAllocated = FALSE;
	}
	else {
		return NULL;
	}

    return TRUE;
}

//------------------------------------------------------------------------------
//  Initializes the passed timer to the value passed in milliseconds.
//    The timer state is not changed (eg if it was running, it is left running).
void ResetTimer( TIMERHANDLE thTimer, WORD wMillisecs )
{
    if( !IsTimerHandleValid( thTimer ) )
        return;

    tiTimers[thTimer-1].wMillisecsLeft = wMillisecs;
}

//------------------------------------------------------------------------------
void ResetSecTimer( TIMERHANDLE thTimer, WORD wSecs )
{
    if( !IsSecTimerHandleValid( thTimer ) )
        return;

    tiSecTimers[thTimer-1].wSecsLeft = wSecs;
}

//------------------------------------------------------------------------------
//  Description:  Starts the passed timer. Timer counts down from the
//                current/last count of the timer.
void StartTimer( TIMERHANDLE thTimer ) {
    if( !IsTimerHandleValid( thTimer ) )
        return;

    tiTimers[thTimer-1].bIsRunning = TRUE;
}

void StartSecTimer( TIMERHANDLE thTimer )
{
    if( !IsSecTimerHandleValid( thTimer ) )
        return;

    tiSecTimers[thTimer-1].bIsRunning = TRUE;
}

//------------------------------------------------------------------------------
//  Stops the passed timer. Does not clear or reset the timer value.
void StopTimer ( TIMERHANDLE thTimer )
{
    if( !IsTimerHandleValid( thTimer ) )
        return;

    tiTimers[thTimer-1].bIsRunning = FALSE;
}

//------------------------------------------------------------------------------
//  Finds the current running state of the passed timer returns true/false if timer running
//    returns false if timer not valid
BOOL TimerRunning( TIMERHANDLE thTimer )
{
    if( !IsTimerHandleValid( thTimer ) )
        return 0;

    return tiTimers[thTimer-1].bIsRunning;
}

//------------------------------------------------------------------------------
//  Returns the amount of time left on the timer or zero if not a valid timer
//  It does not reset, start, or stop the timer.
WORD GetTimeLeft( TIMERHANDLE thTimer ) {
	WORD TimeLeft;
	
    if( !IsTimerHandleValid( thTimer ) )
        return 0;

	TimeLeft = tiTimers[thTimer-1].wMillisecsLeft;
   return TimeLeft;
}

//  Checks if the passed timer has reached 0
//  Returns true/false if the timer expired

BOOL TimerExpired( TIMERHANDLE thTimer ) {
    // Returns TRUE if the specified timer has counted down to 0, if enabled.
    // Does not reset or restart the timer.

    if( !IsTimerHandleValid( thTimer ) )
        return FALSE;

    // Timer has expired if it is running and there are no
    // milliseconds left to count down.
    if( tiTimers[thTimer-1].bIsRunning && ( tiTimers[thTimer-1].wMillisecsLeft == 0 ) )
        return TRUE;

    // Fall through means the timer has not expired.
    return( FALSE );
}

// Return true if timer has counted down to 0. If not or timer not valid returns false
BOOL SecTimerExpired( TIMERHANDLE thTimer ) {
    // Returns TRUE if the specified timer has expired. Does
    // not reset or restart the timer though.
    if( !IsSecTimerHandleValid( thTimer ) )
        return FALSE;

    // Timer has expired if it is running and there are no
    // milliseconds left to count down.
    if( tiSecTimers[thTimer-1].bIsRunning && ( tiSecTimers[thTimer-1].wSecsLeft == 0 ) )
        return TRUE;

    // Fall through means the timer has not expired.
    return( FALSE);
}

// This was used to start a timer for how long the uP should sleep. However,
// the philosophy has changed. The timer is now set inside Timer A0 interrupt
// now it just sets the flag to tell A0 timer interrupt what to do
void SetLPMode( BOOL flag ) {
    //lpVar.bLPMode = TRUE;
    lowPowerFlag = flag;
    //lpVar.wSecsLeft = mSec;
}

//  the system timer is running 50 times slower in low power mode
void SysTimerLPMConfig(void) {
    TA1CCTL0 = CCIE;                                // CCR0 interrupt enabled
    TA1CCR0 = 50000;
    TA1CTL = TASSEL_2 +ID_3+ MC_1 + TACLR;          // SMCLK /8 , upmode, clear TAR
    __bis_SR_register(GIE);
}

// RFC timer is forced to restart, even though it auto updates in the Timer A0 interrupt.
void resetRFCTimer(void) {
    RFC_Time = GetTimeParameter( SRT_RFC_RATE );
}
//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------


//  Arguments: TIMERHANDLE  thTimer - handle for the timer to be checked
//
//  Description:  Returns TRUE if the passed handle is valid, FALSE otherwise.
//              Handle is invalid if it falls outside the timer info array range.

BOOL IsTimerHandleValid( TIMERHANDLE thAHandle ) {
    if( thAHandle == 0  || thAHandle > MAX_TIMERS ) {
        return FALSE;
    }

    // Handle is invalid if timer not allocated for that handle.

    if( tiTimers[thAHandle-1].bIsAllocated == FALSE ) {
        return FALSE;
    }

    // Fall through means timer handle is valid.

    return TRUE;
}

// Returns true if timer exists
BOOL IsSecTimerHandleValid( TIMERHANDLE thAHandle ) {
    if( thAHandle == 0  || thAHandle > MAX_TIMERS_SEC ) {
        return FALSE;
    }

    // Handle is invalid if timer not allocated for that handle.

    if( tiSecTimers[thAHandle-1].bIsAllocated == FALSE ) {
        return FALSE;
    }

    // Fall through means timer handle is valid.

    return TRUE;
}

//  Description:  Decrements the milliseconds left on any timer that has been
//                allocated and is running. This function gets called once
//                every millisecond from the 1 ms tick interrupt

// This function was only called from the Timer A0 interrupt so it was just moved there
//void SysTick_Handler(void) {
//    BYTE iIndex;
//
//    WaitCount++;        //increment the counter for WaitMsecs()
//    MsecsCount++;       //increment mS counter for 1 second timer
//    MsecsCount1++;      // increment mS counter for 10mS timer
//    UTCTime++;
//    if( MsecsCount >= 1000 ) {
//    	UpTime++;                       // number of seconds running
//    	MsecsCount = 0;
//        for( iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex ++ ) {
//            if( tiSecTimers[iIndex].bIsRunning  == TRUE ) {
//               if( tiSecTimers[iIndex].wSecsLeft > 0 ) {
//                    tiSecTimers[iIndex].wSecsLeft--;
//               }
//            }
//        }
//    }
//
//    // 10mS counter
//    if( MsecsCount1 >= 10 ) {
//    	TenMsecCount++;             // will wrap at 65535
//       	MsecsCount1 = 0;
//    }
//
//    // parse our timer array for running timers
//    for( iIndex = 0; iIndex < MAX_TIMERS; iIndex ++ ) {
//        if( tiTimers[iIndex].bIsRunning  == TRUE ) {
//           if( tiTimers[iIndex].wMillisecsLeft > 0 ) {
//                tiTimers[iIndex].wMillisecsLeft--;
//           }
//        }
//    }
//}


