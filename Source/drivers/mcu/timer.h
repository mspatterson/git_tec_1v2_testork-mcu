//************************************************************************
//
//  TIMER.H: Public entry points and data definitions for accessing
//           timer services.
//
//  Copyright (c) 2001, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************


#ifndef _TIMER_H

    #define _TIMER_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "..\drivers.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------


#define MAX_TIMERS      	8
#define MAX_TIMERS_SEC      2

#define RFC_OVERHEAD        2                               // Subtracted from RFC value to account for time to reset timer

//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------

typedef BYTE TIMERHANDLE;

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------

//  Description: set TIMER A1 to interrupt every 1msec 
void	SysTimerConfig(void);


//  Description: disable TIMER A1
void SysTimerDisable(void);

//  Description: counts the number of msec since the module was initialized
//              resets to zero after 2^32 msec
DWORD GetTickCount( void );

//  Description: counts the number of secs since the module was initialized
DWORD GetSystemUpTime( void );

//  Description: maintains a UTC timer that gets updated by GPS messages
DWORD GetSystemUTCTime( void );

//  Description: updates the system-based UTC time with the value passed
void SetSystemUTCTime( DWORD newTime );

//  Description: waits for a given number of msec to pass before continuing on.
//               This function is ment for short waits. Otherwise the system can
//               stall here for up to 65 seconds
void WaitMsecs( WORD msecCount );

TIMERHANDLE RegisterTimer( void );  // allocates a timer to a specific process. Returns NULL if there are no timers available

//  Description: Initializes the passed timer to the value passed in milliseconds.
//             The timer state is not changed (eg if it was running, it is left running).
void ResetTimer( TIMERHANDLE thTimer, WORD wMillisecs );


void resetRFCTimer(void);   // will force RFC timer to start over. Used when entering IdleMode

//  Description:  Starts the passed timer. Timer counts down from the
//                current/last count of the timer.
void StartTimer( TIMERHANDLE thTimer );

void StopTimer ( TIMERHANDLE thTimer );     // Stops the passed timer. Does not clear or reset the timer value.

BOOL TimerRunning( TIMERHANDLE thTimer );   // finds the current running state of the specified timer

//  Description: returns how much time is left on a given timer
//              It does not reset, start, or stop the timer.
WORD GetTimeLeft( TIMERHANDLE thTimer );

BOOL TimerExpired( TIMERHANDLE thTimer );   // returns TRUE if a given timer has reached 0

//  Description:  Returns TRUE if the passed handle is valid, FALSE otherwise.
// Handle is invalid if it falls outside the timer info array range.
BOOL IsTimerHandleValid( TIMERHANDLE thAHandle );

// Initializes timers, must be called before any other function in
//  this module. This function assumes that interrupts can be hooked.
//  The SysTick interrupt is configured and used for timer updates.
void InitTimers( void  );

BOOL UnregisterTimer( TIMERHANDLE theHandle);   // Remove timer

WORD GetTenMsCount (void);  // returns the how much time has passed in 10mS blocks. Resets after 655350mS

TIMERHANDLE RegisterSecTimer( void );   // if available, a timer is given a number

BOOL UnregisterSecTimer( TIMERHANDLE theHandle);

void StartSecTimer( TIMERHANDLE thTimer );

void ResetSecTimer( TIMERHANDLE thTimer, WORD wSecs );

BOOL SecTimerExpired( TIMERHANDLE thTimer );

BOOL IsSecTimerHandleValid( TIMERHANDLE thAHandle );

void SetLPMode( BOOL flag );

void SysTimerLPMConfig(void);

#endif  // _TIMER_H













