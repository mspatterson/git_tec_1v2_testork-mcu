/*
 * i2c.c
 *
 *  Created on: 2012-07-20
 *      Author: DD
 */

#include "i2c.h"

#define I2C_READ	1
#define I2C_WRITE	0

static BOOL Read_nWrite; 	// 0 - write; 1 - read 
static I2C_OBJECT *pI2Cobj;

void i2cB1Initialize(BYTE address) {
	P3SEL |= 1<<7;                                          // Assign I2C SDA P3.7 pins to USCI_B1
	P5SEL |= 1<<4;                                          // Assign I2C SCL P5.4 pins to USCI_B1
	UCB1CTL1 |= UCSWRST;                                    // Enable SW reset
	UCB1CTL0 = UCMST + UCMODE_3 + UCSYNC;                   // I2C Master, synchronous mode
	UCB1CTL1 = UCSSEL_2 + UCSWRST;                          // Use SMCLK, keep SW reset
//	UCB1BR0 = 80;                                           // fSCL = SMCLK/80 = ~100kHz
	UCB1BR0 = 21;                                           // fSCL = SMCLK/22 = ~380kHz
	UCB1BR1 = 0;
	UCB1I2CSA = address;                                    // Slave Address
	UCB1CTL1 &= ~UCSWRST;                                   // Clear SW reset, resume operation
}

void i2cWrite( I2C_OBJECT *pI2C_obj ) {
	pI2Cobj = pI2C_obj;
	i2cB1Initialize( pI2Cobj->bySlaveAddress );
	Read_nWrite = I2C_WRITE;
	UCB1IE |= UCTXIE;                                       // Enable TX interrupt
	UCB1CTL1 |= UCTR + UCTXSTT;                             // I2C TX, start condition
	
}

void i2cRead( I2C_OBJECT *pI2C_obj ) {
	pI2Cobj = pI2C_obj;
	i2cB1Initialize(pI2Cobj->bySlaveAddress);
	Read_nWrite = I2C_READ;
	UCB1IE |= UCTXIE;                                       // Enable TX interrupt
	UCB1CTL1 |= UCTR + UCTXSTT;                             // I2C TX, start condition
}


//------------------------------------------------------------------------------
// The USCI_B1 data ISR is used to move received data from the I2C slave
// to the MSP430 memory. 
#pragma vector = USCI_B1_VECTOR
__interrupt void USCI_B1_ISR(void) {
  switch( __even_in_range( UCB1IV, 12 ) ) {
  case  0: break;                                           // Vector  0: No interrupts
  case  2: break;                                           // Vector  2: ALIFG
  case  4: break;                                           // Vector  4: NACKIFG
  case  6: break;                                           // Vector  6: STTIFG
  case  8: break;                                           // Vector  8: STPIFG
  case 10:                                                  // Vector 10: RXIFG
		if( pI2Cobj->wRxQIndex < pI2Cobj->wRxQSize ) {
  			*pI2Cobj->ptrRxQ++ = UCB1RXBUF;
  			pI2Cobj->wRxQIndex++;
  			if(pI2Cobj->wRxQIndex == (pI2Cobj->wRxQSize -1))// Only one byte left?
			
  				UCB1CTL1 |= UCTXSTP;                        // Generate I2C stop condition after sending the byte
  				
  		}
  		
  		// if all bytes has been received
  		if( pI2Cobj->wRxQIndex == pI2Cobj->wRxQSize ) {
  			*pI2Cobj->ptrRxQ = UCB1RXBUF;
  			UCB1IFG &= ~UCRXIFG;                            // Clear USCI_B1 RX int flag
      		UCB1IE &= ~UCRXIE;					            // disable RX interrupt
      		pI2Cobj->bRxQReceiving = FALSE;
  		}
	
    break; 
  case 12: 									                // Vector 12: TXIFG
  	if ( Read_nWrite == I2C_WRITE ) {
//  		if(pI2Cobj->wTxQIndex <= pI2Cobj->wTxQSize)
  		if(pI2Cobj->wTxQIndex < pI2Cobj->wTxQSize) {
  			UCB1TXBUF = *pI2Cobj->ptrTxQ++;
  			pI2Cobj->wTxQIndex++;
  		} else {
  			UCB1CTL1 |= UCTXSTP;                            // I2C stop condition
      		UCB1IFG &= ~UCTXIFG;                            // Clear USCI_B1 TX int flag
      		UCB1IE &= ~UCTXIE;					            // disable TX interrupt
      		pI2Cobj->bTxQSending = FALSE;
  		}
  	} else {
  		if(pI2Cobj->wTxQIndex < pI2Cobj->wTxQSize) {
  			UCB1TXBUF = *pI2Cobj->ptrTxQ++;
  			pI2Cobj->wTxQIndex++;
  		} else {
  			pI2Cobj->bTxQSending = FALSE;
  			UCB1IFG &= ~UCTXIFG;                  	        // Clear USCI_B1 TX int flag
      		UCB1IE &= ~UCTXIE;					  	        // disable TX interrupt
      		UCB1CTL1 &= ~UCTR;						        // set in RX mode
      		UCB1CTL1 |= UCTXSTT;  					        // I2C start
      		UCB1IE |= UCRXIE;                               // Enable RX interrupt
  		}
  	}
    break;                           		
  default: break; 
  }
}



