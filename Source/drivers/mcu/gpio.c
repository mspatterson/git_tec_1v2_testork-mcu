
// Description:      initialize the MSP430 pins

#include "gpio.h"

// Direction registers table
static volatile BYTE * pPortDirRegs[] = {
	&P1DIR,
	&P2DIR,
	&P3DIR,
	&P4DIR,
	&P5DIR,
	&P6DIR,
	&P7DIR,
	&P8DIR,
	&P9DIR,
	&P10DIR,
	&P11DIR
};
// Output registers table
static volatile BYTE * pPortOutRegs[] = {
	&P1OUT,
	&P2OUT,
	&P3OUT,
	&P4OUT,
	&P5OUT,
	&P6OUT,
	&P7OUT,
	&P8OUT,
	&P9OUT,
	&P10OUT,
	&P11OUT

};

// Input registers table
static volatile BYTE * pPortInRegs[] = {
	&P1IN,
	&P2IN,
	&P3IN,
	&P4IN,
	&P5IN,
	&P6IN,
	&P7IN,
	&P8IN,
	&P9IN,
	&P10IN,
	&P11IN,

};

// Selection register table
static volatile BYTE * pPortSelRegs[] = {
	&P1SEL,
	&P2SEL,
	&P3SEL,
	&P4SEL,
	&P5SEL,
	&P6SEL,
	&P7SEL,
	&P8SEL,
	&P9SEL,
	&P10SEL,
	&P11SEL

};

// Resistor Enable register table
static volatile BYTE * pPortRenRegs[] = {
	&P1REN,
	&P2REN,
	&P3REN,
	&P4REN,
	&P5REN,
	&P6REN,
	&P7REN,
	&P8REN,
	&P9REN,
	&P10REN,
	&P11REN

};

// set the corresponding pin as peripheral
// The pin is defined by port (upper nibble where 0 is port 1) and Pin (lower nibble)
void SetPinAsPeripheral(BYTE pinName) {
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS) {
		*(pPortSelRegs[portNumber]) |= bitMask;
	}
}

// set the corresponding pin as output
// The pin is defined by port (upper nibble where 0 is port 1) and Pin (lower nibble)
void SetPinAsOutput(BYTE pinName) {
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS) {
		*(pPortSelRegs[portNumber]) &= ~bitMask;
		*(pPortDirRegs[portNumber]) |= bitMask;
	}
}

// Description:      set the corresponding pin as input
// The pin is defined by port (upper nibble where 0 is port 1) and Pin (lower nibble)
void SetPinAsInput(BYTE pinName) {
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS) {
		*(pPortSelRegs[portNumber]) &= ~bitMask;        // Turn off special function settings
		*(pPortDirRegs[portNumber]) &= ~bitMask;        // Set pin as input
	}
}

// set the corresponding pin logic level
// The pin is defined by port (upper nibble where 0 is port 1) and Pin (lower nibble)
void SetOutputPin(BYTE pinName, BOOL bState) {
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS){
		if (bState == TRUE) {
			*(pPortOutRegs[portNumber]) |= bitMask;         // Set pin high
		} else {
			*(pPortOutRegs[portNumber]) &= ~bitMask;        // Set pin low
		}
	}
}

// Read the logic value of the corresponding pin
// The pin is defined by port (upper nibble where 0 is port 1) and Pin (lower nibble)
BOOL ReadInputPin(BYTE pinName) {
	BYTE portNumber;
	BYTE pinNumber;
	volatile BYTE bitMask;
	volatile BYTE bState;
	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;                         // Shift pin bit into place

	if( portNumber < TOTAL_PORT_NUMBERS) {          // port 1 is represented by 0, 2 by 1
		bState = *pPortInRegs[portNumber];          // get port pins via register address
		bState &= bitMask;                          // Mask out all but the one we need

		if (bState) return ON;                      // if remaining bit is high
	}
	return OFF;                                     // if value is low or out of range
}

// Description:      set the corresponding pin logic level
// The pin is defined by port (upper nibble where 0 is port 1) and Pin (lower nibble)
void EnableInputPullUp(BYTE pinName) {
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS) {
		*(pPortRenRegs[portNumber]) |= bitMask;     // set pull-up on specified pin
		*(pPortOutRegs[portNumber]) |= bitMask;     // must be output by definition
	}
}

// initialize the MSP430 pin-out to default state
// by writing directly to the port special function register defined in msp430f5419a.h
void initMPS430Pins( void ) {
	P8OUT  = PORT_8_VALUE_INT;
    P8DIR  = PORT_8_DIR_INIT;
    P8SEL  = PORT_8_PERIPH_SEL;

	P1OUT  = PORT_1_VALUE_INT;
	P1DIR  = PORT_1_DIR_INIT;
    P1SEL  = PORT_1_PERIPH_SEL;
        
    P2DIR  = PORT_2_DIR_INIT;
    P2OUT  = PORT_2_VALUE_INT;
    P2SEL  = PORT_2_PERIPH_SEL;
    
    P3DIR  = PORT_3_DIR_INIT;
    P3OUT  = PORT_3_VALUE_INT;
    P3SEL  = PORT_3_PERIPH_SEL;
    
    P4DIR  = PORT_4_DIR_INIT;
    P4OUT  = PORT_4_VALUE_INT;
    P4SEL  = PORT_4_PERIPH_SEL;
    
    P5DIR  = PORT_5_DIR_INIT;
    P5OUT  = PORT_5_VALUE_INT;
    P5SEL  = PORT_5_PERIPH_SEL;

    P6DIR  = PORT_6_DIR_INIT;
    P6OUT  = PORT_6_VALUE_INT;
    P6SEL  = PORT_6_PERIPH_SEL;

    P7DIR  = PORT_7_DIR_INIT;
    P7OUT  = PORT_7_VALUE_INT;
    P7SEL  = PORT_7_PERIPH_SEL;

//    P8OUT  = PORT_8_VALUE_INT;
//    P8DIR  = PORT_8_DIR_INIT;
//    P8SEL  = PORT_8_PERIPH_SEL;

    P9DIR  = PORT_9_DIR_INIT;
    P9OUT  = PORT_9_VALUE_INT;
    P9SEL  = PORT_9_PERIPH_SEL;

    P10DIR  = PORT_10_DIR_INIT;
    P10OUT  = PORT_10_VALUE_INT;
    P10SEL  = PORT_10_PERIPH_SEL;

    P11DIR  = PORT_11_DIR_INIT;
    P11OUT  = PORT_11_VALUE_INT;
    P11SEL  = PORT_11_PERIPH_SEL;
}  

// initialize the MSP430 pin-out in power down mode
// June 2019 BDD, Changed most pins to inputs instead of outputs (set low)
// Leaving ST gyro on caused a big current draw otherwise for "Wake-on-Turns"
void SetMPS430PinsInPowerDown( void ) {

    P1DIR  = PORT_1_DIR_POWER_DOWN;
    P1OUT  = PORT_1_VALUE_POWER_DOWN;
    P1SEL  = PORT_1_PERIPH_SEL;
        
    P2DIR  = PORT_2_DIR_POWER_DOWN;
    P2OUT  = PORT_2_VALUE_POWER_DOWN;
    P2SEL  = PORT_2_PERIPH_SEL;
    
    P3DIR  = PORT_3_DIR_POWER_DOWN;
    P3OUT  = PORT_3_VALUE_POWER_DOWN;
    P3SEL  = PORT_3_PERIPH_SEL;
    
    P4DIR  = PORT_4_DIR_POWER_DOWN;
    P4OUT  = PORT_4_VALUE_POWER_DOWN;
    P4SEL  = PORT_4_PERIPH_SEL;
    
    P5DIR  = PORT_5_DIR_POWER_DOWN;
    P5OUT  = PORT_5_VALUE_POWER_DOWN;
    P5SEL  = PORT_5_PERIPH_SEL;

    P6DIR  = PORT_6_DIR_POWER_DOWN;
    P6OUT  = PORT_6_VALUE_POWER_DOWN;
    P6SEL  = PORT_6_PERIPH_SEL;

    P7DIR  = PORT_7_DIR_POWER_DOWN;
    P7OUT  = PORT_7_VALUE_POWER_DOWN;
    P7SEL  = PORT_7_PERIPH_SEL;

    P8DIR  = PORT_8_DIR_POWER_DOWN;
    P8OUT  = PORT_8_VALUE_POWER_DOWN;
    P8SEL  = PORT_8_PERIPH_SEL;

    P9DIR  = PORT_9_DIR_POWER_DOWN;
    P9OUT  = PORT_9_VALUE_POWER_DOWN;
    P9SEL  = PORT_9_PERIPH_SEL;

    P10DIR  = PORT_10_DIR_POWER_DOWN;
    P10OUT  = PORT_10_VALUE_POWER_DOWN;     // Leave EN_VD for ST gyro and Wake-on-Turns
    P10SEL  = PORT_10_PERIPH_SEL;

    P11DIR  = PORT_11_DIR_POWER_DOWN;
    P11OUT  = PORT_11_VALUE_POWER_DOWN;
    P11SEL  = PORT_11_PERIPH_SEL;
}  

