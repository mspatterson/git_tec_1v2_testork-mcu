//******************************************************************************
//
//  flash.c: flash memory routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-03-13     DD          Initial Version
//
//******************************************************************************

#include "flash.h"



//******************************************************************************
//
//  Function: ReadFlash
//
//  Arguments: pointer to store the data, number of bytes to read
//
//  Returns: none
//
//  Description: read wLength number of bytes from the information memory
//
//******************************************************************************

void ReadFlash(WORD wSegmentAddress, BYTE *pData, WORD wLength)
{
	unsigned int i;
	char *Flash_ptr;

	Flash_ptr = (char *) wSegmentAddress;      // Initialise Flash segment C ptr

	for (i = 0; i < wLength; i++)
	{
		*pData++ = *Flash_ptr++;          // copy value
	}

}


//******************************************************************************
//
//  Function: WriteFlash
//
//  Arguments: pointer to the data to be written , number of bytes
//
//  Returns: none
//
//  Description: write wLength number of bytes to the information memory
//
//******************************************************************************

void WriteFlash(WORD wSegmentAddress, BYTE *pData, WORD wLength)
{
	unsigned int i;
	char * Flash_ptr;                         // Initialize Flash pointer
	Flash_ptr = (char *) wSegmentAddress;

	ResetWatchdog();
	__disable_interrupt();                  // 5xx Workaround: Disable global
											// interrupt while erasing. Re-Enable
											// GIE if needed
	FCTL3 = FWKEY;                            // Clear Lock bit
	FCTL1 = FWKEY+ERASE;                      // Set Erase bit
	ResetWatchdog();
	*Flash_ptr = 0;                           // Dummy write to erase Flash seg
	FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation

//	ResetWatchdog();
	for (i = 0; i < wLength; i++)
	{
		*Flash_ptr++ = *pData++;              // Write value to flash
		ResetWatchdog();
	}

	FCTL1 = FWKEY;                           // Clear WRT bit
	FCTL3 = FWKEY+LOCK;                      // Set LOCK bit

	__enable_interrupt();					// enable interrupts
}















