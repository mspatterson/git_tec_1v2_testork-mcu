/*
 * spi.c
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#include "spi.h"

BOOL SpiA0Initialize(void)
{
	SetPinAsPeripheral(ADC_SCLK);
	SetPinAsPeripheral(ADC_DIN);
	SetPinAsPeripheral(ADC_DOUT);

	UCA0CTL1 |= UCSWRST; // **Put state machine in reset**
	UCA0CTL0 |= UCMST + UCSYNC + UCMSB; // 3-pin, 8-bit SPI master, Clock polarity low, MSB

	UCA0CTL1 |= UCSSEL_2; 				// SMCLK
	UCA0BR0 = 16; // /2
	UCA0BR1 = 0; //
	UCA0MCTL = 0; // No modulation
	UCA0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
//	UCA0IE |= UCRXIE; // Enable USCI_A0 RX interrupt
	return TRUE;

}

//////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			SpiA0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
//!
//!	\brief		writes and reads bytes  the SPI interface
//!
//!	\note 		the chip must be selected from the calling function
//!
//!	\param[in]	pDataBuffer		pointer to Data to write and then store the response
//!
//!	\param[in]	wLength			Number of bytes to w/r
//!
//!	\param[out]	void
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void SpiA0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
{

	WORD cnt;
	for( cnt = 0; cnt < wLength; cnt++ )
		{
			while( (UCA0IFG&UCTXIFG) == 0 );

			UCA0TXBUF = pDataBuffer[cnt];

			while( (UCA0IFG&UCRXIFG) == 0 );

			pDataBuffer[cnt] = UCA0RXBUF;
		}

	while( (UCA0IFG&UCTXIFG) == 0 );

}

BOOL SpiB0Initialize(void)
{
	SetPinAsPeripheral(L3G_SCLK);
	SetPinAsPeripheral(L3G_SDO);
	SetPinAsPeripheral(L3G_SDI);

	UCB0CTL1 |= UCSWRST; // **Put state machine in reset**
	UCB0CTL0 |= UCMST + UCSYNC + UCMSB+UCCKPL; // 3-pin, 8-bit SPI master, Clock polarity high, MSB

	UCB0CTL1 |= UCSSEL_2; 				// SMCLK
	UCB0BR0 = 4; // /2
	UCB0BR1 = 0; //
	UCB0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
//	UCA0IE |= UCRXIE; // Enable USCI_A0 RX interrupt
	return TRUE;

}

//////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			SpiA0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
//!
//!	\brief		writes and reads bytes  the SPI interface
//!
//!	\note 		the chip must be selected from the calling function
//!
//!	\param[in]	pDataBuffer		pointer to Data to write and then store the response
//!
//!	\param[in]	wLength			Number of bytes to w/r
//!
//!	\param[out]	void
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void SpiB0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
{

	WORD cnt;
	for( cnt = 0; cnt < wLength; cnt++ )
		{
			while( (UCB0IFG & UCTXIFG) == 0 );

			UCB0TXBUF = pDataBuffer[cnt];

			while( (UCB0IFG & UCRXIFG) == 0 );

			pDataBuffer[cnt] = UCB0RXBUF;
		}

	while( (UCB0IFG & UCTXIFG) == 0 );

}


BOOL SpiA3Initialize(void)
{
//		SetPinAsOutput(ADIS_SCLK);
//	SetPinAsOutput(ADIS_DIN);
//	SetPinAsInput(ADIS_DOUT);
//	SetPinAsPeripheral(ADIS_SCLK);
//	SetPinAsPeripheral(ADIS_DIN);
//	SetPinAsPeripheral(ADIS_DOUT);
//
//
//	UCA3CTL1 |= UCSWRST; // **Put state machine in reset**
//	UCA3CTL0 |= UCMST + UCSYNC + UCMSB+UCCKPL; // 3-pin, 8-bit SPI master, Clock polarity high, MSB
//
//	UCA3CTL1 |= UCSSEL_2; 				// SMCLK
//	// set the SPI CLK
//	UCA3BR0 = 250; // (32 -> 250KHz)	// 250 -31KHz works fine
//	UCA3BR1 = 0; //
//	UCA3MCTL = 0; // No modulation
//	UCA3CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
////	UCA0IE |= UCRXIE; // Enable USCI_A0 RX interrupt
	return TRUE;

}

//////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			SpiA0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
//!
//!	\brief		writes and reads bytes  the SPI interface
//!
//!	\note 		the chip must be selected from the calling function
//!
//!	\param[in]	pDataBuffer		pointer to Data to write and then store the response
//!
//!	\param[in]	wLength			Number of bytes to w/r
//!
//!	\param[out]	void
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void SpiA3TxRxPacket(BYTE *pDataBuffer, WORD wLength)
{

	WORD cnt;
	for( cnt = 0; cnt < wLength; cnt++ )
		{
			while( (UCA3IFG&UCTXIFG) == 0 );

			UCA3TXBUF = pDataBuffer[cnt];

			while( (UCA3IFG&UCRXIFG) == 0 );

			pDataBuffer[cnt] = UCA3RXBUF;
		}

	while( (UCA3IFG&UCTXIFG) == 0 );

}



