#ifndef BUILDS_H_
#define BUILDS_H_

#define RELEASE                 0
#define DEBUG	                1
#define MAJOR_VERSION 	        2
#define MINOR_VERSION 	        0
#define BUILD_NUMBER  	        48
#define REL_DBG			        DEBUG

#define WATCHDOG_ENABLE	        1

//#define DBG_MESSAGES	        1
#define ZB_MODULE_ENABLE		1
#define COMPASS_ENABLE			1
#define ST_GYRO_ENABLE			1
#define SWITCH_ON_OFF			1
#define GAS_GAUGE_ENABLE		1
//#define ACCELEROMETER_ENABLE	1

//#define TEST_FUNCTIONS		1
 
//#define DBG_MESSAGES_ADC	1
#endif /*BUILDS_H_*/

