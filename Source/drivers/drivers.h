/*
 * drivers.h
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#ifndef DRIVERS_H_
#define DRIVERS_H_
#include "msp430F5419a.h"

#include "typedefs.h"
#include <string.h>
#include <stdlib.h>

#include "mcu\system.h"
#include "mcu\spi.h"
#include "mcu\gpio.h"
#include "mcu\uart.h"
#include "mcu\timer.h"
#include "mcu\i2c.h"
#include "mcu\flash.h"
#include "mcu\adc12.h"

#include "board\ads1220.h"                              // 24 bit Analog to Digital Converters
#include "board\ext_interface.h"
#include "board/CC2530_Radio.h"                         // Texas Instruments 802.15.4 radio
#include "board\board_gpio.h"
#include "board/FXOS8700CQ.h"                           // Compass & Accelerometer
#include "board/L3G4200D.h"                             // Digital High Speed Gyro
#include "board/M41T62.h"                               // Real Time Clock
#include "board/STC3100.h"                              // Coulomb Counter
#include "board/DS28CM00.h"                             // Serial number chip (not really used)...

#include "utils.h"
#include "builds.h"
#include "DataManager.h"
#include "common_defines.h"
#include "WTTTSProtocolUtils.h"

#endif /* DRIVERS_H_ */
