/* compass.c
 *
 *  Created on: 2012-07-25
 *      Author: DD
 *  Renamed FXOS8700CQ.C and included the Accelerometer
 *      Nov 2019
 *      Brian Dewald
 */
 
#include <Source/drivers/board/FXOS8700CQ.h>


I2C_OBJECT	i2cFXO;
BYTE RxCompassBuffer[COMPASS_BUF_SIZE];
BYTE TxCompassBuffer[COMPASS_BUF_SIZE];
TIMERHANDLE thCompassTimer;
MAGNETIC_DATA   sMagnetData;
ACCELEROMETOR_DATA  sAccelData;

const BYTE CompassDataRate[] = {
		COMP_M_ODR_800HZ,
		COMP_M_ODR_400HZ,
		COMP_M_ODR_200HZ,
		COMP_M_ODR_100HZ,
		COMP_M_ODR_50HZ,
		COMP_M_ODR_12HZ,
		COMP_M_ODR_6HZ,
		COMP_M_ODR_1HZ
};

int	CompassData[3];
 
static BYTE bDataArray[30] = {0};

I2C_OBJECT  i2cAccel;
BYTE RxAccelBuffer[ACCEL_BUF_SIZE];
BYTE TxAccelBuffer[ACCEL_BUF_SIZE];

int AccelData[3];
/*
The accelerometer readings are a 14-bit reading "left-justified" in a 16 bit space,
so you have to right shift it by 4. It isn't explicitly said in the data sheet,
but you can infer it from a couple of inconsistencies in the data sheet and experimentation.
*/

//------------------------------------------------------------------------------
//  This function configures the FXOS8700 into 200 Hz hybrid mode,
//   meaning that both accelerometer and magnetometer data are provided at the 200 Hz rate.
void AccelAndCompassInit(void) {

    memset(RxCompassBuffer, 0, sizeof(RxCompassBuffer));
    memset(TxCompassBuffer, 0, sizeof(TxCompassBuffer));

    FXOReadId();
    while( AccelCompassDataReady() == FALSE );

    // write 0000 0000 = 0x00 to accelerometer control register 1 to place FXOS8700 into standby
    // [7-1] = 0000 000
    // [0]: active=0
    i2cFXO.ptrTxQ = TxCompassBuffer;
    i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 2;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 0;
    i2cFXO.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = FXO_CTRL_REG1_ADDR;
    TxCompassBuffer[1] = 0x00;
    i2cWrite( &i2cFXO );
    __delay_cycles( 10000 );

    // write 0001 1111 = 0x1F to magnetometer control register 1
    // [7]: m_acal=0: auto calibration disabled
    // [6]: m_rst=0: no one-shot magnetic reset
    // [5]: m_ost=0: no one-shot magnetic measurement
    // [4-2]: m_os=111=7: 8x over-sampling (for 200Hz) to reduce magnetometer noise
    // [1-0]: m_hms=11=3: select hybrid mode with accel and magnetometer active
    i2cFXO.bySlaveAddress = FXO_ADDRESS;
    i2cFXO.ptrTxQ = TxCompassBuffer;
    i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 2;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 0;
    i2cFXO.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;
    TxCompassBuffer[1] = 0x1F;
    i2cWrite( &i2cFXO );
    __delay_cycles( 10000 );

    // write 0010 0000 = 0x20 to magnetometer control register 2
    // [7]: reserved
    // [6]: reserved
    // [5]: hyb_autoinc_mode=1 to map the magnetometer registers to follow the
    // accelerometer registers
    // [4]: m_maxmin_dis=0 to retain default min/max latching even though not used
    // [3]: m_maxmin_dis_ths=0
    // [2]: m_maxmin_rst=0
    // [1-0]: m_rst_cnt=00 to enable magnetic reset each cycle
    i2cFXO.bySlaveAddress = FXO_ADDRESS;
    i2cFXO.ptrTxQ = TxCompassBuffer;
    i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 2;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 0;
    i2cFXO.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_M_CTRL_REG2_ADDR;
    TxCompassBuffer[1] = 0x20;
    i2cWrite( &i2cFXO );
    __delay_cycles( 10000 );


    // write 0000 0001= 0x01 to XYZ_DATA_CFG register
    // [7]: reserved
    // [6]: reserved
    // [5]: reserved
    // [4]: hpf_out=0
    // [3]: reserved
    // [2]: reserved
    // [1-0]: fs=01 for accelerometer range of +/-4g range with 0.488mg/LSB
    i2cFXO.bySlaveAddress = FXO_ADDRESS;
    i2cFXO.ptrTxQ = TxCompassBuffer;
    i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 2;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 0;
    i2cFXO.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_XYZ_DATA_CFG;
    TxCompassBuffer[1] = 0x01;
    i2cWrite( &i2cFXO );
    __delay_cycles( 10000 );

    // write 0000 1101b = 0x0D to accelerometer control register 1
    // [7-6]: aslp_rate=00
    // [5-3]: dr=001 for 200Hz data rate (when in hybrid mode)
    // [2]: lnoise=1 for low noise mode
    // [1]: f_read=0 for normal 16 bit reads
    // [0]: active=1 to take the part out of standby and enable sampling
    i2cFXO.bySlaveAddress = FXO_ADDRESS;
    i2cFXO.ptrTxQ = TxCompassBuffer;
    i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 2;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 0;
    i2cFXO.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = FXO_CTRL_REG1_ADDR;
    TxCompassBuffer[1] = 0x0D;
    i2cWrite( &i2cFXO );
    __delay_cycles( 10000 );
}

//------------------------------------------------------------------------------
// Start the read sequence for the accelerometer and compass data
//  Will load 6 Words into 12 bytes starting with Status
void AccelCompassReadStart(void) {

    i2cFXO.bySlaveAddress = FXO_ADDRESS;
    i2cFXO.ptrTxQ = TxCompassBuffer;
    i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 1;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 13;
    i2cFXO.bRxQReceiving = TRUE;

    TxCompassBuffer[0] = FXO_STATUS;
//    SetOutputPin(TP2, TRUE);
    i2cRead( &i2cFXO );
}

void clearMagnetData(void) {
    memset( &sMagnetData, 0, sizeof( sMagnetData ) );
    memset( &sAccelData, 0, sizeof( sAccelData ) );
}

//------------------------------------------------------------------------------
// This will get the latest readings from the FXOS8700 after the call to
//  AccelCompassReadStart() has had time to save a value.
//  Accelerometer data needs to be shifted to remove unused bits.
BOOL ReadAccelCompass(void)
{
    if( AccelCompassDataReady() == TRUE )  {
        AccelCompassGetRawData( bDataArray, sizeof( bDataArray ) );

        sAccelData.iXField[0] = ( bDataArray[1] << 8 ) | bDataArray[2];
        sAccelData.iYField[0] = ( bDataArray[3] << 8 ) | bDataArray[4];
        sAccelData.iZField[0] = ( bDataArray[5] << 8 ) | bDataArray[6];
        sMagnetData.iXField[0] = ( bDataArray[7] << 8 ) | bDataArray[8];
        sMagnetData.iYField[0] = ( bDataArray[9] << 8 ) | bDataArray[10];
        sMagnetData.iZField[0] = ( bDataArray[11] << 8 ) | bDataArray[12];


        sAccelData.iXField[0] = AccelDataShift( sAccelData.iXField[0] );
        sAccelData.iYField[0] = AccelDataShift( sAccelData.iYField[0] );
        sAccelData.iZField[0] = AccelDataShift( sAccelData.iZField[0] );

        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
// Remove the unused lower 2 bits that the FXOS8700 sends out.
//  A negative number needs to be preserved.
int AccelDataShift(signed int reading) {

    reading >>= 2;                                          // shift out unused bits

    if( reading & 0x2000 ) {                                // if was a negative number
        reading |= 0xC000;                                  // make value negative again
    }
    return reading;
}

BOOL AccelCompassDataReady(void) {
	return ( !i2cFXO.bRxQReceiving );
}

//------------------------------------------------------------------------------
// Built in ID of the FXOS8700. Just a way to verify the device and that it's working
void FXOReadId(void) {

	i2cFXO.bySlaveAddress = FXO_ADDRESS;
	i2cFXO.ptrTxQ = TxCompassBuffer;
   	i2cFXO.wTxQIndex = 0;
    i2cFXO.wTxQSize = 1;
    i2cFXO.bTxQSending = TRUE;
    i2cFXO.ptrRxQ = RxCompassBuffer;
    i2cFXO.wRxQIndex = 0;
    i2cFXO.wRxQSize = 2;
    i2cFXO.bRxQReceiving = TRUE;

    TxCompassBuffer[0] = FXO_WHO_AM_I_ADDR;		    // address of the first config register
    i2cRead( &i2cFXO );
}

//------------------------------------------------------------------------------
// copy the compass & accelerometer data into the given buffer
void AccelCompassGetRawData(BYTE *pData, WORD maxSize) {
    unsigned int i=0;
    while( (i < 13 ) && ( i < maxSize ) ) {
        pData[i] = RxCompassBuffer[i];
        i++;
    }
}

//------------------------------------------------------------------------------
// Set the FXOS8700 in Standby mode. Uses 2uA
void AccelCompassDisable(void) {

	i2cFXO.bySlaveAddress = FXO_ADDRESS;
	i2cFXO.ptrTxQ = TxCompassBuffer;
	i2cFXO.wTxQIndex = 0;
	i2cFXO.wTxQSize = 2;
	i2cFXO.bTxQSending = TRUE;
	i2cFXO.ptrRxQ = RxCompassBuffer;
	i2cFXO.wRxQIndex = 0;
	i2cFXO.wRxQSize = 0;
	i2cFXO.bRxQReceiving = FALSE;

	TxCompassBuffer[0] = FXO_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = STANDBY_MODE;
    i2cWrite( &i2cFXO );
    __delay_cycles( 10000 );
}


//----------------------------------Archived Code-------------------------------

//void CompassInit(void) {
//
//    // clear the buffers
//    memset( &RxCompassBuffer, 0, sizeof( RxCompassBuffer ) );
//
//    i2cCompass.bySlaveAddress = FXO_ADDRESS;
//
//    i2cCompass.ptrTxQ = TxCompassBuffer;
//    i2cCompass.wTxQIndex = 0;
//    i2cCompass.wTxQSize = 4;
//    i2cCompass.bTxQSending = TRUE;
//    i2cCompass.ptrRxQ = RxCompassBuffer;
//    i2cCompass.wRxQIndex = 0;
//    i2cCompass.wRxQSize = 0;
//    i2cCompass.bRxQReceiving = FALSE;
//
//    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;
//    TxCompassBuffer[1] = COMP_MAGN_ONLY_ON;
//    TxCompassBuffer[2] = 0x10;
//    TxCompassBuffer[3] = 0x80;
//
//    i2cWrite( &i2cCompass );
//    __delay_cycles( 10000 );                                // wait for i2c write to complete (1.2mS is enough for 45 bytes)
//
//    i2cCompass.ptrTxQ = TxCompassBuffer;
//    i2cCompass.wTxQIndex = 0;
//    i2cCompass.wTxQSize = 2;
//    i2cCompass.bTxQSending = TRUE;
//    i2cCompass.ptrRxQ = RxCompassBuffer;
//    i2cCompass.wRxQIndex = 0;
//    i2cCompass.wRxQSize = 0;
//    i2cCompass.bRxQReceiving = FALSE;
//
//    TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
//    TxCompassBuffer[1] = COMP_M_ODR_100HZ | COMP_ACTIVE_MODE;
//    i2cWrite( &i2cCompass );
//    __delay_cycles( 10000 );                                // wait for i2c write to complete (1.2mS is enough for 45 bytes)
//}

//void CompassInit(BYTE number)
//{
//  BYTE i=0;
//
//  if(thCompassTimer ==0)
//  {
//      thCompassTimer = RegisterTimer();
//  }
//
//  ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
//  StartTimer( thCompassTimer );
////    if(thCompassTimer)
////    {
////        while(!TimerExpired( thCompassTimer));
////    }
////    else
////    {
////        __delay_cycles(400000);
////    }
////    UnregisterTimer(thCompassTimer);
//
//  __delay_cycles(1000);
//  // clear the buffers
//  for(i=0;i<sizeof(RxCompassBuffer);i++)
//  {
//      RxCompassBuffer[i]=0;
//      TxCompassBuffer[i]=0;
//  }
//
//  switch(number)
//  {
//      case 0:
//          i2cCompass.bySlaveAddress = FXO_ADDRESS;
//          break;
//      case 1:
//          i2cCompass.bySlaveAddress = COMPASS_ADDRESS_2;
//          break;
//      default:
//          break;
//  }
//
//  i2cCompass.ptrTxQ = TxCompassBuffer;
//      i2cCompass.wTxQIndex = 0;
//    i2cCompass.wTxQSize = 2;
//    i2cCompass.bTxQSending = TRUE;
//    i2cCompass.ptrRxQ = RxCompassBuffer;
//    i2cCompass.wRxQIndex = 0;
//    i2cCompass.wRxQSize = 0;
//    i2cCompass.bRxQReceiving = FALSE;
//
//
//    TxCompassBuffer[0] = M_CTRL_REG1_ADDR;
//  TxCompassBuffer[1] = MAGN_ONLY_ON;
//
//    i2cCompassWrite(&i2cCompass);
////    ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
////    while(!TimerExpired( thCompassTimer));
//    __delay_cycles(10000);
//  i2cCompass.ptrTxQ = TxCompassBuffer;
//  i2cCompass.wTxQIndex = 0;
//  i2cCompass.wTxQSize = 2;
//  i2cCompass.bTxQSending = TRUE;
//  i2cCompass.ptrRxQ = RxCompassBuffer;
//  i2cCompass.wRxQIndex = 0;
//  i2cCompass.wRxQSize = 0;
//  i2cCompass.bRxQReceiving = FALSE;
//
//
//  TxCompassBuffer[0] = CTRL_REG1_ADDR;
//  TxCompassBuffer[1] = M_ODR_100HZ | ACTIVE_MODE;
//    i2cCompassWrite(&i2cCompass);
////    ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
////    while(!TimerExpired( thCompassTimer));
//    __delay_cycles(10000);
//
//}

//void CompassReadDataStart(void) {
//
//  i2cCompass.bySlaveAddress = FXO_ADDRESS;
//  i2cCompass.ptrTxQ = TxCompassBuffer;
//      i2cCompass.wTxQIndex = 0;
//    i2cCompass.wTxQSize = 1;
//    i2cCompass.bTxQSending = TRUE;
//    i2cCompass.ptrRxQ = RxCompassBuffer;
//    i2cCompass.wRxQIndex = 0;
//    i2cCompass.wRxQSize = 6;
//    i2cCompass.bRxQReceiving = TRUE;
//
//    TxCompassBuffer[0] = COMP_M_OUT_X_MSB_ADDR;
////    SetOutputPin(TP2, TRUE);
//    i2cRead( &i2cCompass );
//}

//BOOL CompassTxDone(void) {
//  return ( !i2cFXO.bTxQSending );
//}

////// return the address of the compass data
////BYTE* CompassGetDataPointer(void) {
////    return RxCompassBuffer;
////}
//
//int* CompassGetDataPointerInt(void) {
//  return (int*)RxCompassBuffer;
//}

//void CompassPowerDown(void) {
//
//  i2cFXO.bySlaveAddress = FXO_ADDRESS;
//  i2cFXO.ptrTxQ = TxCompassBuffer;
//  i2cCompass.wTxQIndex = 0;
//  i2cCompass.wTxQSize = 2;
//  i2cCompass.bTxQSending = TRUE;
//  i2cFXO.ptrRxQ = RxCompassBuffer;
//  i2cFXO.wRxQIndex = 0;
//  i2cFXO.wRxQSize = 0;
//  i2cFXO.bRxQReceiving = FALSE;
//
//  TxCompassBuffer[0] = COMP_CTRL_REG2_ADDR;
//  TxCompassBuffer[1] = COMP_SLEEP_MODE;
//    i2cWrite( &i2cFXO );
//    __delay_cycles( 10000 );
//}
//
//void AccelInit(void) {
//    BYTE i=0;
//    TIMERHANDLE thCompassTimer;
//    thCompassTimer = RegisterTimer();
//    CompassEnable();
//    ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
//    StartTimer( thCompassTimer );
//    if(thCompassTimer) {
//        while( !TimerExpired( thCompassTimer ) ) {
//            ResetWatchdog();
//        }
//    } else  {
//        ResetWatchdog();
//        __delay_cycles(400000);
//    }
//    UnregisterTimer(thCompassTimer);
//
//    // clear the buffers
//    for(i=0;i<sizeof(RxAccelBuffer);i++)  {
//        RxAccelBuffer[i]=0;
//        TxAccelBuffer[i]=0;
//    }
//
//    i2cAccel.bySlaveAddress = ACCEL_ADDRESS;
//    i2cAccel.ptrTxQ = TxAccelBuffer;
//    i2cAccel.wTxQIndex = 0;
//    i2cAccel.wTxQSize = 5;
//    i2cAccel.bTxQSending = TRUE;
//    i2cAccel.ptrRxQ = RxAccelBuffer;
//    i2cAccel.wRxQIndex = 0;
//    i2cAccel.wRxQSize = 0;
//    i2cAccel.bRxQReceiving = FALSE;
//
//    // the MSB of the SUB address has to be '1' to allow multiple data read/write
//    TxAccelBuffer[0] = CTRL_REG1_ADDR |0x80;        // address of the first config register
//    TxAccelBuffer[1] = DATA_RATE_50_HZ|Z_AXIS_EN|Y_AXIS_EN|X_AXIS_EN;       // CTRL_REG1_A register data
////  TxAccelBuffer[1] = DATA_RATE_50_HZ|LOW_POWER_EN|Z_AXIS_EN|Y_AXIS_EN|X_AXIS_EN;      // CTRL_REG1_A register data
////  TxAccelBuffer[2] = 0x00;                // CTRL_REG2_A register data
//    TxAccelBuffer[2] = FILTER_EN;               // CTRL_REG2_A register data
//    TxAccelBuffer[3] = 0x00;                // CTRL_REG3_A register data
//    TxAccelBuffer[4] = DATA_UPDATE | BIG_ENDIAN | FULL_SCALE_8G | HIGH_RES_EN;        // CTRL_REG4_A register data
////  TxAccelBuffer[4] = DATA_UPDATE|BIG_ENDIAN;          // CTRL_REG4_A register data
////  TxAccelBuffer[4] = DATA_UPDATE|BIG_ENDIAN;          // CTRL_REG4_A register data
//
//    i2cWrite(&i2cAccel);
//}
//
//void AccelReadDataStart(void) {
//    i2cAccel.bySlaveAddress = ACCEL_ADDRESS;
//    i2cAccel.ptrTxQ = TxAccelBuffer;
//    i2cAccel.wTxQIndex = 0;
//    i2cAccel.wTxQSize = 1;
//    i2cAccel.bTxQSending = TRUE;
//    i2cAccel.ptrRxQ = RxAccelBuffer;
//    i2cAccel.wRxQIndex = 0;
//    i2cAccel.wRxQSize = 6;
//    i2cAccel.bRxQReceiving = TRUE;
//
//    TxAccelBuffer[0] = OUT_X_L_REG_ADDR | 0x80;     // address of the first config register
//    i2cRead(&i2cAccel);
//}
//
//
//BOOL AccelDataReady(void) {
//    return (!i2cAccel.bRxQReceiving);
//}
//
//BOOL AccelTxDone(void) {
//    return (!i2cAccel.bTxQSending);
//}
//
//// read all configuration registers
////CRA_REG_M (00h), CRB_REG_M (01h), MR_REG_M (02h)
//void AccelRegisterDump(void) {
//    i2cAccel.bySlaveAddress = ACCEL_ADDRESS;
//    i2cAccel.ptrTxQ = TxAccelBuffer;
//    i2cAccel.wTxQIndex = 0;
//    i2cAccel.wTxQSize = 1;
//    i2cAccel.bTxQSending = TRUE;
//    i2cAccel.ptrRxQ = RxAccelBuffer;
//    i2cAccel.wRxQIndex = 0;
//    i2cAccel.wRxQSize = 29;
//    i2cAccel.bRxQReceiving = TRUE;
//
//    TxAccelBuffer[0] = CTRL_REG1_ADDR| 0x80;        // address of the first config register
//    i2cRead(&i2cAccel);
//}
//
//// copy the compass data into the given buffer
//void AccelGetRawData(BYTE *pData, WORD maxSize) {
//    unsigned int i=0;
//    while( ( i < i2cAccel.wRxQIndex ) && ( i < maxSize ) ) {
//        pData[i] = RxAccelBuffer[i];
//        i++;
//    }
//}

//// read all configuration registers
//// CRA_REG_M (00h), CRB_REG_M (01h), MR_REG_M (02h)
//void CompassRegisterDump(void) {
//
//  i2cFXO.bySlaveAddress = FXO_ADDRESS;
//  i2cFXO.ptrTxQ = TxCompassBuffer;
//      i2cFXO.wTxQIndex = 0;
//    i2cFXO.wTxQSize = 1;
//    i2cFXO.bTxQSending = TRUE;
//    i2cFXO.ptrRxQ = RxCompassBuffer;
//    i2cFXO.wRxQIndex = 0;
//    i2cFXO.wRxQSize = 3;
//    i2cFXO.bRxQReceiving = TRUE;
//
//    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;       // address of the first configuration register
//    i2cRead( &i2cFXO );
//}

//// This reads the 6 bytes from the FXOS8700 that are the last compass reading
//BOOL ReadCompass(void) {
//    if( AccelCompassDataReady() == TRUE ) {
//        CompassGetRawData( bDataArray, sizeof( bDataArray ) );
//        sMagnetData.iXField[0] = bDataArray[0];
//        sMagnetData.iXField[0] = ( sMagnetData.iXField[0] << 8 ) | bDataArray[1];
//        sMagnetData.iZField[0] = bDataArray[2];
//        sMagnetData.iZField[0] = ( sMagnetData.iZField[0] << 8 ) | bDataArray[3];
//        sMagnetData.iYField[0] = bDataArray[4];
//        sMagnetData.iYField[0] = ( sMagnetData.iYField[0] << 8 ) | bDataArray[5];
//        return TRUE;
//    }
//    return FALSE;
//}

//// copy the compass data into the given buffer
//void CompassGetRawData(BYTE *pData, WORD maxSize) {
//  unsigned int i=0;
//  while( ( i < 6) && ( i < maxSize ) ) {
//      pData[i] = RxCompassBuffer[i];
//      i++;
//  }
//}

//void CompassEnable(void) {
//
//  i2cFXO.bySlaveAddress = FXO_ADDRESS;
//  i2cFXO.ptrTxQ = TxCompassBuffer;
//  i2cFXO.wTxQIndex = 0;
//  i2cFXO.wTxQSize = 2;
//  i2cFXO.bTxQSending = TRUE;
//  i2cFXO.ptrRxQ = RxCompassBuffer;
//  i2cFXO.wRxQIndex = 0;
//  i2cFXO.wRxQSize = 0;
//  i2cFXO.bRxQReceiving = FALSE;
//
//
//  TxCompassBuffer[0] = FXO_CTRL_REG1_ADDR;
//  TxCompassBuffer[1] = COMP_M_ODR_100HZ | COMP_ACTIVE_MODE;
//    i2cWrite( &i2cFXO );
//    __delay_cycles( 10000 );
//}
