/* ads1220.c
 *  Created on: 2012-05-18
 *      Author: DD
 */

#include "ads1220.h"

/********************************************************************************
 * 	in ADS1220 REVC the polarity of bit CM in Config Register has been changed
 * 	CM: Conversion mode
 * 	This bit sets the conversion mode for the ADS1220.
 * 	0 : Single-shot mode (default)
 * 	1 : Continuous conversion mode
 *******************************************************************************/

//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012- 05-18    	DD          Initial Implementation
//	....
//
//	2014-06-17		DD			added - can configure ADC to use internal Vref (2.048V) or IDAC (configurable current source)
// 	2015-09-09		DD			added - use ADC4_CSn (#6) configured with internal Vref and sourcing 50uA.
//  2019-10-25      BDD         Did some refactoring. Removed code used for debugging. got rid of excess lines to save mouse wheel.
//*******************************************************************************


#if(USED_ADC_PN==ADS1220_REVC)
#define ADS1220_SINGLE_SAMPLE		0<<2
#define ADS1220_CONT_MODE			1<<2
#else
#define ADS1220_SINGLE_SAMPLE		1<<2
#define ADS1220_CONT_MODE			0<<2
#endif

#define ADS1220_ENABLE_TEMP			1<<2
#define ADS1220_INT_REFERENCE		0<<6
#define ADS1220_EXT_REFERENCE		1<<6
#define ADS1220_EXT_REF_AIN0_AIN3	2<<6
#define ADS1220_EXT_REF_AVDD		3<<6
#define ADS1220_EXT_FIR_50_60_HZ	1<<4
#define ADS1220_PSW_SW_EN			1<<3


#define ADS1220_COM_WREG			0X40
#define ADS1220_COM_RREG			0X20
#define ADS1220_COM_RESET			0X06
#define ADS1220_COM_SHUTDOWN		0X02
#define ADS1220_COM_RDATA			0X10
#define ADS1220_COM_SYNC			0x08

#define ADS1220_REG0				0X00
#define ADS1220_REG1				0X01
#define ADS1220_REG2				0X02
#define ADS1220_REG3				0X03

#define ADS1220_NOP					0XFF

//! AD1220 full range
#define ADS1220_FULL_RANGE 		0x7FFFFF

#define ADS1220_TEMP_SENSOR			0X02

#define REG_TO_READ			3

// Writes a bytes to a register
static BOOL ADS1220WriteRegister(BYTE bSlaveSelect, BYTE bAddress, BYTE *pData,	BYTE bDataSize);
// Reads a bytes from a register
static BOOL ADS1220ReadRegister(BYTE bSlaveSelect, BYTE bAddress, BYTE bDataSize, BYTE *pData);
// read the ADS1220 conversion result
static BOOL ADS1220GetResult(BYTE bSlaveSelect, BYTE *pData);


//static ADS1220_GAIN adcGain = ADS1220_GAIN_1;
static ADS1220_GAIN adcGain = ADS1220_GAIN_64;
static ADS1220_RATE	adcRate = AD1220_UPDATE_RATE_600_HZ;
static ADS1220_MODE adcMode = AD1220_TURBO_MODE;
static ADS1220_IDAC1_MUX	idac1Mux = AD1220_IDAC1_AIN0;		// run the IDAC current through the AIN0

static signed long tempRawData;
static SBYTE adcTemperature;

typedef struct ADC_SELECT_ADS1220WriteRegister
{
	BYTE pinSel;
	BYTE pinDrdy;
	BYTE pinRefEn;
}ADC_SELECT_;


//static const ADC_SELECT_ adcSelect[ADS1220_TOTAL] =
//{
//		{ADC1_CSn,ADC1_DRDY,S1_EN_n},
//		{ADC2_CSn,ADC2_DRDY,S2_EN_n},
//		{ADC3_CSn,ADC3_DRDY,S3_EN_n},
//		{ADC4_CSn,ADC4_DRDY,S4_EN_n},
//		{ADC5_CSn,ADC5_DRDY,S5_EN_n},
//		{ADC6_CSn,ADC6_DRDY,S6_EN_n},
//		{ADC7_CSn,ADC7_DRDY,S7_EN_n},
//		{ADC0_CSn,ADC0_DRDY,P9_TP39},
//};

//// use channels 1, 2, 3, 5, 6, 7 for strain gauges
//static const ADC_SELECT_ adcSelect[ADS1220_TOTAL] =
//{
//		{ADC1_CSn,ADC1_DRDY,S1_EN_n},
//		{ADC2_CSn,ADC2_DRDY,S2_EN_n},
//		{ADC3_CSn,ADC3_DRDY,S3_EN_n},
//		{ADC5_CSn,ADC5_DRDY,S5_EN_n},
//		{ADC6_CSn,ADC6_DRDY,S6_EN_n},
//		{ADC7_CSn,ADC7_DRDY,S7_EN_n},
//		{ADC4_CSn,ADC4_DRDY,S4_EN_n},
//		{ADC0_CSn,ADC0_DRDY,P9_TP39},
//};

// changed in 2v05
// use channels 1, 2, 3, 5, 6, 7 for strain gauges. channel 4 is RTD. Channel 8 is precision gyro

// creates an array of structures with the information for each ADC
static const ADC_SELECT_ adcSelect[ADS1220_TOTAL] =
{
		{ADC2_CSn,ADC2_DRDY,S2_EN_n},               // TORQUE 0     ADC2
		{ADC6_CSn,ADC6_DRDY,S6_EN_n},               // TORQUE 180   ADC6
		{ADC1_CSn,ADC1_DRDY,S1_EN_n},               // TENSION 0    ADC1
		{ADC3_CSn,ADC3_DRDY,S3_EN_n},               // TENSION 90   ADC3
		{ADC5_CSn,ADC5_DRDY,S5_EN_n},               // TENSION 180  ADC5
		{ADC7_CSn,ADC7_DRDY,S7_EN_n},               // TENSION 270  ADC7
		{ADC4_CSn,ADC4_DRDY,S4_EN_n},               // RTD          ADC4
		{ADC0_CSn,ADC0_DRDY,P9_TP39},               // GYRO         ADC8
};

//------------------------------------------------------------------------------
//  Reads a bytes from a specified ADC register
//  Send    bAddress		Address of the register to read
//          bDataSize		Number of bytes to read - can be 1
//          pData			pointer to the result buffer
//  Returns a pass / fail
BOOL ADS1220ReadRegister(BYTE bSlaveSelect, BYTE bAddress, BYTE bDataSize, BYTE *pData) {
	BYTE bPacket[6];

	unsigned int cnt;

	if ( bAddress > ADS1220_LAST_REGISTER )
		return FALSE;

	if ( bDataSize > ADS1220_TOTAL_REGISTERS )
		return FALSE;

	bPacket[0] = ADS1220_COM_RREG | (bAddress << 2) | bDataSize;
	bPacket[1] = ADS1220_NOP;
	bPacket[2] = ADS1220_NOP;
	bPacket[3] = ADS1220_NOP;
	bPacket[4] = ADS1220_NOP;

	SpiA0TxRxPacket( bPacket, ( bDataSize + 2 ) );

	for ( cnt = 1; cnt <= bDataSize+1; cnt++ ) {
		pData[cnt - 1] = bPacket[cnt];
	}
	return TRUE;
}

//------------------------------------------------------------------------------
//  Writes bytes to the specified ADC register
//  param[in]   bAddress		Address of the register to change
//              *pData			pointer to values to write
BOOL ADS1220WriteRegister( BYTE bSlaveSelect, BYTE bAddress, BYTE *pData, BYTE bDataSize ) {
	BYTE bPacket[5];
	unsigned int i;
	if ( bAddress > ADS1220_LAST_REGISTER )
		return FALSE;

	if ( bDataSize > ADS1220_TOTAL_REGISTERS )
		return FALSE;

	bPacket[0] = ADS1220_COM_WREG | ( bAddress << 2 ) | bDataSize;

	for ( i = 1; i <= bDataSize; i++ )
		bPacket[i] = pData[i - 1];

	SpiA0TxRxPacket( bPacket, ( bDataSize + 1 ) );

	return TRUE;
}

//------------------------------------------------------------------------------
//  Read the conversion result - 4 Bytes
//  param[in]   bAddress		Address of the register to change
//              *pData			pointer to values to write
BOOL ADS1220GetResult(BYTE bSlaveSelect, BYTE *pData) {
	BYTE bPacket[5];
	unsigned int i;
	
	bPacket[0] = ADS1220_COM_RDATA;
	for (i = 1; i <= 4; i++)
		bPacket[i]=0xFF;
		
	SpiA0TxRxPacket(bPacket, 4);

	for (i = 1; i <= 4; i++)
		pData[i-1]=bPacket[i];

	return TRUE;
}

//------------------------------------------------------------------------------
//  Takes a single sample and converts it to micro volts.
//  param[in]	analogChannel		Specifies the analog channel to configure.
//
//  param[out]	p_vResult			Pointer to the destination to store the result in uV
//
//	return      - TRUE if successful. The result is now stored in p_vResult.
//              - FALSE if unsuccessful.
BOOL ADS1220ReadVoltage( BYTE adcNumber, ADS1220_SOURCE analogChannel, UVOLTS *p_vResult ) {

	BYTE bSettings[3];
	BYTE bResult[5];
	long int iSample;
	long int iTemp;

	const long int iAdcFactorFor2500mV = (2.5 / ADS1220_FULL_RANGE) * 100; // check!

	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;

	if (analogChannel > ADS1220_MAX_CHANNELS)
		return FALSE;

	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	bSettings[0] = analogChannel|adcGain;
	bSettings[1] = adcRate|adcMode|ADS1220_SINGLE_SAMPLE;
	bSettings[2] = ADS1220_EXT_REF_AIN0_AIN3|ADS1220_EXT_FIR_50_60_HZ;
	ADS1220WriteRegister( adcSelect[adcNumber].pinSel ,ADS1220_REG0,bSettings, sizeof(bSettings) );

	ADS1220ReadAllRegs(adcNumber, bResult, sizeof(bResult));

	ADS1220StartConversion(adcNumber);

	while( ReadInputPin(adcSelect[adcNumber].pinDrdy ) == TRUE );

	ADS1220GetResult(adcSelect[adcNumber].pinSel, bResult);

	iSample = bResult[0];
	iSample <<=16;
	iSample |= (bResult[1]<<8)|(bResult[2]);

//	SetOutputPin(adcSelect[adcNumber].pinRefEn, TRUE );

	iTemp = iSample * iAdcFactorFor2500mV;

	*p_vResult = (iTemp /100);

	return TRUE;
}

BOOL ADS1220ReadAllRegs(BYTE adcNumber, BYTE *pData, WORD maxSize) {
	if ( adcNumber >= ADS1220_TOTAL )
			return FALSE;

	if( REG_TO_READ > maxSize )
			return FALSE;

	SetOutputPin( adcSelect[adcNumber].pinSel, FALSE );

	ADS1220ReadRegister( adcSelect[adcNumber].pinSel, 0x00, REG_TO_READ, pData );

	SetOutputPin( adcSelect[adcNumber].pinSel, TRUE );

	return TRUE;
}

//------------------------------------------------------------------------------
//  Send a command to ADC to start conversion
//
//  Param[in]	adcNumber (defined in adcSelect[])
BOOL ADS1220StartConversion(BYTE adcNumber) {
	BYTE bPacket[5];

	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;

	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	bPacket[0] = ADS1220_COM_SYNC;

	SpiA0TxRxPacket(bPacket, 1);
	
	SetOutputPin(adcSelect[adcNumber].pinSel , TRUE );
	
	return TRUE;
}

//------------------------------------------------------------------------------
//  Send a reset command to ADC (if it wasn't obvious)
BOOL ADS1220Reset(BYTE adcNumber) {
	BYTE bPacket[5];

	if ( adcNumber >= ADS1220_TOTAL )
		return FALSE;

	SetOutputPin( adcSelect[adcNumber].pinSel , FALSE );

	bPacket[0] = ADS1220_COM_RESET;

	SpiA0TxRxPacket( bPacket, 1 );

	SetOutputPin( adcSelect[adcNumber].pinSel , TRUE );

	return TRUE;
}

// Set the corresponding ADC in power down.
BOOL ADS1220Powerdown(BYTE adcNumber) {
	BYTE bPacket[5];

	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;

	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	bPacket[0] = ADS1220_COM_SHUTDOWN;

	SpiA0TxRxPacket(bPacket, 1);

	SetOutputPin(adcSelect[adcNumber].pinSel , TRUE );

	return TRUE;
}

//------------------------------------------------------------------------------
// Configure the  corresponding ADC in continuous conversion mode.
//  Analog Epson Gyro ADC is configured to use internal 2.048V reference, all Strain Gauges ADCs use the IDAC
//
// param[in]    adcNumber		- number of the ADC
//              analogChannel	- analog channel to measure
//              idacCurrent		- constant current value
BOOL ADS1220Configure( ADS1220_SOURCE analogChannel, BYTE adcNumber, ADS1220_IDAC_CURRENT idacCurrent ) {
	BYTE bSettings[5];
	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;
		
	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	bSettings[0] = ADS1220_COM_WREG | (ADS1220_REG0 << 2) | 3;
	/*
	 * 	Care must be taken not to exceed the compliance voltage of the IDACs.
	 *	In other words, limit the voltage on the pin where the IDAC is routed to < (AVDD � 0.9 V), otherwise the specified
	 *	accuracy of the IDAC current is not met
	 */

	// GYRO and Switch ADC are configured to use internal Vref and gain 1.
	// All strain gauges ADC use current source and gain 64
	switch(adcNumber) {
	case ADC_EPSON_GYRO:
		bSettings[1] = analogChannel|ADS1220_GAIN_1;
		bSettings[2] = adcRate|adcMode|ADS1220_CONT_MODE;
		bSettings[3] = ADS1220_INT_REFERENCE|ADS1220_EXT_FIR_50_60_HZ|ADS1220_PSW_SW_EN;	// internal reference 2.048V
		//bSettings[3] = ADS1220_EXT_REF_AIN0_AIN3|ADS1220_EXT_FIR_50_60_HZ|ADS1220_PSW_SW_EN;	// external reference
		bSettings[4] = 0x00;
		break;
	case ADC_SWITCH_IN:
		bSettings[1] = analogChannel|ADS1220_GAIN_1;
		bSettings[2] = adcRate|adcMode|ADS1220_CONT_MODE;
		bSettings[3] = ADS1220_INT_REFERENCE|ADS1220_EXT_FIR_50_60_HZ|ADS1220_PSW_SW_EN|AD1220_IDAC_CURRENT_50uA;	// internal reference 2.048V and 50uA current source
		bSettings[4] = idac1Mux;
		break;

	default:
		bSettings[1] = analogChannel | adcGain;
		bSettings[2] = adcRate|adcMode | ADS1220_CONT_MODE;
		bSettings[3] = ADS1220_EXT_REF_AIN0_AIN3 | ADS1220_EXT_FIR_50_60_HZ | ADS1220_PSW_SW_EN | idacCurrent;	// external reference
		if(idacCurrent) {
			bSettings[4] = idac1Mux;
		} else {
			bSettings[4] = 0x00;
		}
		break;
	}

	SpiA0TxRxPacket( bSettings, 5);
	SetOutputPin(adcSelect[adcNumber].pinSel , TRUE );
	return TRUE;
}

//------------------------------------------------------------------------------
//	Configure the  corresponding ADC as temperature sensor
//	param[in]	adcNumber		- number of the ADC
BOOL ADS1220ConfigAsTemperatureSensor(BYTE adcNumber) {
	BYTE bSettings[5];
	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;
		
	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	bSettings[0] = ADS1220_COM_WREG | (ADS1220_REG0 << 2) | 3;
	bSettings[1] = 0;
//	bSettings[2] = adcRate|adcMode|ADS1220_TEMP_SENSOR;
	bSettings[2] = ADS1220_TEMP_SENSOR;		// Normal mode 20sps
	bSettings[3] = ADS1220_EXT_REF_AIN0_AIN3|ADS1220_EXT_FIR_50_60_HZ|ADS1220_PSW_SW_EN;
//	bSettings[3] = ADS1220_EXT_FIR_50_60_HZ|ADS1220_PSW_SW_EN;
//	bSettings[3] = ADS1220_EXT_REF_AIN0_AIN3|ADS1220_EXT_FIR_50_60_HZ;
	bSettings[4] = 0x00;
//	ADS1220WriteRegister( adcSelect[adcNumber].pinSel ,ADS1220_REG0,bSettings, sizeof(bSettings) );
	SpiA0TxRxPacket( bSettings, 5);
	SetOutputPin(adcSelect[adcNumber].pinSel , TRUE );
	return TRUE;
}

//------------------------------------------------------------------------------
//  Configure the  corresponding ADC in continuous conversion mode.
//      Use internal 2.048V reference,and IDAC.
//      Used to check the value of the connected strain gauge.
//  param[in]   adcNumber		- number of the ADC
//              analogChannel	- analog channel to measure
//              idacCurrent		- constant current value
BOOL ADS1220ConfigureInSGTestMode( ADS1220_SOURCE analogChannel, BYTE adcNumber, ADS1220_IDAC_CURRENT idacCurrent ) {
	BYTE bSettings[5];
	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;

	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	bSettings[0] = ADS1220_COM_WREG | (ADS1220_REG0 << 2) | 3;
	bSettings[1] = analogChannel|ADS1220_GAIN_1;
	bSettings[2] = adcRate|adcMode|ADS1220_CONT_MODE;
	bSettings[3] = ADS1220_INT_REFERENCE|ADS1220_EXT_FIR_50_60_HZ|ADS1220_PSW_SW_EN|idacCurrent;	// internal reference 2.048V

	if(idacCurrent) {
		bSettings[4] = idac1Mux;
	} else {
		bSettings[4] = 0x00;
	}

	SpiA0TxRxPacket( bSettings, 5);
	SetOutputPin(adcSelect[adcNumber].pinSel , TRUE );
	return TRUE;
}

// check if the ADC has data ready
BOOL ADS1220HasResult(BYTE adcNumber) {

	if ( adcNumber >= ADS1220_TOTAL )
		return FALSE;

	if( ( ReadInputPin(adcSelect[adcNumber].pinDrdy ) ) == FALSE )
		return TRUE;
	else
		return FALSE;
}

//	Reads the ADC raw data and converts them to signed long value. If successful the result is stored in p_vResult.
BOOL ADS1220ReadVoltageCont(BYTE adcNumber, UVOLTS *p_vResult) {

	BYTE bResult[5];
	volatile signed long iSample;
	volatile DWORD dwResult;

	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;

	SetOutputPin( adcSelect[adcNumber].pinSel, FALSE );

	ADS1220GetResult( adcSelect[adcNumber].pinSel, bResult );

	// Move array bResult into double word variable dwResult
	dwResult = bResult[0];
	dwResult =( dwResult << 16 );
	dwResult |= ( (DWORD)bResult[1] << 8 ) | (bResult[2] );


	if ( dwResult > ADS1220_FULL_RANGE ) {                  // if the result is negative (three bytes)
		iSample = (signed long)( dwResult | 0xFF000000 );   // add FF to upper byte (make negative in 4 bytes)
	} else {
		iSample = (signed long)dwResult;                    // if positive number
	}

	*p_vResult = iSample;                                   // move result into pointer
	
	SetOutputPin( adcSelect[adcNumber].pinSel, TRUE );

	return TRUE;
}

// Takes a sample and converts it to microvolts. If successful the result is stored in p_vResult.
BOOL ADS1220ReadTempCont(BYTE adcNumber, UVOLTS *p_vResult) {

	BYTE bResult[5];
	volatile signed long iSample;
//	long int iTemp;
	volatile DWORD dwResult;

	const long int iAdcTempCoefficient = 30517; 

	if (adcNumber >= ADS1220_TOTAL)
		return FALSE;

	SetOutputPin(adcSelect[adcNumber].pinSel , FALSE );

	ADS1220GetResult(adcSelect[adcNumber].pinSel, bResult);

	dwResult = bResult[0];
	dwResult =(dwResult<<16);
	dwResult |= ((DWORD)bResult[1]<<8)|(bResult[2]);
	
	if (dwResult>ADS1220_FULL_RANGE) {
		iSample = (signed long)(dwResult|0xFF000000);
	} else {
		iSample = (signed long)dwResult;
	}
	
	iSample /= 1000;
	iSample *= iAdcTempCoefficient;	
	iSample /= 1000;
		
	*p_vResult = iSample;
	
	SetOutputPin(adcSelect[adcNumber].pinSel , TRUE );

//	iTemp = iSample * iAdcFactorFor2500mV;
//
//	*p_vResult = (iTemp /100);

	return TRUE;
}

// Turn On the ADC external reference The Gyro ADC Reference is always On - doesn't have switch
BOOL ADS1220TurnOnReference(BYTE adcNumber) {

	if (adcNumber >= ADS1220_TOTAL)
			return FALSE;

	// the Gyro ADC Reference doesn't have a switch
	if (adcNumber != ADC_EPSON_GYRO)
		SetOutputPin(adcSelect[adcNumber].pinRefEn, FALSE );

	return TRUE;
}

//!	\brief		Turn Off the ADC external reference
//!				The Gyro ADC Reference is always On - doesn't have switch
BOOL ADS1220TurnOffReference(BYTE adcNumber) {

	if ( adcNumber >= ADS1220_TOTAL )
			return FALSE;

	// the Gyro ADC Reference doesn't have a switch
	if ( adcNumber != ADC_EPSON_GYRO )
		SetOutputPin( adcSelect[adcNumber].pinRefEn, TRUE );

	return TRUE;
}

void ReadTemperature(void) {
	if( ADS1220HasResult( ADC_USED_AS_TEMP ) ) {
		ADS1220ReadTempCont( ADC_USED_AS_TEMP, &tempRawData );
	}
}

// Description:      Connect all Vrefs to StrainGauge Connectors
void TurnOnAllAdcRef(void) {
    char adcNumber;

    for(adcNumber =FIRST_SG_ADC; adcNumber<=LAST_SG_ADC; adcNumber++) {
        ADS1220TurnOnReference(adcNumber);
    }
}

// convert ADC counts in temperature 0.5�C per count starting at 0 (-64�C to +63.5�C)
SBYTE GetTemperature(void) {
	int iTemp;
	signed long lTemp;
	// to do convert the raw data in temperature
	// 0.5�C per count starting at 0 (-64�C to +63.5�C)
	// tempRawData is in mDeg -> 1 �C = 1000
	lTemp = tempRawData/100;
	iTemp= (int)lTemp;	// convert the mdeg in 0.1�C -> 10 = 1�C

	if(iTemp<-640) {
		adcTemperature = -128;		// -64�C
	} else {
		if(iTemp>635) {
			adcTemperature = 127;	// +63.5�C
		} else {
			iTemp *=2;				// convert in 0.5�C per count
			iTemp/=10;
			adcTemperature = (SBYTE)iTemp;
		}
	}
	return adcTemperature;
}

// Send a power down command to all ADCs. Turn off the ADC CLK
void PowerDownADC(void) {
    BYTE adcNumber;
    for(adcNumber = FIRST_SG_ADC; adcNumber<ADS1220_TOTAL; adcNumber++) {
        ADS1220Powerdown(adcNumber);
    }
    P11SEL &= ~BIT0;
}

// Description:      Disconnect all Vrefs from StrainGauge Connectors
void TurnOffAllAdcRef(void) {
    BYTE adcNumber;
    for(adcNumber =FIRST_SG_ADC; adcNumber<=LAST_SG_ADC; adcNumber++) {
        ADS1220TurnOffReference(adcNumber);
    }
}
