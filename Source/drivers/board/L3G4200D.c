
#include <Source/drivers/board/L3G4200D.h>

#define ST_GYRO_250DPS_SC	875
#define ST_GYRO_UPDATE_FREQ	106	//100
#define ST_GYRO_ZERO_RATE_P	300
#define ST_GYRO_ZERO_RATE_N	-300
#define SELF_TEST_VALUE		13000000
#define SELF_TEST_VALUE_2K	53000000
#define HALF_RPM_AT_2K       43                     // Gyro will give a raw reading of 86 at 1 RPM
// in 2000 DPS mode, the device will output 86 counts at 1 RPM

unsigned char SToutBuff[MAX_ST_OUT_SIZE];
unsigned char SToutPtr;
unsigned char STtxSize;			

unsigned char STinBuff[MAX_ST_IN_SIZE];
unsigned char STinPtr;

static int	zeroStValue;
static int	iTempValue;
static char	stGyroID;
//static WORD wCounter;
float currentSTz;							// hold new value of every ST z update
float accSTz;								// hold incremented value of z when moving. Is reported when movement stops
float avgSTz;
float STzBuff[ST_AVG_BUFF_SIZE];			// holds the last x "currentSTz" values
float zeroAvgz;								// average value of the device when not moving.
signed long coeff130dps;
signed long coeffLSB;
static long rpmStGyro;

static TIMERHANDLE thGyroTimer = 0;         // generic 1mS timer used throughout this file.


// this will set the ST gyro in normal mode and taking samples continuously.
void initST(void) {
	//unsigned char cnt;
	SetPinAsOutput( L3G_CS_1 );
	SetPinAsInput( L3G_1_DRDY );
	
	SetOutputPin( L3G_CS_1, TRUE );			                // disable chip select

	rpmStGyro = 0;
	
	SpiB0Initialize();
	if( thGyroTimer == TIMER_NOT_REGISTERED ) {
		thGyroTimer = RegisterTimer();
	    StartTimer( thGyroTimer );
	}
	ResetTimer( thGyroTimer, ST_WAKEUP_PERIOD );

	while( !TimerExpired( thGyroTimer ) ) {
		ResetWatchdog();
	}
	setSTreg( SET_REG1, Z_EN | PD );
	setSTreg( SET_REG3, I1_INT1 | I2_WTM | I2_DRDY );

	setSTNormalMode();
}

void startSPI4ST(void) {
	SToutPtr = 0;
	STinPtr = 0;
	SpiB0TxRxPacket( (BYTE*)SToutBuff, (WORD)STtxSize );
}
	
void PowerDownStGyro(void) {
	setSTreg( SET_REG1, POWER_DOWN_GYRO );
}

void WakeUpStGyro(void) {
    setSTreg( SET_REG1, Z_EN | PD );
    setSTreg( SET_REG3, I1_INT1 | I2_WTM | I2_DRDY );
}

void setSTNormalMode(void) {
  #ifdef ST_GYRO_250DPS
  setSTreg( SET_REG4, 0 );                                  // set 250 dps
  #endif
  #ifdef ST_GYRO_2000DPS
  setSTreg( SET_REG4, SET_2K_DPS );                         // set 2000dps (no BDU, BLE, ST0-1, SIM)
  #endif
}

// Get the current Z access turns reading.
void getSTz(void) {
    SetOutputPin( L3G_CS_1, FALSE );                        // enable chip select
	STtxSize = 0;
	SToutBuff[STtxSize++] = OUT_Z_L + ST_READ + ST_MULTI_ADS;
	SToutBuff[STtxSize++] = FILLER;
	SToutBuff[STtxSize++] = FILLER;
	startSPI4ST();	
	SetOutputPin( L3G_CS_1, TRUE );                         // disable chip select
}

//------------------------------------------------------------------------------
// This called from main() and assumes the device is not turning. reads the gyro
// several times and averages the results to get the zero offset.
void calibrateStGyro(void) {
	int i = 0;
	long i32ZeroLevel = 0;

	zeroStValue = 0;
	iTempValue = 0;
	setSTNormalMode();

	ResetTimer( thGyroTimer, 10 );

	while( !TimerExpired( thGyroTimer ) ) {
		ResetWatchdog();
	}

	while ( i < 64 ) {
		while( ReadInputPin( L3G_1_DRDY ) == FALSE ) {
			ResetWatchdog();
		}
		ResetTimer( thGyroTimer, 2);
		while( !TimerExpired( thGyroTimer ) );

		getSTz();
		iTempValue = SToutBuff[2] << 8;
		iTempValue += SToutBuff[1];

		if ( iTempValue < 50 && iTempValue > -50 ) {      // throw out bogus readings. Usually first one or two
            i++;
            i32ZeroLevel += iTempValue;
		}
		ResetWatchdog();
	}
	zeroStValue = (int)( i32ZeroLevel >> 6 );               // because we did 64 readings, divide by 64
}

//------------------------------------------------------------------------------
// This is a self test. If the gyro is working properly, it will return it's
// internal ID number.
void readSTid(void) {
	STtxSize = 0;
	SToutBuff[STtxSize++] = ID_REG + ST_READ;
	SToutBuff[STtxSize++] = FILLER;
	SToutBuff[STtxSize++] = FILLER;
	SetOutputPin(L3G_CS_1, FALSE);			                // enable chip select
	startSPI4ST();
	SetOutputPin(L3G_CS_1, TRUE);			                // disable chip select
	stGyroID = 	SToutBuff[1];
}

//------------------------------------------------------------------------------
// Return the stored ID read from the gyro in readSTid();
char gedStId(void) {
	return stGyroID;
}

//------------------------------------------------------------------------------
// gNumber is 0,1,2
BOOL stGyroHasData(void) {
	return ( ReadInputPin( L3G_1_DRDY ) );                  // if pin is high, gyro has data to send
}

//------------------------------------------------------------------------------
// Reads the value from the ST chip. Usually called after stGyroHasData returns
// true. This will convert the reading into RPM with a simple average since the
// factor used is "half" RPM. If the reading is odd, we add 1 then divide by 2.
// The result returns 0 if not turning and 1 if turning.
BOOL stGyroReadData( int *pResultRPM) {
    signed long i32Temp;
	iTempValue = 0;

	getSTz();

	iTempValue = SToutBuff[2] << 8;
	iTempValue += SToutBuff[1];
				
	i32Temp = (signed long)iTempValue;
	i32Temp -= zeroStValue;

	if ( i32Temp < 0 )                                      // make all values positive
	    i32Temp *= -1;

	i32Temp /= HALF_RPM_AT_2K;                              // convert value to RPM times 2 in 2000 DPS scale

	if (i32Temp & 0x01 )                                    // if result is odd, add one to round up
	    i32Temp++;

	i32Temp >>= 1;                                          // divide by two after rounding

	*pResultRPM = i32Temp;                                  // store value in address sent

	if ( i32Temp )                                          // if value greater than 0
		return TRUE;                                            // device is turning

	return FALSE;                                           // device is not turning
}

//------------------------------------------------------------------------------
// Was previously used to set the "span" value used in calculating RPM but this tested
// out to be inaccurate. Now we use a hard coded value from the data sheet for the
// scale we are in. For now the scale is 2000 DPS.
// As of now, this function does not really do anything...
void SelfTestStGyro(void) {
	int i;
	
	i=0;
	coeff130dps = 0;
	iTempValue = 0;

    #ifdef ST_GYRO_250DPS
      setSTreg( SET_REG4, SET_SELF_TEST );                  // set PD and Zen to 1
    #endif
    #ifdef ST_GYRO_2000DPS
      setSTreg( SET_REG4, SET_SELF_TEST_2K );               // set to 2K DPS self test in positive direction
    #endif

	ResetTimer( thGyroTimer, 100 );                         // Give device time to stabilize
	while( !TimerExpired( thGyroTimer ) ) {
			ResetWatchdog();
	}

	while ( i < 5 ) {                                       // Dummy reads to warm it up.
		while( ReadInputPin( L3G_1_DRDY ) == FALSE ) {      // Wait for data ready pin to become enabled
			ResetWatchdog();
		}
		ResetTimer( thGyroTimer, 2);                        // give some time to insure reading stable
		while( !TimerExpired( thGyroTimer ) );

		getSTz();
		i++;
		ResetWatchdog();
	}

	i = 0;
	while ( i < 20 ) {
		while( ReadInputPin(L3G_1_DRDY) == FALSE ) {        // Wait for data ready pin to become enabled
			ResetWatchdog();
		}

		ResetTimer( thGyroTimer, 2);
		while( !TimerExpired( thGyroTimer ) );

		getSTz();

		iTempValue = SToutBuff[2] << 8;                     // Move buffer into a value
		iTempValue += SToutBuff[1];
		i++;

		coeff130dps = ( coeff130dps * 7 + iTempValue ) >> 3;// Exponential type averaging
		ResetWatchdog();
	}
	
	#ifdef ST_GYRO_250DPS
	coeff130dps = SELF_TEST_VALUE / coeff130dps;
	#endif
	#ifdef ST_GYRO_2000DPS
	coeff130dps = SELF_TEST_VALUE_2K / coeff130dps;
	#endif
	if( coeff130dps < 0 )
		coeffLSB = (int)(coeff130dps * (-1) );
	else
		coeffLSB = (int)coeff130dps;
}

//------------------------------------------------------------------------------
// Will load static variable rpmStGyro with the latest reading to be called in GetRPM();
// Is called from IdleTask();
void GetSTGyroData(void) {

	int iStGyroRPMtmp = 0;
	if ( stGyroReadData( &iStGyroRPMtmp ) ) {               // If moving
		rpmStGyro = iStGyroRPMtmp;                          // send RPM
	} else {                                                // if not moving
		rpmStGyro = 0;
	}
}

//------------------------------------------------------------------------------
// Called from SendRFC to get the reading stored by GetSTGyroData();
BYTE GetRPM(void) {
	return (BYTE) rpmStGyro;
}


//------------------------------------- Archived Code --------------------------

//------------------------------------------------------------------------------
//  setup interrupt on Digital Gyro (P1.6).
// Used to wake the uP from deep sleep when TesTORK turns
// *** Uses too much current ****
void setDigGyroWakeInterrupt(void) {
    __bic_SR_register(GIE);                                 // Disable global interrupts
    P1IFG = 0x00;                                           // cleared all port 1 interrupts
    P1IE |= 0x40;                                           // P1.6 interrupt enabled
    P1IES &= ~0x40;                                         // set P1.6 rising edge
    __bis_SR_register(GIE);                                 // Enable global interrupts
}

//------------------------------------------------------------------------------
//  Description: disable interrupt on Dig Gyro (P1.6) toggle.
void disableDigGyroWakeInterrupt(void) {
    __bic_SR_register(GIE);                                 // Disable global interrupts
    P1IE &= ~0x40;                                          // P1.6 interrupt disabled
    P1IFG &= ~0x40;                                         // P1.6 IFG cleared
    __bis_SR_register(GIE);                                 // Enable global interrupts
}

//------------------------------------------------------------------------------
// Port 1 interrupt service routine
// Was used for "Wake on turns" when we had the gyro send a signal
// when the turns were above a set amount. The gyro drew too much
// current and we opted to just wake every 10 seconds to measure turns
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void) {
    unsigned int timer = 200;                              // Some arbitrary timing value
    if(P1IFG & 0x40)    {                                   // interrupt from P1.6 L3G INT1
        while(timer) {                                      // Insure turns continue for a minimum time before waking
            if( !( P1IN & 0x40 ) ) break;                   // if interrupt line goes low during debounce, go back to sleep
            timer--;
        }
        if( !timer ) {                                      // if while counted down to 0
            LPM4_EXIT;
        }
    }
    P1IFG = 0x00;                                           // Clear all interrupt flags
}

// setup interrupt on P1.7 - L3G_DRDY/INT2
void SetDigGyroDataInterrupt(void) {
    __bic_SR_register(GIE);
    P1IE |= 0x80;                                           // P1.7 interrupt enabled
    P1IES &= ~0x80;                                         // P1.7 rising edge
    P1IFG &= ~0x80;                                         // P1.7 IFG cleared
    __bis_SR_register(GIE);
}

// Set set the gyro to throw a low-high on Int1 when turns exceed a specified value
void setDigGyroWakeOnTurns() {
    setSTreg( INT1_CFG, ZHIE );                             // interrupt on Z turns over a threshold (either direction)
    setSTreg( INT1_THS_ZH, 0x08 );                          // set upper threshold for triggering an interrupt
}

// Set a register in the ST gyro - Added by BDD July 2019
void setSTreg(char reg, char value) {
    SetOutputPin(L3G_CS_1, FALSE);                  // enable chip select
    STtxSize = 0;
    SToutBuff[STtxSize++] = reg;
    SToutBuff[STtxSize++] = value;
    startSPI4ST();
    SetOutputPin(L3G_CS_1, TRUE);                   // disable chip select
}

//**************************************** Archived Code
//void setSTreg1(void)
//{
//  STtxSize = 0;
//  SToutBuff[STtxSize++] = SET_REG1;
//  SToutBuff[STtxSize++] = 0x0C;                           // set PD and Zen to 1
//  SetOutputPin(L3G_CS_1, FALSE);                  // enable chip select
//  startSPI4ST();
//  SetOutputPin(L3G_CS_1, TRUE);                   // enable chip select
//}

//void setSTreg3(void)
//{
//  STtxSize = 0;
//  SToutBuff[STtxSize++] = SET_REG3;
//  SToutBuff[STtxSize++] = 0x0C;                           // Enable Data ready and watermark
////    SToutBuff[STtxSize++] = 0x08;                       // Enable Data ready
////    SToutBuff[STtxSize++] = 0x04;                       // Enable watermark
//  SetOutputPin(L3G_CS_1, FALSE);                  // enable chip select
//  startSPI4ST();
//  SetOutputPin(L3G_CS_1, TRUE);                   // enable chip select
//}

// result in mdps
// return TRUE if it's moving
// read 2 times and add - to be done!!!
//BOOL stGyroReadData( int *pResultRPM,int *pResultDPS, BYTE gNumber) {
//  BOOL bMoving = FALSE;
//  iTempValue = 0;
//  SetOutputPin(stGyroSel[gNumber].pinSel, FALSE);         // enable chip select
//  getSTz();
//  SetOutputPin(stGyroSel[gNumber].pinSel, TRUE);          // disable chip select
//  iTempValue = SToutBuff[2] << 8;
//  iTempValue += SToutBuff[1];
////    if (iTempValue > iStZeroRateP)
////        bMoving = TRUE;
////    if (iTempValue < iStZeroRateN)
////        bMoving = TRUE;
////
////    if(bMoving == FALSE)
////        return bMoving;
//
////    iTempValue -= zeroStValue;
//
////    if(iTempValue <0)
////        iTempValue = iTempValue *(-1);
//
//  i32Temp = (signed long)iTempValue;
//  i32Temp *=coeffLSB;
//  i32Temp /= 100;
//  i32Temp -=zeroStValue;
//  if (i32Temp> 1100)
//      bMoving = TRUE;
//  if (i32Temp < -1100)
//      bMoving = TRUE;
//
//  *pResultRPM = (int)(i32Temp/6);
////    if(*pResultRPM <0)
////        *pResultRPM =(*pResultRPM) *(-1);
//
//  i32Temp /= ST_GYRO_UPDATE_FREQ;
//
//  *pResultDPS = (int)i32Temp;
//
//  return bMoving;
//}

//void setSTreg3NormalMode(void) {
//  STtxSize = 0;
//  SToutBuff[STtxSize++] = SET_REG3;
//  SToutBuff[STtxSize++] = 0x04;       // Enable watermark
//  SetOutputPin(L3G_CS_1, FALSE);          // enable chip select
//  startSPI4ST();
//  SetOutputPin(L3G_CS_1, TRUE);           // enable chip select
//}

//void setSTreg5(void) {
//  STtxSize = 0;
//  SToutBuff[STtxSize++] = SET_REG5;
//  SToutBuff[STtxSize++] = FIFO_ENABLE;        // enable FIFO
//  SetOutputPin(L3G_CS_1, FALSE);          // enable chip select
//  startSPI4ST();
//  SetOutputPin(L3G_CS_1, TRUE);           // enable chip select
//}

//void setSTfifo(void) {
//  STtxSize = 0;
//  SToutBuff[STtxSize++] = SET_FIFO;
//  SToutBuff[STtxSize++] = 0x21;       // FIFO mode 1 and watermark level1
////    SToutBuff[STtxSize++] = 0x22;       // FIFO mode 1 and watermark level 2
//  SetOutputPin(L3G_CS_1, FALSE);          // enable chip select
//  startSPI4ST();
//  SetOutputPin(L3G_CS_1, TRUE);           // enable chip select
//}

//void readSTtemp(void) {
//  STtxSize = 0;
//  SToutBuff[STtxSize++] = ST_READ + GET_TEMP_CMD;
//  SToutBuff[STtxSize++] = FILLER;
//  startSPI4ST();
//}
