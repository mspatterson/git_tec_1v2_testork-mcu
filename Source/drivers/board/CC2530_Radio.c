/*
 * zb_module.c
 *
 *  Created on: 2012-05-28
 *      Author: Owner
 */


#include "CC2530_Radio.h"
#include <string.h>
#include <stdlib.h>

#define MAX_CMD_LINE_LEN                BUF_LENGTH
#define MAX_RESP_LINE_LEN               BUF_LENGTH

#define     NEWLINE_CHAR                '\r'
// ms - the timer interrupt period is 1ms, that's why we set the timeout to 2 to be sure
// it's going to be between 1 and 2 ms
#define RECEIVE_DATA_TIMEOUT			2	// ms	 
#define TX_DATA_IDLE_TIME				3	// ms
#define RADIO_INIT_TIME					1000	// ms

//const BYTE*	pCRLF ="\r\n";
static TIMERHANDLE thZbStateTimer; // Handle to timer to manage current state
static TIMERHANDLE thZbTxTimer; 	// Timer used to provide TX_DATA_IDLE_TIME distance between packets

// Serial port definitions
static UART_OBJECT zbUARTObj;

static BYTE byZbRxBuffer[MAX_RESP_LINE_LEN];
static WORD wZbRxIndex;
static BOOL radioMode;
// Tx buffer must be wide enough to hold any commands to the modem. Command data
// must remain in the buffer until the lower-layer UART is done sending the data.
static BYTE byZbTxBuffer[MAX_CMD_LINE_LEN];
static WORD wZbTxCount;

//static BYTE byZbDataBuffer[MAX_CMD_LINE_LEN];

static BOOL ZbFlowControlEnable;


static BOOL ChangeChannelPending = FALSE;
static BYTE	currentChannel = 0;
static BYTE newChannel = 0;

static const BYTE radioChannels[NBR_RF_CHANNELS]={ZB_CHANNEL_1, ZB_CHANNEL_2, ZB_CHANNEL_3, ZB_CHANNEL_4};

//static const PCHAR atZbErrorCodeTable[NBR_AT_ZB_ERR_CODES] =  { "OK", "", "", "ERROR", "", };

//static BYTE ZbParseResponseValue( const ZB_CMD_LINE_ENTRY* pCmdEntry, PCHAR szRespLine ); 

//------------------------------------------------------------------------------
//  Must be the first function called in this routine.
// This function initializes all internal static variables.
BOOL InitRadioInterface( void) {
	// reset the Zigbee module 
	ResetRadio();
	
	SetOutputPin( ZB_UART_RTS, FALSE );
	__delay_cycles(600);
    // Init state vars
	if( thZbStateTimer == 0 ) {
		thZbStateTimer = RegisterTimer();
	}

	if( thZbTxTimer == 0 ) {
		thZbTxTimer = RegisterTimer();
	}
    
    // Clear the comm buffers
    wZbRxIndex = 0;
    wZbTxCount = 0;
    ZbFlowControlEnable = FALSE;
	UARTA1Init();
	UARTA1RxEnable();	// enable RX
	StartTimer( thZbStateTimer );
	StartTimer( thZbTxTimer );

	ResetTimer( thZbStateTimer, RADIO_INIT_TIME );
	while(TimerExpired(thZbStateTimer)==FALSE) {
		ResetWatchdog();
	}
	SetRadioTimeout((WORD) RADIO_COMM_TIMEOUT);
	ResetTimer( thZbStateTimer, 10 );
	while(TimerExpired(thZbStateTimer)==FALSE) {
		ResetWatchdog();
	}
	SetRadioChannel();
	ResetTimer( thZbStateTimer, 5 );
	while(TimerExpired(thZbStateTimer)==FALSE) {
		ResetWatchdog();
	}
//	SetRadioPower(DEFAULT_RADIO_POWER);
	radioMode = TRUE;
    // Init is good!
    return TRUE;
}

//------------------------------------------------------------------------------
// This function is called in the context of an UART TX ISR.
void atZbTxISR( void ) {

    if( zbUARTObj.wTxQIndex < zbUARTObj.wTxQSize ) {
        UCA1TXBUF = zbUARTObj.ptrTxQ[zbUARTObj.wTxQIndex];
        zbUARTObj.wTxQIndex++;
    }
    else {
        // Disable TX interrupt
        UARTA1TxDisable();
        UARTA1RxEnable();
        zbUARTObj.bTxQSending = FALSE;
        SetOutputPin(ZB_UART_RTS, FALSE);
    }
}

//------------------------------------------------------------------------------
//  This function is called in the context of an UART RX ISR.
void atZbRxISR( void ) {
 
	if( wZbRxIndex < MAX_RESP_LINE_LEN ) {
    	byZbRxBuffer[wZbRxIndex] = UCA1RXBUF;
        ++wZbRxIndex;
		ResetTimer( thZbStateTimer, RECEIVE_DATA_TIMEOUT );        
    }
}

//------------------------------------------------------------------------------
// Call this function to check if packet has been received
// the end of packet for now is received bytes >0, and nothing has been received for 1ms
BOOL ReceiverHasData(void) {

	BOOL bResult = FALSE;
	
	if (wZbRxIndex>0)
		if(TimerExpired(thZbStateTimer))
			bResult = TRUE;
			
	return bResult;		
}

//------------------------------------------------------------------------------
//  Copy UART data to the supplied buffer
WORD ReadReceiverData(BYTE *pData, WORD maxSize) {
	WORD wCnt;
	
	wCnt = 0;
	UARTA1RxDisable();		// disable RX interrupt 

	while( ( wZbRxIndex ) && ( wCnt < maxSize ) ) {
		*pData = byZbRxBuffer[wCnt];
		pData++;
		wCnt++;
		wZbRxIndex--;
	}
	wZbRxIndex = 0;			// flush the byZbRxBuffer
	UARTA1RxEnable();		// enable RX interrupt
	return wCnt;
}

//------------------------------------------------------------------------------
//  Set the UART flow control enable flag
void ZbEnableFlowControl(void) {
	ZbFlowControlEnable = TRUE;	
}

//------------------------------------------------------------------------------
//  Start the UART transmission.
BOOL ZbTiSendBinaryData( BYTE* pData, WORD wDataLen  ) {
	memcpy( byZbTxBuffer, pData, wDataLen );
			
	SetOutputPin(ZB_UART_RTS, TRUE );
    wZbTxCount = wDataLen;

  	FlushUartSerialTxQueue( &zbUARTObj );
 
	if ( ZbFlowControlEnable == TRUE ) {
		while( ReadInputPin( ZB_UART_CTS ) == TRUE );
	}
	
	if( !UARTA1SendBuffer( &zbUARTObj, byZbTxBuffer, wZbTxCount ) )
    	return FALSE;
    	
    return TRUE;	
}

//------------------------------------------------------------------------------
//  Start the UART transmission.
BOOL ZbTiSendCommand( BYTE* pData, WORD wDataLen  ) {
	memcpy( byZbTxBuffer, pData, wDataLen );

	SetOutputPin(ZB_UART_RTS, TRUE );
    wZbTxCount = wDataLen;

  	FlushUartSerialTxQueue( &zbUARTObj );

	if (ZbFlowControlEnable ==TRUE) {
		while(ReadInputPin(ZB_UART_CTS)==TRUE) {
			ResetWatchdog();
		}
	}

	if( !UARTA1SendBuffer( &zbUARTObj, byZbTxBuffer, wZbTxCount ) )
    	return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Description: return the TX sending status
BOOL IsZbTiSending(void) {
	return zbUARTObj.bTxQSending;
}

//------------------------------------------------------------------------------
//  Description: return the TX sending status
BYTE GetRfChannel(void) {
	if(currentChannel>=NBR_RF_CHANNELS)
		currentChannel = 0;
	return radioChannels[currentChannel];
}

//------------------------------------------------------------------------------
//  Send a power down command to ZB radio module
void PowerDownRadio(void) {
	BYTE bData[4];

	bData[0]= COMMAND;
	bData[1]= RADIO_POWER_DOWN;
	bData[3]= 0;
	ZbTiSendCommand( bData, 2  );
	while( IsZbTiSending() ) {
		ResetWatchdog();
	}
	radioMode = FALSE;
}

//------------------------------------------------------------------------------
//  Description: Increase the current radio channel number
void IncreaseZbChannel(void) {
	currentChannel++;
	if(currentChannel>=NBR_RF_CHANNELS)
		currentChannel = 0;
}

//------------------------------------------------------------------------------
//  Description: Set the rf channel on the radio
void SetRadioChannel(void) {
	BYTE bData[4];
	bData[0]= COMMAND;
	bData[1]= RADIO_CHANGE_CHANNEL;
	bData[2]= radioChannels[currentChannel];
	ZbTiSendCommand( bData, 3  );
	while( IsZbTiSending() ) {
		ResetWatchdog();
	}
	ChangeChannelPending = FALSE;
}

//  Reset the radio chip
void ResetRadio(void) {
	SetOutputPin(ZB_RSTn, FALSE);
	__delay_cycles(200);
	SetOutputPin(ZB_RSTn, TRUE);
	radioMode = TRUE;
}

//------------------------------------------------------------------------------
//  Verify radio chip power mode
BOOL IsRadioActive(void) {
	return radioMode;
}

//------------------------------------------------------------------------------
//  Put channel change in the queue
void SetChangeChannelPending(BYTE channel) {
	WORD i;

	for(i=0;i<NBR_RF_CHANNELS;i++) {
		if(radioChannels[i]== channel) {
			newChannel = i;
			ChangeChannelPending =TRUE;
		}
	}
}

//------------------------------------------------------------------------------
//  Verify if new channel needs to be set on the radio
BOOL IsChannelChangePending(void) {
	return ChangeChannelPending;
}

//------------------------------------------------------------------------------
//  Update the current RF channel from the queue and set it on the radio
void SetNewChannel(void) {
	currentChannel = newChannel;
	SetRadioChannel();
}

//------------------------------------------------------------------------------
//  Set the radio TX power
void SetRadioPower(BYTE txPower) {
	BYTE bData[4];
	bData[0]= COMMAND;
	bData[1]= RADIO_CHANGE_POWER;
	bData[2]= txPower;
	ZbTiSendCommand( bData, 3  );
	while( IsZbTiSending() ) {
		ResetWatchdog();
	}
}

//------------------------------------------------------------------------------
//  Turn off the radio receiver.
void SetRadioReceiverOff(void) {
	BYTE bData[4];
	bData[0]= COMMAND;
	bData[1]= RADIO_RCVR_OFF;
	bData[3]= 0;
	ZbTiSendCommand( bData, 2  );
	while(IsZbTiSending()) {
		ResetWatchdog();
	}
}

//------------------------------------------------------------------------------
//  Turn on the radio receiver.
void SetRadioReceiverOn(void) {
	BYTE bData[4];
	bData[0]= COMMAND;
	bData[1]= RADIO_RCVR_ON;
	bData[3]= 0;
	ZbTiSendCommand( bData, 2  );
	while( IsZbTiSending() ) {
		ResetWatchdog();
	}
}

//------------------------------------------------------------------------------
//  Turn off the radio receiver for a period of  msTimeoff.
//  After that the radio will turn on the receiver automatically.
void SetRadioOffTimed(WORD msTimeoff) {
	BYTE bData[6];
	bData[0]= COMMAND;
	bData[1]= RADIO_RCVR_OFF_TIMED;
	bData[2]= (BYTE)msTimeoff;
	bData[3]= (BYTE)(msTimeoff>>8);
	bData[4]= 0;

	ZbTiSendCommand( bData, 4  );
	while(IsZbTiSending()) {
		ResetWatchdog();
	}
}

//------------------------------------------------------------------------------
// This is a system watchdog timer where the main processor needs to send a pulse or the radio will reset it.
//  Set the radio communication timeout - radio will reset the MCU if it does not receive data over
//  the UART for more than timeout ms.
void SetRadioTimeout(WORD msTimeout) {
	BYTE bData[6];
	bData[0]= COMMAND;
	bData[1]= RADIO_SET_TIMEOUT;
	bData[2]= (BYTE)msTimeout;
	bData[3]= (BYTE)(msTimeout>>8);
	bData[4]= 0;

	ZbTiSendCommand( bData, 4  );
	while(IsZbTiSending()) {
		ResetWatchdog();
	}
}

