#ifndef GYRO_ST_H_
#define GYRO_ST_H_


#include "..\drivers.h"


#define MAX_ST_OUT_SIZE	25
#define MAX_ST_IN_SIZE  25

#define ST_WAKEUP_PERIOD	250

#define ST_PWR		        0x01		// P1.7 had damage so we are using P3.0. A high applies power to the L3G4200D
#define ST_CSn		        0x04		// P2.2 (not per the schematic)
#define ST_AVG_BUFF_SIZE	10	        // number of values that are averaged for avgSTz
#define ST_READ			    0x80	    // MS bit is high in address if read is requested
#define ST_MULTI_ADS	    0x40	    // Will auto increment the address if high
#define GET_TEMP_CMD	    0x26
#define SET_REG1		    0x20
#define POWER_DOWN_GYRO     0x00
#define X_EN                0x01        // Enable X axis reading
#define Y_EN                0x02        // Enable y axis reading
#define Z_EN                0x04        // Enable z axis reading
#define PD                  0x08        // ST gyro Power Down (0:Power Down, 1:normal mode)
#define BW0                 0x10        // Band width
#define BW1                 0x20
#define DR0                 0x40        // Data Rate
#define DR1                 0x80
#define SET_REG3		    0X22
#define I2_EMPTY            0x01        // FIFO empty
#define I2_ORUN             0x02        // FIFO overrun
#define I2_WTM              0x04        // FIFO watermark
#define I2_DRDY             0x08        // FIFO Data Ready
#define PP_OD               0x10        // Push-Pull/Open_Drain (0:PP, 1:OD)
#define H_LACTIVE           0x20        // Interrupt active (0:High, 1:Low)
#define I1_BOOT             0x40        // Boot Status available on INT1
#define I1_INT1             0x80        // Interrupt Enable

#define SET_REG4		    0X23
#define SET_REG5		    0X24
#define INT1_CFG            0x30        // register for digital gyro interrupt source(s)
#define INT1_THS_ZH         0x36        // High byte register for Z interrupt threshold level
#define INT1_THS_ZL         0x37        // Low byte register for Z interrupt threshold level
#define FIFO_ENABLE		    0X40
#define OUT_Z_L			    0x2C	    // Can be read in a continuous sequence
#define OUT_Z_H			    0x2D
#define SET_FIFO		    0x2E
#define SET_SELF_TEST	    0X02
#define SET_SELF_TEST_2K	0X32	    // SELF TEST AT 2000dps
#define SET_2K_DPS		    0X30
#define SET_BDU			    0X80
#define ID_REG			    0x0F
#define FILLER			    0x00	    // Needed to shift extra bytes out during a read
#define ST_GYRO_TOTAL	    1

#define ZHIE                0x20        // bit 6 on INT1_CFG register causes an interrupt on P1.6 of the uP
#define ZLIE                0X10        // Bit 5 will allow reverse turns to generate an interrupt on P1.6 of uP

//extern unsigned char SToutBuff[MAX_ST_OUT_SIZE];
//extern unsigned char SToutPtr;
//extern unsigned char STtxSize;

//extern unsigned char STinBuff[MAX_ST_IN_SIZE];
//extern unsigned char STinPtr;

void initST(void);
void readSTtemp(void);
//void setSTreg1(void);
//void setSTreg3(void);
void getSTz(void);
void dispSTz(void);
void analyzeSTz(void);
//void ST4200Send(unsigned char * data, unsigned char size);
void calibrateStGyro(void);
void readSTid(void);
char gedStId(void);
void setSTfifo(void);
BOOL stGyroHasData(void);
BOOL stGyroReadData( int *pResult);
//BOOL stGyroReadData( int *pResultRPM,int *pResultDPS, BYTE gNumber);
void SelfTestStGyro(void);
void setSTNormalMode(void);
//void setSTreg3NormalMode(void);
void setDigGyroWakeInterrupt(void);
void disableDigGyroWakeInterrupt(void);
void setSTreg(char reg, char value);
void setDigGyroWakeOnTurns(void);
void GetSTGyroData(void);
//WORD GetRPM(void);
BYTE GetRPM(void);
void PowerDownStGyro(void);
void WakeUpStGyro(void);

#endif /*GYRO_ST_H_*/
