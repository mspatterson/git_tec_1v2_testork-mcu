/*
 * board_gpio.c
 *
 *  Created on: 2012-12-03
 *      Author: DD
 */
#include "board_gpio.h"

#define DEBOUNCE_TIME	500
#define SWITCH_ON	0x00
#define SWITCH_OFF	0xFF
#define SWITCH_HIGH		TRUE
#define SWITCH_LOW		FALSE
//static BYTE swOnState;
//static BYTE swOffState;
//static BYTE swOldState;
//static BYTE swNewState;
static TIMERHANDLE thDebounce = 0;
extern BOOL reedSwitchEvent;

//  This function initialize the switch input and register a timer for debounce
void ReedSwitchInit(void) {
	SetPinAsInput(REED_SWITCH);	
	if(thDebounce==0) {
		thDebounce = RegisterTimer();
	}

	//GetSwitchInitState();                     // Don't need. Button is now momentary that goes low when pressed
}


//  Returns: TRUE if the switch has not changed state and
//			 FALSE if the switch has changed state
//  Description: This function read and debounce the SWITCH input
//               When the switch has changed his state the board goes to power down
// June 2019 BDD. Changed the function so that it only looks for a "low" as we only use a momentary push button now.
//                Now checks to see if button is "low" during entire debounce time
BOOL IsSwitchOn(void) {

	if( !ReadInputPin( REED_SWITCH ) ) {                    // if button or reed is being pressed
		ResetTimer( thDebounce, DEBOUNCE_TIME );
		StartTimer( thDebounce );
		while( !TimerExpired( thDebounce ) ) {
		    if( ReadInputPin( REED_SWITCH ) )
		        return TRUE;                                // button was released before debounce timeout
		}
		return FALSE;                               // button was pressed continuously during debounce
	}
	return TRUE;                                    // button is not being pressed
}

//  setup interrupt on REED_SWITCH (P2.5) toggle.
//  check the current switch state and setup the edge accordingly.
//	If the input is high - setup High/Low edge
//	If the input is low  - setup Low/High edge
// June 2019 BDD, Set up for only high/low edge as we only use momentary contact now
void SetReedSwitchInterrupt(void) {
	__bic_SR_register(GIE);
	P2IE |= 0x20;                                   // P2.5 interrupt enabled
	P2IES |= 0x20;                                  // set P2.5 High/Low edge
	P2IFG = 0x00;                                   // P2.5 IFG cleared
	__bis_SR_register(GIE);
	reedSwitchEvent = FALSE;
}

//  disable interrupt on REED_SWITCH (P2.5) toggle.
void DisableReedSwitchInterrupt(void) {
	__bic_SR_register(GIE);
	P2IE &= ~0x20;                                  // P2.5 interrupt disabled
	P2IFG &= ~0x20;                                 // P2.5 IFG cleared
	__bis_SR_register(GIE);
}

//  Port 2 interrupt service routine. Used to wake the system from deep sleep
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void) {
	if(P2IFG&0x20) {						        // interrupt from P2.5-REED_SWITCH
		//LPM4_EXIT;                                  // Wake processor
	    LPM0_EXIT;                                // Macro that alters the register to wake the uP
	}
	P2IFG = 0;                                      // Clear all interrupt flags
	reedSwitchEvent = TRUE;
}

//  Enable Analog Power Supply
void EnableAnalogPower(void) {
	SetPinAsOutput(EN_VA);
	SetOutputPin(EN_VA,ON);
}

//  Disable Analog Power Supply
void DisableAnalogPower(void) {
	SetPinAsOutput(EN_VA);
	SetOutputPin(EN_VA,FALSE);
}

//  Description:  Enable Digital Power Supply
void EnableDigitalPower(void) {
	SetPinAsOutput(EN_VD);
	SetOutputPin(EN_VD,TRUE);
}

//  Description:  Disable Digital Power Supply
void DisableDigitalPower(void) {
	SetPinAsOutput(EN_VD);
	SetOutputPin(EN_VD,FALSE);
}

//  Description:  Enable Radio Power Supply
void EnableRadioPower(void) {
	SetPinAsOutput(EN_VR);
	SetOutputPin(EN_VR,TRUE);
}

//  Description:  Disable Radio Power Supply
void DisableRadioPower(void) {
	SetPinAsOutput(EN_VR);
	SetOutputPin(EN_VR,FALSE);
}

//  Description:  Enable Analog Gyro Power Supply
void EnableAnalogGyro(void) {
	SetOutputPin(EN_AG,TRUE);
}

//  Description:  Disable Analog Gyro Power Supply
void DisableAnalogGyro(void) {
	SetOutputPin(EN_AG,FALSE);
}

//  Description:  Read the 3 HW_REV inputs to determine the hardware revision
BYTE GetHardwareRev(void) {
	static BYTE byVar;
	byVar = 0;
	SetPinAsInput(HWD_REV_IN1);
	SetPinAsInput(HWD_REV_IN2);
	SetPinAsInput(HWD_REV_IN0);

	if(ReadInputPin(HWD_REV_IN0))
		byVar = 1;
	if(ReadInputPin(HWD_REV_IN1))
		byVar |= 1<<1;
	if(ReadInputPin(HWD_REV_IN2))
		byVar |= 1<<2;

	//*pByte = byVar;
	return byVar;
}

//  Description:  Enable All Power Supplies
void EnableAllPower(void) {
	EnableAnalogPower();
	EnableDigitalPower();
	EnableRadioPower();
//	EnableAnalogGyro();

}

//  Description:  Disable All Power Supplies
void DisableAllPower(void) {
	DisableAnalogPower();
	DisableDigitalPower();
	DisableRadioPower();
	DisableAnalogGyro();
}

//  Description:  Enable Battery Sense Voltage Divider
void EnableBatSense(void) {
	SetPinAsOutput(BAT_SENSE_EN);
	SetOutputPin(BAT_SENSE_EN,TRUE);
}

//  Description:  Disable Battery Sense Voltage Divider
void DisableBatSense(void) {
	SetPinAsOutput(BAT_SENSE_EN);
	SetOutputPin(BAT_SENSE_EN,FALSE);
}

//  Description:  Flash the switch LED for power up
void LedSwPowerUpSequence(void) {
	BOOL boolVar;
	BYTE bVar;
	boolVar = FALSE;
	bVar = 6;
	if(thDebounce==0) {
		thDebounce = RegisterTimer();
	}
	while(bVar--) {
		boolVar ^=1;
		ResetTimer( thDebounce, 500 );
		StartTimer( thDebounce );
		while(TimerExpired( thDebounce)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(SW_LED, boolVar );
	}
	SetOutputPin(SW_LED, FALSE );
}

//  Description:  Dim the switch LED gradually over 3 seconds
void LedSwPowerDownSequence(void) {
	WORD cycleCounter;
	register WORD pwmCounter = 0;
	volatile WORD pwmOnCycle;
	cycleCounter = 368;
	if(thDebounce==0) {
		thDebounce = RegisterTimer();
	}
	pwmOnCycle = 740;
	StartTimer( thDebounce );
	while(cycleCounter--) {
		ResetTimer( thDebounce, 10 );
		while(TimerExpired( thDebounce )==FALSE) {
			ResetWatchdog();
			pwmCounter++;
			if(pwmCounter>pwmOnCycle)
				SetOutputPin(SW_LED, FALSE );
		}
		SetOutputPin(SW_LED, TRUE );
		pwmCounter = 0;
		pwmOnCycle -=2;
	}
	SetOutputPin(SW_LED, FALSE );
}

//  Description:  Enable External Power Supply
void EnableExtPower(void) {
	SetPinAsOutput(EN_V_EXT);
	SetOutputPin(EN_V_EXT,TRUE);
}

//  Description:  Disable External Power Supply
void DisableExtPower(void) {
	SetPinAsOutput(EN_V_EXT);
	SetOutputPin(EN_V_EXT,FALSE);
}

//*************************************************************** Archive
//  This function determine the initial state of the
//               switch - HIGH/LOW
// June 2019 BDD, No longer needed since button is now momentary that
//void GetSwitchInitState(void) {
//    if(ReadInputPin(REED_SWITCH)) {
//        swNewState = SWITCH_HIGH;
//        swOldState = SWITCH_HIGH;
//    } else {
//        swNewState = SWITCH_LOW;
//        swOldState = SWITCH_LOW;
//    }
//    ResetTimer( thDebounce, DEBOUNCE_TIME );
//    StartTimer(thDebounce);
//    while(!TimerExpired( thDebounce)) {
//        if(ReadInputPin(REED_SWITCH)) {
//            swNewState = SWITCH_HIGH;
//        } else {
//            swNewState = SWITCH_LOW;
//        }
//
//        if(swNewState!=swOldState) {
//            ResetTimer( thDebounce, DEBOUNCE_TIME );
//            swOldState = swNewState;
//        }
//        ResetWatchdog();
//    }
//    swOnState = swNewState;
//}

//void LedSwPowerDownSequence(void)
//{
//  BOOL boolVar;
//  BYTE bVar;
//  boolVar = FALSE;
//  bVar = 6;
//  if(thDebounce==0)
//  {
//      thDebounce = RegisterTimer();
//  }
//  while(bVar--)
//  {
//      boolVar ^=1;
//      ResetTimer( thDebounce, 500 );
//      StartTimer( thDebounce );
//      while(TimerExpired( thDebounce)==FALSE)
//      {
//          ResetWatchdog();
//      }
//      SetOutputPin(SW_LED, boolVar );
//  }
//  SetOutputPin(SW_LED, FALSE );
//}

