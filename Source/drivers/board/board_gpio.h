#ifndef BOARD_GPIO_H_
#define BOARD_GPIO_H_


#include "..\drivers.h"

void ReedSwitchInit(void);
BOOL IsSwitchOn(void);
void SetReedSwitchInterrupt(void);
void DisableReedSwitchInterrupt(void);
void EnableAnalogPower(void);
void DisableAnalogPower(void);
void EnableDigitalPower(void);
void DisableDigitalPower(void);
void EnableRadioPower(void);
void DisableRadioPower(void);
void EnableAnalogGyro(void);
void DisableAnalogGyro(void);
BYTE GetHardwareRev(void);
void EnableAllPower(void);
void DisableAllPower(void);
void EnableBatSense(void);
void DisableBatSense(void);
void GetSwitchInitState(void);
void LedSwPowerUpSequence(void);
void LedSwPowerDownSequence(void);
void EnableExtPower(void);
void DisableExtPower(void);
#endif /*BOARD_GPIO_H_*/
