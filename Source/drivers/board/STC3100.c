//******************************************************************************
//
//  BatteryGas.c: Module Title
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module contains the code to read and write the I2C.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-Mar-14     CT          Initial Implementation
//  Nov 2019        BDD         Combined BatGasGuage and BatGasGuagei2c into one
//                              and called it STC3100.C. Refactored the code and
//                              removed many extra un-needed lines.
//*******************************************************************************

#include "STC3100.h"

#define RUN_MODE         0x10
#define RESET_CHARGE_ACC 0x02
#define POWER_DOWN       0x00
#define STC3100_PORTNUM       2
#define STC3100_DEV_ADDR_W    0xE0
#define STC3100_DEV_ADDR_R    0xE1

#define STC3100_DEV_BIT_RATE  90000       //400 kHz max (fast), 100 kHz (standard mode)
#define STC3100_ADDRESS         0x70

#define BAT_GAUGE_BUF_SIZE      20
#define MAX_BATT_CHARGE 0xFFFF
#define BATT_CHARGE_STEPS_AS_ONE_PERCENT 42

#define BATT_MAX_CHARGE_LEVEL 65535
#define BATT_MIN_CHARGE_LEVEL 61335
#define MID_POINT_UNUSED_RANGE ( BATT_MIN_CHARGE_LEVEL  / 2 )

#define UV_CNT_LIMIT     4
#define UVLOW_THRESHOLD  1270
#define CHARGE_VOLTAGE_MAX 1434

typedef enum {
    //Control Registers
    REG_MODE = 0,
    REG_CTRL = 1,
    REG_CHARGE_LOW = 2,
    REG_CHARGE_HIGH = 3,
    REG_COUNTER_LOW = 4,
    REG_COUNTER_HIGH = 5,
    REG_CURRENT_LOW = 6,
    REG_CURRENT_HIGH = 7,
    REG_VOLTAGE_LOW = 8,
    REG_VOLTAGE_HIGH = 9,
    REG_TEMP_LOW = 10,
    REG_TEMP_HIGH = 11,

    //Device ID Registers
    REG_ID0 = 24,
    REG_ID1 = 25,
    REG_ID2 = 26,
    REG_ID3 = 27,
    REG_ID4 = 28,
    REG_ID5 = 29,
    REG_ID6 = 30,
    REG_ID7 = 31,

    //RAM Registers
    //#define REG_RAM0  32
    //...
    //#define REG_RAM31 63

    NUM_BATTGAS_REGS
} BatteryGas_Reg;


// Converting Battery level to display indicator:
//===============================================================================
// Gas gauge indicates 'full' at 65536 (0xffff) reading from the battery charge
// accumulator. Now:
//
// Accumulator Resolution:  0.6mAh
// total battery charge:    2500 mAh
// So,  2500/0.6 ~ 4200 steps from 'full' to 'empty'
//
// 4200/100 = 42 steps for each percentage point in the batt. charge
//===============================================================================


//******************************************************************************
// Battery Voltage Monitoring:
//
// STC3100 Resolution: 2.44 mV / LSB
//
// Under-voltage Lockout: Set to 2.75 V
//
//   3.1 V
// ----------  = 1270
// 2.44 mV/LSB
//
//******************************************************************************

//static  BYTE byUVEventCount = 0;
//static  DWORD dwInitCount   = 0;
static BYTE batLife = 0;
static WORD wBatLife = 0;
BYTE bGasArray[30] = {0};

GAS_GAUGE_DATA sGasGaugeData;
TIMERHANDLE thGasTimer;

I2C_OBJECT  i2cBatGauge;
BYTE RxBatGaugeBuffer[BAT_GAUGE_BUF_SIZE];
BYTE TxBatGaugeBuffer[BAT_GAUGE_BUF_SIZE];

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------
//static BOOL ReadBatteryGasReg ( BatteryGas_Reg regAddr, BYTE* pData );
//static BOOL WriteBatteryGasReg( BatteryGas_Reg regAddr, BYTE Data );
//static BYTE convertBatteryChargeToPercent( WORD wBattChargeLevel );
//static BOOL ReadBatteryGasRegChargeLevel (WORD* ptrResult );

//------------------------------------------------------------------------------
// Called by main(), this sets up all registers for the gas gauge and it is never
//  shut down until battery is removed or dies.
void InitGasGauge(void) {

    //  enable 3V3_D. Otherwise it keeps the I2C lines low.
    thGasTimer = RegisterTimer();
    ResetTimer( thGasTimer, 100 );
    StartTimer( thGasTimer );
    while( !TimerExpired( thGasTimer ) ) {
        ResetWatchdog();
    }

    ResetTimer( thGasTimer, 1000 );
    BatteryGasMonitorReadAllRegs();
    while( STC3100DataReady() == FALSE ) {
        ResetWatchdog();
        if( TimerExpired( thGasTimer ) )
            break;
    }
    STC3100GetRawData( bGasArray, sizeof( bGasArray ) );
    ResetTimer( thGasTimer, 1000 );
    BatteryGasMonitorReadId();
    while( STC3100DataReady() == FALSE ) {
        ResetWatchdog();
        if( TimerExpired( thGasTimer ) )
            break;
    }

    STC3100GetRawData( bGasArray, sizeof( bGasArray ) );

    ResetTimer( thGasTimer, 1000 );

    ResetBatteryGasMonitor();

    while( !TimerExpired( thGasTimer ) ) {
        ResetWatchdog();
    }
    ResetTimer( thGasTimer, 1000 );
    BatteryGasMonitorReadAllRegs();
    while( STC3100DataReady() == FALSE ) {
        ResetWatchdog();
        if( TimerExpired( thGasTimer ) )
            break;
    }
    STC3100GetRawData( bGasArray, sizeof( bGasArray ) );

    UnregisterTimer(thGasTimer);
}

//------------------------------------------------------------------------------
// Reads all the registers from the Gas Gauge and stores them in
// RxBatGaugeBuffer[]
void BatteryGasMonitorReadAllRegs(void) {
	STC3100I2cRead(REG_MODE, 12);
}

//------------------------------------------------------------------------------
// Reads the internal ID of the gas gauge. All devices are the same.
void BatteryGasMonitorReadId(void) {
	STC3100I2cRead(REG_ID0, 8);
}

//------------------------------------------------------------------------------
// Resets the Gas Gauge
// Called from InitGasGauge()
void ResetBatteryGasMonitor( void ) {
    BYTE pByte[4];

    pByte[0] = RUN_MODE;
    pByte[1] = RESET_CHARGE_ACC;
    //switch from standby to run mode
    STC3100WriteRegs(REG_MODE, pByte,3 );
}

//------------------------------------------------------------------------------
// This will place all the values gathered from BatteryGasMonitorReadAllRegs()
// and place them in the structure sGasGaugeData for future reference.
// in reality, all we use is sGasGaugeData.ChargeDataMa that's stored in wBatLife
void ParseGasGaugeData(BYTE *pData) {
    WORD wTmp;
    sGasGaugeData.ChargeDataRaw = ( (WORD)pData[3] << 8 ) | pData[2];
    sGasGaugeData.CurrentValue = ( (WORD)pData[7] << 8 ) | pData[6];
    sGasGaugeData.NumberOfConversion = ( (WORD)pData[5] << 8 ) | pData[4];
    sGasGaugeData.BatVoltageMv = (WORD)( ( ((DWORD)pData[9] << 8 ) | pData[8]) * 244 / 100);

    sGasGaugeData.ChargeDataMa = sGasGaugeData.ChargeDataRaw * (-2) / 10;

    wTmp = sGasGaugeData.BatVoltageMv /20;      // show batLife as voltage in 20mV steps
    batLife = (BYTE)wTmp;
    wBatLife = sGasGaugeData.ChargeDataMa;
}

//------------------------------------------------------------------------------
// Called from SendRFC() to give mA hours used since the battery was inserted
WORD GetBatUsedmAh(void) {
    return wBatLife;
}

//------------------------------------------------------------------------------
// This is not used as we get battery voltage from the processors A/D
WORD GetBatVoltageGasGauge(void) {
    return sGasGaugeData.BatVoltageMv;
}

//------------------------------------------------------------------------------
// Writes data to the coulomb counter
// regAddr = Starting address
// *pRegVal = data to send
// length is size of data
// Returns pass or fail
BOOL STC3100WriteRegs (BYTE regAddr, BYTE *pRegVal , BYTE length) {
    unsigned int i;

    if( (i2cBatGauge.bTxQSending == TRUE) || (i2cBatGauge.bRxQReceiving == TRUE) )
        return FALSE;

    i2cBatGauge.bySlaveAddress = STC3100_ADDRESS;
    i2cBatGauge.ptrTxQ = TxBatGaugeBuffer;
    i2cBatGauge.wTxQIndex = 0;
    i2cBatGauge.wTxQSize = length;
    i2cBatGauge.bTxQSending = TRUE;
    i2cBatGauge.ptrRxQ = RxBatGaugeBuffer;
    i2cBatGauge.wRxQIndex = 0;
    i2cBatGauge.wRxQSize = 0;
    i2cBatGauge.bRxQReceiving = FALSE;

    TxBatGaugeBuffer[0] = regAddr;                          // address of the first config register
    for( i = 0; i < (length-1); i++ ) {
        TxBatGaugeBuffer[i+1] = *pRegVal;                   // CRA_REG_M register data
        pRegVal++;                                          // increase the pointer
    }

    i2cWrite( &i2cBatGauge );

    return TRUE;
}

//------------------------------------------------------------------------------
// Reads requested number of bytes from the .  If the length
//    goes past the last address, it will wrap around to 0.
// Returns pass or fail
BOOL STC3100I2cRead( BYTE regAddr, WORD Length ) {

    if( ( i2cBatGauge.bTxQSending == TRUE ) || ( i2cBatGauge.bRxQReceiving == TRUE ) )
        return FALSE;

    memset( RxBatGaugeBuffer, 0, sizeof( RxBatGaugeBuffer ) );

    i2cBatGauge.bySlaveAddress = STC3100_ADDRESS;
    i2cBatGauge.ptrTxQ = TxBatGaugeBuffer;
    i2cBatGauge.wTxQIndex = 0;
    i2cBatGauge.wTxQSize = 1;
    i2cBatGauge.bTxQSending = TRUE;
    i2cBatGauge.ptrRxQ = RxBatGaugeBuffer;
    i2cBatGauge.wRxQIndex = 0;
    i2cBatGauge.wRxQSize = Length;
    i2cBatGauge.bRxQReceiving = TRUE;

    TxBatGaugeBuffer[0] = regAddr;                          // address of the first register
    i2cRead( &i2cBatGauge );

    return TRUE;
}

//------------------------------------------------------------------------------
// Transfers data read by BatteryGasMonitorReadAllRegs() to the
// passed value in *pData. Will transfer 12 bytes or maxSize,
// whichever is smaller.
void STC3100GetRawData(BYTE *pData, WORD maxSize) {
    unsigned int i = 0;

    while( ( i < 12 ) && ( i < maxSize ) ) {
        pData[i] = RxBatGaugeBuffer[i];
        i++;
    }
}

//------------------------------------------------------------------------------
// Called by InitGasGauge() and IdleTask()
BOOL STC3100DataReady(void) {
    return ( !i2cBatGauge.bRxQReceiving );                  // if it's receiving data, send a FALSE
}

//------------------------------------------------------------------------------
// Not currently called
BOOL STC3100TxDone(void) {
    return ( !i2cBatGauge.bTxQSending );
}

//------------------------------------------------------------------------------
// Not currently called
BYTE GetBatLife(void) {
    return batLife;
}

//------------------------------------------------------------------------------
// Currently is never called
void PowerDownBatteryGasMonitor( void ) {
    BYTE bData;

    bData = POWER_DOWN;
    //switch to standby mode
    STC3100WriteRegs(REG_MODE, &bData, 2 );
}

//------------------------------------Archived Code-----------------------------

//BOOL getBattVoltage( WORD* ptrBattVoltage )
//{

//    WORD wResult;
//    WORD wLength;
//    BYTE byAddr;
//    BYTE byVoltageLow;
//    BYTE byVoltageHigh;
//    BOOL bResult;
//    BOOL bReadGood;
//
//    bReadGood = TRUE;
//    wLength = 1;
//
//    // read the low byte
//    byAddr  = REG_VOLTAGE_LOW;
//    bResult = STC3100I2cRead( byAddr, &byVoltageLow, wLength );
//    if( !bResult)
//    {
//        bReadGood = FALSE;
//    }
//
//    // read the high byte
//    byAddr  = REG_VOLTAGE_HIGH;
//    bResult = STC3100I2cRead( byAddr, &byVoltageHigh, wLength );
//    if( !bResult)
//    {
//        bReadGood = FALSE;
//    }
//
//
//    wResult = byVoltageHigh;
//    wResult <<= 8;
//    wResult |= byVoltageLow;
//
//    *ptrBattVoltage = wResult;
//    return bReadGood;
//}

//******************************************************************************
//
//  Function: STC3100I2cBusy
//
//  Arguments: none
//
//  Returns: TRUE if  is busy (internal write), else FALSE if ready for new write
//
//  Description: Checks if  is busy with internal write.
//
//******************************************************************************
//BOOL STC3100I2cBusy( void )
//{
    // write to  with no address or data, if success  is not busy
//    if( I2cWrite( STC3100_PORTNUM, STC3100_DEV_ADDR_W, NULL, 0, TRUE ) == I2C_WRITE_SUCCESS )
//    {
//        return( FALSE );
//    }

    // fall through means  is busy
//    return( TRUE );
//}

////  Reads a single register from Gas Gauge
//void InitBatteryGasMonitor( void ) {
//    BYTE data;
//    data = RUN_MODE;
//    //switch from standby to run mode
//    STC3100WriteRegs(REG_MODE, &data, 2 );
//
//}

//******************************************************************************
//
//  Function:  UpdateBatteryGasMonitor
//
//  Arguments: None
//
//  Returns:   TRUE if successful, else FALSE
//
//  Description: Checks on the battery state of charge and returns this value;
//               the value returned will range from 0 to 5; 0 indicates no
//               charge, 5 indicates fully charged
//
//******************************************************************************
//BOOL UpdateBatteryGasMonitor( BYTE* ptrNewChargeLevel )
//{
//
//    BOOL bBattVoltValid;
//    WORD wBattVoltage;
//	BYTE byChargeLevelInPercent;
//
//    // Take a reading of the current charge level
//    WORD wChargeLevel;
//
//
//
//    if( !ReadBatteryGasRegChargeLevel( &wChargeLevel ) )
//    {
//        *ptrNewChargeLevel = 0;
//        return FALSE;
//    }
//
//    //Check 1
//    //
//    // test the charge level that we get from the coulomb counter for
//    // validity. By this we mean: does it fall into the range we expect.
//    // The reason it may not fall into this range is simple: the first time
//    // the chip powers up it sets the battery charge level at 65535; it
//    // then overflows to '0' if charge is flowing into the battery, or
//    // it starts decrementing if charge is flowing out of the battery. So,
//    // if you plug in an almost fully charged battery, it will power up
//    // to 65535, then the count will over flow to '0', and keep incrementing
//    // until the charger stops. At which point you'll have a very low
//    // value, which actually indicates a full battery.
//    if( wChargeLevel < MID_POINT_UNUSED_RANGE  )
//    {
//        // we must reset the charge level to the default so that the battery
//        // looks full.
//
//        // read the low byte
//        BYTE byAddr = REG_CTRL;
//        STC3100WriteRegs ( byAddr, RESET_CHARGE_ACC );
//
//        // over-ride the read value
//        wChargeLevel = 65535;
//    }
//
//    // Check 2
//    //
//    // We also check the battery voltage; we do this so that we can recover
//    // from cases where the coulomb counter has lost synchronization with
//    // the Battery. We do this as follows:
//    // *) monitor the battery voltage
//    // *) if it goes to 3.5V then the battery is very close to fully charged -
//    // *)      reset the coulomb counter to full charge in this case
//    bBattVoltValid = getBattVoltage( &wBattVoltage );
//    if( bBattVoltValid )
//    {
//        if( wBattVoltage > CHARGE_VOLTAGE_MAX )
//        {
//            // read the low byte
//            BYTE byAddr = REG_CTRL;
//            STC3100WriteRegs ( byAddr, RESET_CHARGE_ACC );
//
//            // over-ride the read value
//            wChargeLevel = 65535;
//
//        }
//
//    }
//
//
//    // convert the charge level reading into the range that is displayed by the
//    // system: 0 for no charge; 5 for fully charged
//    byChargeLevelInPercent = convertBatteryChargeToPercent( wChargeLevel );
//
//    *ptrNewChargeLevel = byChargeLevelInPercent;
//
//    return TRUE;
//}

//
//BYTE convertBatteryChargeToPercent( WORD wBattChargeLevel )
//{
//	BYTE byPercentLeft;
//    int iChargeLost = (int)(MAX_BATT_CHARGE - wBattChargeLevel);
//
//    BYTE byPercentPointsLost = 0;
//
//    while( (iChargeLost > BATT_CHARGE_STEPS_AS_ONE_PERCENT) && ( byPercentPointsLost < 100 ) )
//    {
//        iChargeLost -= BATT_CHARGE_STEPS_AS_ONE_PERCENT;
//        byPercentPointsLost++;
//    }
//
//    byPercentLeft = 100 - byPercentPointsLost;
//
//    return byPercentLeft;
//}

//******************************************************************************
// Private Functions
//******************************************************************************

//******************************************************************************
//
//  Function:    ReadBatteryGasRegChargeLevel
//
//  Arguments:
//
//  Returns:     BOOL - True or False according to read result
//
//  Description: Reads a single register.
//
//******************************************************************************
//BOOL ReadBatteryGasRegChargeLevel (WORD* ptrResult )
//{
//    WORD wLength;
//    BYTE byAddr;
//    BOOL bResult;
//
//    BYTE byChargeLow;
//    BYTE byChargeHigh;
//    WORD wResult;
//
//    wLength = 1;
//
//    // read the low byte
//    byAddr  = REG_CHARGE_LOW;
//    bResult = STC3100I2cRead( byAddr, &byChargeLow, wLength );
//
//    if( !bResult)
//         return FALSE;
//
//    // read the high byte
//    byAddr  = REG_CHARGE_HIGH;
//    bResult = STC3100I2cRead( byAddr, &byChargeHigh, wLength );
//
//    if( !bResult)
//         return FALSE;
//
//    wResult = byChargeHigh;
//    wResult <<= 8;
//    wResult |= byChargeLow;
//
//    *ptrResult = wResult;
//
//    return TRUE;
//}

//******************************************************************************
//
//  Function:  ReadBatteryGasReg
//
//  Arguments:
//
//  Returns:  TRUE if successful, else FALSE
//
//  Description: use to reads a single register location
//
//******************************************************************************
//BOOL ReadBatteryGasReg( BatteryGas_Reg regAddr, BYTE* pData )
//{
//    WORD wLength = 1;
//
//    if ( !STC3100I2cRead( regAddr, pData, wLength ) )
//        return FALSE;
//
//    return TRUE;
//}

//******************************************************************************
//
//  Function: WriteBatteryGasReg
//
//  Arguments:

//
//  Returns: TRUE if successful, else FALSE
//
//  Description: Writes a single register.
//
//******************************************************************************
//BOOL WriteBatteryGasReg( BatteryGas_Reg regAddr, BYTE regVal )
//{
//    if ( !STC3100WriteReg ( regAddr, regVal ) )
//        return FALSE;
//
//    return TRUE;
//}

//******************************************************************************
//
//******************************************************************************
//void ResetBattUnderVoltageMonitor( void )
//{
//    byUVEventCount = 0;
//    dwInitCount    = 0;
//}

//******************************************************************************
//
//******************************************************************************
//void updateBattUnderVoltageMonitor( BOOL* ptrBattUVLOIsValid, BOOL* ptrBattOk )
//{
//
//
//    WORD wBattVoltage;
//    BOOL bBattReadValid;
//
//    bBattReadValid = getBattVoltage( &wBattVoltage);
//    if(!bBattReadValid)
//    {
//        *ptrBattUVLOIsValid = FALSE;
//        *ptrBattOk          = TRUE;
//    }
//
//    // increment the 'init' counter which we use to check the number of samples
//    // that have been processed; we do this to determine if we have had enough
//    // samples to make a judgement on the battery:
//    dwInitCount++;
//    if( dwInitCount <  UV_CNT_LIMIT )
//    {
//        // we haven't taken enough samples to know what the battery state really
//        // is; therefore, we indicate that the UVLO is not yet valid
//        *ptrBattUVLOIsValid = FALSE;
//        *ptrBattOk          = TRUE;
//        return;
//
//    }
//
//    // now: we have taken enough samples to know if we are undervoltage or
//    //      otherwise:
//    *ptrBattUVLOIsValid = TRUE;
//
//    if( wBattVoltage <  UVLOW_THRESHOLD )
//    {
//        // The battery is below the UVLOW threshold! Increment the count and check
//        // to see if we have had the minimum number of consecutive undervoltage events
//        // to indicate 'battery not OK'
//        byUVEventCount++;
//        if( byUVEventCount >= UV_CNT_LIMIT )
//        {
//            *ptrBattOk          = FALSE;
//        }
//    }
//    else
//    {
//        // we are not undervoltage; therefore, we reset the undervoltage event counter
//        byUVEventCount = 0;
//        *ptrBattOk          = TRUE;
//    }
//}

//******************************************************************************
//
//  Function: I2cInit
//
//  Arguments: void
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: Initialize the I2C CODEC.
//
//******************************************************************************
//BOOL STC3100I2cInit( void )
//{
//
//  i2cBatGauge.bySlaveAddress = STC3100_ADDRESS;
//  i2cBatGauge.ptrTxQ = TxBatGaugeBuffer;
//      i2cBatGauge.wTxQIndex = 0;
//    i2cBatGauge.wTxQSize = 4;
//    i2cBatGauge.bTxQSending = TRUE;
//    i2cBatGauge.ptrRxQ = RxBatGaugeBuffer;
//    i2cBatGauge.wRxQIndex = 0;
//    i2cBatGauge.wRxQSize = 0;
//    i2cBatGauge.bRxQReceiving = FALSE;
//
//    // the MSB of the SUB address has to be '1' to allow multiple data read/write
//    TxBatGaugeBuffer[0] = 0x80;       // address of the first config register
//  TxBatGaugeBuffer[1] = DATA_RATE_220_HZ;     // CRA_REG_M register data
//  TxBatGaugeBuffer[2] = INPUT_RANGE_1P3_GAUSS;    // CRB_REG_M register data
//  TxBatGaugeBuffer[3] = CONT_MODE;                // MR_REG_M register data
//
//  i2cCompassWrite(&i2cBatGauge);
//
//    // no initialization required
//    return( TRUE );
//}





