/*
 * BatteryGas.h
 *
 *  Created on: 2012-03-14
 *      Author: Engineer
 */

#ifndef BATTERYGAS_H_
#define BATTERYGAS_H_

#include "..\drivers.h"


void InitBatteryGasMonitor( void );
   // initialize the coulomb counter that measures battery charge - the 'gas'
   // gauge for the battery


//BYTE UpdateBatteryGasMonitor( BYTE* ptrNewChargeLevel );
    // this process monitors battery charge level and reports back a percentage
    // of total battery charge remaining in battery; i.e., it reports 89% when
    // there is 89% of battery charge left


//void updateBattUnderVoltageMonitor( BOOL* ptrBattUVLOIsValid, BOOL* ptrBattOk );
    // this process monitors under-voltage condition on the battery; it should
    // be called every 5 seconds; if '*ptrBattUVLOIsValid' is true, then
    // '*ptrBattOk' indicate 'TRUE' if battery is OK, and 'FALSE' if the
    // 'filtered' battery voltage has gone under-voltage long enough to indicate
    // the system should be shut down from the battery.


//void ResetBattUnderVoltageMonitor( void );
    // resets the battery under-voltage monitor process described in 'updateBattUnderVoltageMonitor'

void BatteryGasMonitorReadAllRegs(void);
void BatteryGasMonitorReadId(void);
void ResetBatteryGasMonitor( void );
void PowerDownBatteryGasMonitor( void );
BYTE GetBatLife(void);
void ParseGasGaugeData(BYTE *pData);
WORD GetBatUsedmAh(void);
WORD GetBatVoltageGasGauge(void);

//BOOL STC3100I2cInit( void );
BOOL STC3100I2cBusy( void );
BOOL STC3100WriteRegs (BYTE regAddr, BYTE *pRegVal , BYTE length);
BOOL STC3100I2cRead( BYTE regAddr, WORD Length );
BOOL STC3100DataReady(void);
BOOL STC3100TxDone(void);
void STC3100GetRawData(BYTE *pData, WORD maxSize);
void InitGasGauge(void);

#endif /* BATTERYGAS_H_ */
