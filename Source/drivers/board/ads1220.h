/*
 * ads1220.h
 *
 *  Created on: 2012-05-18
 *      Author: Owner
 */

#ifndef ADS1220_H_
#define ADS1220_H_

#include "..\drivers.h"

//! Total number of ADC channels available
//#define ADC_EXT_MAX_CHANNELS	(6)

//! Bit resolution of the ADC
#define AD1220_BIT_RESOLUTION		(24)

//! Sample resolution of the ADC
#define ADS1220_RESOLUTION			(1<<AD1220_BIT_RESOLUTION)

#define ADS1220_LAST_REGISTER		3
#define ADS1220_TOTAL_REGISTERS		4




/*0x00h Register 0
Bits [7:4] MUX[3:0]: Input multiplexer configuration
These bits configure the input multiplexer. No effect when in temperature sensor mode.
0000: AIN0:AIN1 	1000: AIN0:GND
0001: AIN0:AIN2 	1001: AIN1:GND
0010: AIN0:AIN3 	1010: AIN2:GND
0011: AIN1:AIN2 	1011: AIN3:GND
0100: AIN1:AIN3 	1100: Ext. VREF/4 (PGA bypassed)
0101: AIN2:AIN3 	1101: (AVDD-AVSS)/4 monitor (PGA bypassed)
0110: AIN1:AIN0 	1110: Short to (AVDD-AVSS)/2
0111: AIN3:AIN2 	1111: Short to (AVDD-AVSS)/2
*/

// Analog input sources
// DO NOT CHANGE THESE VALUES! THEY CORRESPOND TO ACTUAL REGISTER BITS!
typedef enum ADS1220_INPUT_ENUM
{
	// 0000: AIN0:AIN1
	ADS1220_IN_AIN0_AIN1 		= 0<<4,

	// 0001: AIN0:AIN2
	ADS1220_IN_AIN0_AIN2		= 1<<4,

	// 0010: AIN0:AIN3
	ADS1220_IN_AIN0_AIN3		= 2<<4,

	// 0011: AIN1:AIN2
	ADS1220_IN_AIN1_AIN2		= 3<<4,

	// 0100: AIN1:AIN3
	ADS1220_IN_AIN1_AIN3		= 4<<4,

	// 0101: AIN2:AIN3
	ADS1220_IN_AIN2_AIN3		= 5<<4,

	// 0110: AIN1:AIN0
	ADS1220_IN_AIN1_AIN0		= 6<<4,

	// 0111: AIN3:AIN2
	ADS1220_IN_AIN3_AIN2		= 7<<4,

	// 1000: AIN0:GND
	ADS1220_IN_AIN0				= 8<<4,

	// 1001: AIN1:GND
	ADS1220_IN_AIN1				= 9<<4,

	// 1010: AIN2:GND
	ADS1220_IN_AIN2				= 10<<4,

	// 1011: AIN3:GND
	ADS1220_IN_AIN3				= 11<<4,

	// 1100: Ext. VREF/4 (PGA bypassed)
	ADS1220_IN_VREF				= (12<<4)|1,

	// 1101: (AVDD-AVSS)/4 monitor (PGA bypassed)
	ADS1220_IN_AVDD_BY_4		= (13<<4)|1,

	// 1110: Short to (AVDD-AVSS)/2
	ADS1220_IN_AVDD_BY_2		= 14<<4,

	// 1111: Short to (AVDD-AVSS)/2
	ADS1220_IN_AVDD_BY_2_1		= 15<<4,

	ADS1220_MAX_CHANNELS

} ADS1220_SOURCE;

/*0x00h Register 0
 * Bits [3:1] PGA[2:0]: Programmable gain amplifier configuration
	These bits configure the programmable gain amplifier. No effect when in temperature sensor mode.
	PGA_BYPASS = '0' 				PGA_BYPASS = '1'
	000: Gain = 1 					000: Gain = 1
	001: Gain = 2 					001: Gain = 2
	010: Gain = 4 					010: Gain = 4
	011: Gain = 8 					011: Gain = 4
	100: Gain = 16 					100: Gain = 4
	101: Gain = 32 					101: Gain = 4
	110: Gain = 64 					110: Gain = 4
	111: Gain = 128 				111: Gain = 4

 */
//! Defines allowed sensitivity settings
typedef enum ADS1220_GAIN_ENUM_ {

	//! adc gain 1
	ADS1220_GAIN_1				= 0<<1,

	//! adc gain 2
	ADS1220_GAIN_2				= 1<<1,

	//! adc gain 4
	//ADC Input Range (2.5 V Reference) - 0.625 V
	ADS1220_GAIN_4				= 2<<1,

	//! adc gain 8
	//ADC Input Range (2.5 V Reference) - 0.3125 V
	ADS1220_GAIN_8				= 3<<1,

	//! adc gain 16
	//ADC Input Range (2.5 V Reference) -  0.1562 V
	ADS1220_GAIN_16				= 4<<1,

	//! adc gain 32
	//ADC Input Range (2.5 V Reference) - 78.125 mV
	ADS1220_GAIN_32				= 5<<1,

	//! adc gain 64
	//ADC Input Range (2.5 V Reference) - 39.06 mV
	ADS1220_GAIN_64				= 6<<1,

	//! adc gain 128
	//ADC Input Range (2.5 V Reference) - 19.53 mV
	ADS1220_GAIN_128			= 7<<1,

	//! Unknown accelerometer sensitivity
	ADC_EXT_GAIN_UNKNOWN

} ADS1220_GAIN ;


/*
0x01h Register 1
Bits [7:5] DR[2:0]: Data rate
These bits control the data rate setting.
Normal Mode 			Low Power Mode 				Turbo Mode
000 : 20SPS 			000 : 5SPS 					000 : 40PS
001 : 45SPS 			001 : 11.25SPS 				001 : 90SPS
010 : 90SPS 			010 : 22.5SPS 				010 : 180SPS
011 : 175SPS 			011 : 44SPS 				011 : 350SPS
100 : 330SPS 			100 : 82.5SPS 				100 : 660SPS
101 : 600SPS 			101 : 150SPS 				101 : 1200SPS
110 : 1000SPS 			110 : 250SPS 				110 : 2000SPS
111 : 1000SPS 			111 : 250SPS 				111 : 2000SPS
*/
//! Defines allowed sampling rate
typedef enum ADS1220_RATE_ENUM_ {
	AD1220_UPDATE_RATE_20_HZ			=0<<5,
	AD1220_UPDATE_RATE_45_HZ			=1<<5,
	AD1220_UPDATE_RATE_90_HZ			=2<<5,
	AD1220_UPDATE_RATE_175_HZ			=3<<5,
	AD1220_UPDATE_RATE_330_HZ			=4<<5,
	AD1220_UPDATE_RATE_600_HZ			=5<<5,
	AD1220_UPDATE_RATE_1000_HZ			=6<<5,
	AD1220_UPDATE_RATE_1_KHZ			=7<<5
} ADS1220_RATE ;

/*
 * 0x01h Register 1
Bits [4:3] MODE[1:0]: Operating mode
00 : Normal mode (256 kHz internal system clock)
01 : Low power mode (Internal duty cycle of 1:4)
10 : Turbo mode (512 kHz internal system clock)
11 : Not Used
 */
//! Defines allowed sampling rate
typedef enum ADS1220_OPER_MODE_ {
	AD1220_NORMAL_MODE					=0<<3,
	AD1220_LOW_POWER_MODE				=1<<3,
	AD1220_TURBO_MODE					=2<<3
} ADS1220_MODE ;

typedef enum ADS1220_IDAC_CURRENT_ {
	AD1220_IDAC_CURRENT_OFF				=0,
	AD1220_IDAC_CURRENT_10uA			=1,
	AD1220_IDAC_CURRENT_50uA			=2,
	AD1220_IDAC_CURRENT_100uA			=3,
	AD1220_IDAC_CURRENT_250uA			=4,
	AD1220_IDAC_CURRENT_500uA			=5,
	AD1220_IDAC_CURRENT_1000uA			=6,
	AD1220_IDAC_CURRENT_1500uA			=7
} ADS1220_IDAC_CURRENT ;


typedef enum ADS1220_IDAC1_MUX_ {
	AD1220_IDAC1_OFF					=0<<5,
	AD1220_IDAC1_AIN0					=1<<5,
	AD1220_IDAC1_AIN1					=2<<5,
	AD1220_IDAC1_AIN2					=3<<5,
	AD1220_IDAC1_AIN3					=4<<5,
	AD1220_IDAC1_REFP0					=5<<5,
	AD1220_IDAC1_REFN0					=6<<5
} ADS1220_IDAC1_MUX ;



BOOL ADS1220ReadVoltage(BYTE adcNumber, ADS1220_SOURCE analogChannel, UVOLTS *p_vResult);
BOOL ADS1220ReadAllRegs(BYTE adcNumber, BYTE *pData, WORD maxSize);
BOOL ADS1220StartConversion(BYTE adcNumber);
BOOL ADS1220Reset(BYTE adcNumber);
BOOL ADS1220Configure(ADS1220_SOURCE analogChannel,BYTE adcNumber, ADS1220_IDAC_CURRENT idacCurrent);
BOOL ADS1220ConfigAsTemperatureSensor(BYTE adcNumber);
BOOL ADS1220ConfigureInSGTestMode(ADS1220_SOURCE analogChannel,BYTE adcNumber, ADS1220_IDAC_CURRENT idacCurrent);
BOOL ADS1220HasResult(BYTE adcNumber);
BOOL ADS1220ReadVoltageCont(BYTE adcNumber, UVOLTS *p_vResult);
BOOL ADS1220TurnOnReference(BYTE adcNumber);
BOOL ADS1220ReadTempCont(BYTE adcNumber, UVOLTS *p_vResult);
BOOL ADS1220TurnOffReference(BYTE adcNumber);
void ReadTemperature(void);
SBYTE GetTemperature(void);
BOOL ADS1220Powerdown(BYTE adcNumber);
void PowerDownADC(void);
void TurnOnAllAdcRef(void);
void TurnOffAllAdcRef(void);

#endif /* ADS1220_H_ */
