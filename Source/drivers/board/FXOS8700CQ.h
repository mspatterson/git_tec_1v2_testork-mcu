#ifndef COMPASS_H_
#define COMPASS_H_

#include "..\drivers.h"

#define DEVICE_PN		FXOS8700CQ

#define FXO_ADDRESS             	0x1E		// U1 on SCH-00686-03-2v0


#define FXO_STATUS                  0X00
#define COMP_SYSMOD_ADDR            0X0B
#define COMP_INT_SOURCE_ADDR        0X0C
#define FXO_WHO_AM_I_ADDR           0X0D

#define COMP_XYZ_DATA_CFG           0x0E

#define FXO_CTRL_REG1_ADDR          0X2A
#define COMP_CTRL_REG2_ADDR         0X2B
#define COMP_CTRL_REG3_ADDR         0X2C
#define COMP_CTRL_REG4_ADDR         0X2D
#define COMP_CTRL_REG5_ADDR         0X2E

#define COMP_M_DR_STATUS_ADDR       0X32
#define COMP_M_OUT_X_MSB_ADDR       0X33
#define COMP_M_OUT_X_LSB_ADDR       0X34
#define COMP_M_OUT_Y_MSB_ADDR       0X35
#define COMP_M_OUT_Y_LSB_ADDR       0X36
#define COMP_M_OUT_Z_MSB_ADDR       0X37
#define COMP_M_OUT_Z_LSB_ADDR       0X38
#define COMP_TEMP_ADDR              0X51
#define COMP_M_CTRL_REG1_ADDR       0X5B
#define COMP_M_CTRL_REG2_ADDR       0X5C
#define COMP_M_CTRL_REG3_ADDR       0X5D
#define COMP_M_INT_SRC_ADDR         0X5E
#define COMP_M_VECM_CFG_ADDR        0X69
#define COMP_M_VECM_THS_MSB_ADDR    0X6A
#define COMP_M_VECM_THS_LSB_ADDR    0X6B

#define COMP_WHO_AM_I_VAL           0XC7
#define COMP_ACTIVE_MODE            0X01
#define COMP_M_ODR_800HZ            0<<3
#define COMP_M_ODR_400HZ            1<<3
#define COMP_M_ODR_200HZ            2<<3
#define COMP_M_ODR_100HZ            3<<3
#define COMP_M_ODR_50HZ             4<<3
#define COMP_M_ODR_12HZ             5<<3
#define COMP_M_ODR_6HZ              6<<3
#define COMP_M_ODR_1HZ              7<<3

#define COMP_ACC_ONLY_ON            0X00
#define COMP_MAGN_ONLY_ON           0X01
#define COMP_ACC_AND_MAGN_ON        0X11

#define COMP_STANDBY_MODE           0X00
#define COMP_WAKE_MODE              0X01
#define COMP_SLEEP_MODE             3<<3

#define COMP_X_AXIS	                0
#define COMP_Z_AXIS                 1
#define COMP_Y_AXIS                 2

#define COMPASS_POWER_UP_TIME       50
#define COMPASS_WRITE_TIME          10
#define NBR_FIELD_RANGE             7

#define ACCEL_ADDRESS               0x19//0X3C
#define CTRL_REG1_ADDR              0X20
#define CTRL_REG2_ADDR              0X21
#define CTRL_REG3_ADDR              0X22
#define CTRL_REG4_ADDR              0X23
#define CTRL_REG5_ADDR              0X24
#define CTRL_REG6_ADDR              0X25
#define REFER_REG_ADDR              0X26
#define STAT_REG_ADDR               0X27
#define OUT_X_L_REG_ADDR            0X28
#define OUT_X_H_REG_ADDR            0X29
#define OUT_Y_L_REG_ADDR            0X2A
#define OUT_Y_H_REG_ADDR            0X2B
#define OUT_Z_L_REG_ADDR            0X2C
#define OUT_Z_H_REG_ADDR            0X2D

#define FIFO_CTRL_REG_ADDR          0X2E
#define FIFO_SRC_REG_ADDR           0X2F
#define INT1_CFG_REG_ADDR           0X30
#define INT1_SRC_REG_ADDR           0X31
#define INT1_THS_REG_ADDR           0X32
#define INT1_DUR_REG_ADDR           0X33
#define INT2_CFG_REG_ADDR           0X34
#define INT2_SRC_REG_ADDR           0X35
#define INT2_THS_REG_ADDR           0X36
#define INT2_DUR_REG_ADDR           0X37
#define CLICK_CFG_REG_ADDR          0X38
#define CLICK_SRC_REG_ADDR          0X39
#define CLICK_THS_REG_ADDR          0X3A
#define TIME_LIMIT_REG_ADDR         0X3B
#define TIME_LATENCY_REG_ADDR       0X3C
#define TIME_WINDOW_REG_ADDR        0X3D

#define STANDBY_MODE                0X00
#define DATA_RATE_1_HZ              1<<4
#define DATA_RATE_10_HZ             2<<4
#define DATA_RATE_25_HZ             3<<4
#define DATA_RATE_50_HZ             4<<4
#define DATA_RATE_100_HZ            5<<4
#define DATA_RATE_200_HZ            6<<4
#define DATA_RATE_400_H             7<<4
#define DATA_RATE_1620_HZ           8<<4
#define DATA_RATE_5376_HZ           9<<4
#define LOW_POWER_EN                1<<3
#define Z_AXIS_EN                   1<<2
#define Y_AXIS_EN                   1<<1
#define X_AXIS_EN                   1<<0

#define DATA_UPDATE                 1<<7    //output registers not updated until MSB and LSB reading
#define BIG_ENDIAN                  1<<6    // 1: data MSB @ lower address
#define FULL_SCALE_2G               0<<4
#define FULL_SCALE_4G               1<<4
#define FULL_SCALE_8G               2<<4
#define FULL_SCALE_16G              3<<4
#define HIGH_RES_EN                 1<<3

#define HPMSEL_0                    0<<6
#define HPMSEL_1                    1<<6
#define HPMSEL_2                    2<<6
#define HPMSEL_3                    3<<6
#define FILTER_EN                   1<<3

#define COMPASS_BUF_SIZE            10
#define ACCEL_BUF_SIZE              40

void AccelAndCompassInit(void);
BOOL ReadAccelCompass(void);
int  AccelDataShift(signed int reading);
void AccelCompassReadStart(void);
BOOL AccelCompassDataReady(void);
void AccelCompassGetRawData(BYTE *pData, WORD maxSize);
void AccelCompassDisable(void);
void FXOReadId(void);
void clearMagnetData(void);

//void AccelInit(void);
//void AccelReadDataStart(void);
//BOOL AccelDataReady(void);
//BOOL AccelTxDone(void);
//void AccelRegisterDump(void);

//void CompassInit(void);
//BOOL CompassTxDone(void);
//void CompassRegisterDump(void);
//void CompassGetRawData(BYTE *pData, WORD maxSize);
//void CompassEnable(void);
//BYTE* CompassGetDataPointer(void);
//int* CompassGetDataPointerInt(void);
//void CompassPowerDown(void);
//BOOL ReadCompass(void);

#endif /*COMPASS_H_*/
